package com.pajato.argus.android.ui.video.component

import androidx.compose.ui.graphics.vector.ImageVector

data class ActionParams(
    val details: DetailParams,
    val icon: ImageVector,
    val desc: Int,
    val text: Int,
    val action: () -> Unit
)
