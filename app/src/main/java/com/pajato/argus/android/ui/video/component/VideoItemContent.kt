package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable fun VideoItemContent(params: DetailParams) {
    TitleBox(Modifier.height(200.dp).padding(horizontal = 4.dp), params)
    InfoBox(params.video, params.coordinator.shelf)
}
