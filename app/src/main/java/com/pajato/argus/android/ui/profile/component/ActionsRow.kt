package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.profile.I18nStrings.PROFILE_MODIFY_BUTTON_DESCRIPTION
import com.pajato.argus.android.ui.video.screen.VideosScreen.ROUTE
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.argus.profile.uc.ProfileUseCases.modify
import com.pajato.i18n.strings.StringsResource.get

@Composable fun ActionsRow(item: SelectableProfile, nav: NavController) {
    val editDesc = get(PROFILE_MODIFY_BUTTON_DESCRIPTION)
    IconButton({ editProfile(item, nav) }, Modifier.padding(end  = 10.dp)) { Icon(Default.Edit, editDesc) }
}

private fun editProfile(item: SelectableProfile, nav: NavController) =
    modify(ArgusProfileRepo, item).also { nav.navigate(ROUTE) }
