package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.rejected.RejectedViewModel
import com.pajato.argus.android.ui.video.handler.reject
import com.pajato.argus.android.ui.video.handler.waitlist
import com.pajato.argus.android.ui.waiting.WaitingViewModel

@Composable fun RejectMenuItem(params: DetailParams, dismiss: Handler) {
    val context = LocalContext.current
    if (params.coordinator.shelf.id == RejectedViewModel.id) return
    DropdownMenuItem({ Text(context.getString(R.string.reject_menu_label)) }, { reject(params, dismiss) })
    HorizontalDivider()
}

@Composable fun WaitlistMenuItem(params: DetailParams, dismiss: Handler) {
    val context = LocalContext.current
    if (params.coordinator.shelf.id == WaitingViewModel.id) return
    DropdownMenuItem({ Text(context.getString(R.string.waitlist_menu_label)) }, { waitlist(params, dismiss) })
    HorizontalDivider()
}
