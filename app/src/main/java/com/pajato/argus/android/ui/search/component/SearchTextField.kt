package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable fun SearchTextField(name: String, onValueChanged: (String) -> Unit, label: @Composable () -> Unit) {
    val contentDesc = ""
    val modifier = Modifier.padding(horizontal = 8.dp).fillMaxWidth()
    val icon = @Composable { Icon(Icons.Filled.Clear, contentDesc, Modifier.clickable { onValueChanged("") }) }
    OutlinedTextField(name, onValueChanged, modifier, label = label, trailingIcon = icon)
}
