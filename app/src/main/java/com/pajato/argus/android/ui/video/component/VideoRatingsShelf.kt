package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.pajato.argus.coordinator.core.video.Video

@Composable fun VideoRatingsShelf(video: Video) {
    val notUsed = remember { mutableStateOf(false) }
    ClickableHeader(text = "Ratings (1)", actions = {}, notUsed)
    Row { VideoRatingItem(video, "tmdb") }
}
