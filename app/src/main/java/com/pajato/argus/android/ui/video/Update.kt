package com.pajato.argus.android.ui.video

import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.android.ui.watchnext.WatchNextViewModel
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.argus.network.uc.NetworkUseCases
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.uc.ProfileUseCases

internal fun update(shelf: Shelf, video: Video, map: MutableMap<String, String>, timestamp: Long) {
    val series = map[EditVideoParam.Series.toKey()]?.toIntOrNull() ?: video.series
    val episode = map[EditVideoParam.Episode.toKey()]?.toIntOrNull() ?: video.episode
    val newVideo = Video(timestamp, video.infoId, episode, series, getNetworkId(video, map), getProfileId(video, map))
    updateShelf(shelf, video, newVideo)
    WatchNextViewModel.loadData()
}

private fun getNetworkId(video: Video, map: MutableMap<String, String>): Int {
    val label = map[EditVideoParam.Network.toKey()]
    val networks = NetworkUseCases.getAll(ArgusNetworkRepo)
    return if (label == null) video.networkId else networks.find { it.network.label == label }!!.network.id
}

private fun getProfileId(video: Video, map: MutableMap<String, String>): Int {
    val label = map[EditVideoParam.Profile.toKey()]
    val profiles = ProfileUseCases.getAll(ArgusProfileRepo)
    return if (label == null) video.profileId else profiles.find { it.profile.label == label }!!.profile.id
}

private fun updateShelf(shelf: Shelf, video: Video, newVideo: Video) = when (shelf.id) {
    WatchNextViewModel.id ->  WatchLaterViewModel.update(video, newVideo)
    HistoryViewModel.id -> HistoryViewModel.update(video, newVideo)
    else -> {}
}
