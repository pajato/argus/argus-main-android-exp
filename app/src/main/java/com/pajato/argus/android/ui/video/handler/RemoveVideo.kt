package com.pajato.argus.android.ui.video.handler

import android.util.Log
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.rejected.RejectedViewModel
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.android.ui.waiting.WaitingViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.removeVideo

fun removeVideo(params: DetailParams, dismiss: Handler) {
    performAction(params.coordinator, params.video)
    goToVideosScreen(params.nav, dismiss)
}

private fun performAction(coordinator: Coordinator, video: Video): Any = when (coordinator.shelf.id) {
    Shelves.LastWatched.name -> HistoryViewModel.runUseCase { removeVideo(coordinator.repo, video) }
    Shelves.WatchLater.name -> WatchLaterViewModel.runUseCase { removeVideo(coordinator.repo, video) }
    Shelves.Rejected.name -> RejectedViewModel.runUseCase { removeVideo(coordinator.repo, video) }
    Shelves.Waiting.name -> WaitingViewModel.runUseCase { removeVideo(coordinator.repo, video) }
    else -> Log.d("removeVideo()","Unknown shelf id: ${coordinator.shelf.id}")
}
