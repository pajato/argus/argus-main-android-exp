package com.pajato.argus.android.ui.filter.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.filter.Composer
import com.pajato.argus.android.ui.filter.Done
import com.pajato.argus.android.ui.filter.FilterViewModel
import com.pajato.argus.android.ui.filter.QueryCB

@Composable internal fun QueryFilterItems(query: String, changed: QueryCB, onDone: Done) {
    val contentDesc = "Clear"
    val icon = @Composable { Icon(Icons.Filled.Clear, contentDesc, Modifier.clickable { changed("") }) }
    QueryTextField(query, icon, changed, onDone, Modifier.padding(horizontal = 8.dp).fillMaxWidth())
    SavedQueries(changed)
}

@Composable private fun QueryTextField(query: String, icon: Composer, changed: QueryCB, onDone: Done, modifier: Modifier) {
    val label = @Composable { Text("Enter query text") }
    val actions = KeyboardActions(onDone)
    OutlinedTextField(
        query, changed, modifier, label = label, trailingIcon = icon, singleLine = true, keyboardActions = actions
    )
}

@Composable private fun SavedQueries(changed: QueryCB) {
    val state = rememberLazyListState()
    val modifier = Modifier
    val currentQuery = FilterViewModel.selectedQuery.value.text
    LazyColumn(modifier, state) {
        items(FilterViewModel.savedQueries.size) { index ->
            val item = FilterViewModel.savedQueries.toList()[index]
            val itemModifier = Modifier.padding(top = 8.dp, start = 8.dp).clickable { changed(item) }
            if (item != currentQuery) SavedQueryRow(item, itemModifier)
        }
    }
}
