package com.pajato.argus.android.ui.video.component

import android.content.Context
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.pajato.argus.android.R
import com.pajato.argus.android.R.string.remove_menu_label
import com.pajato.argus.android.R.string.restore_menu_label
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.video.handler.editVideo
import com.pajato.argus.android.ui.video.handler.removeVideo
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.Shelves.Rejected
import com.pajato.argus.coordinator.core.Shelves.Waiting

@Composable fun OtherMenuItems(params: DetailParams, dismiss: Handler, context: Context) {
    if (params.coordinator.shelf.id == Shelves.WatchNext.name) return
    DropdownMenuItem({ Text(context.getString(R.string.edit_video)) }, { editVideo(params, dismiss) })
    HorizontalDivider()
    DropdownMenuItem({ Text(getRemoveOrRestoreText(context, params)) }, { removeVideo(params, dismiss) })
}

private fun getRemoveOrRestoreText(context: Context, params: DetailParams): String =
    context.getString(if (isRestoreMenuItem(params)) restore_menu_label else remove_menu_label)

private fun isRestoreMenuItem(params: DetailParams): Boolean = params.coordinator.shelf.id == Rejected.name ||
        params.coordinator.shelf.id == Waiting.name
