package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


@Composable fun SearchInfoGeneral(item: SearchData, modifier: Modifier) =
    Search(item.name, item.date, modifier, item.voteAverage, item.voteCount, item.votePopularity)

@Composable
private fun Search(name: String, airDate: String, modifier: Modifier, rating: Double, count: Int, popularity: Double) =
    Column {
        Text(name, Modifier.fillMaxWidth().padding(top = 8.dp), fontSize = 16.sp, textAlign = Center)
        SearchRow(modifier, airDate, rating, count, popularity)
    }

@Composable
private fun SearchRow(modifier: Modifier, airDate: String, rating: Double, count: Int, popularity: Double) =
    Row(modifier.padding(top = 8.dp), Arrangement.SpaceAround) {
        if (airDate.isNotEmpty()) Text(airDate, fontSize = 12.sp)
        Text("$rating ($count)", fontSize = 12.sp)
        Text("$popularity", fontSize = 12.sp, textAlign = Center)
    }
