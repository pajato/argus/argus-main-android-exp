package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.uc.ProfileUseCases

@Composable fun ProfileTextLabel(video: Video) {
    val modifier = Modifier.padding(horizontal = 8.dp)
    val label = ProfileUseCases.get(ArgusProfileRepo, video.profileId)?.profile?.label ?: "Nobody"
    Text(label, modifier, Color.White, 12.sp, textAlign = TextAlign.Center)
}
