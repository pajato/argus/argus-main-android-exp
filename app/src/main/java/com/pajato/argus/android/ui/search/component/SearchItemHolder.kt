package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Arrangement.SpaceEvenly
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultTv

@Composable
fun Modifier.SearchMovieItemHolder(item: SearchResultMovie, nav: NavController, expanded: Boolean, dismiss: Handler) =
    Column(this, SpaceEvenly) {
        SearchItemImage(item)
        SearchItemMenu(item, nav, expanded, dismiss)
    }

@Composable
fun Modifier.SearchTvItemHolder(item: SearchResultTv, nav: NavController, expanded: Boolean, dismiss: Handler) =
    Column(this, SpaceEvenly) {
        SearchItemImage(item)
        SearchItemMenu(item, nav, expanded, dismiss)
    }
