package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.account.screen.AccountScreen
import com.pajato.argus.android.ui.search.screen.QuerySearchScreen

@Composable fun ShelvesHeader(title: String, nav: NavController, start: Painter) {
    val modifier = Modifier.fillMaxWidth().height(52.dp)
    Row(modifier, SpaceBetween, CenterVertically) { HeaderItems(title, nav, start) }
}

@Composable private fun HeaderItems(title: String, nav: NavController, start: Painter) {
    ArgusButton(nav, start, title)
    EndButtons(nav)
}

@Composable private fun ArgusButton(nav: NavController, start: Painter, title: String) =
    TextButton({ nav.navigateUp() }) {
        Image(start, "Argus icon", Modifier, contentScale = ContentScale.Fit)
        Text(title, Modifier.align(CenterVertically).padding(start = 4.dp), textAlign = Center, fontSize = 20.sp)
    }

@Composable private fun EndButtons(nav: NavController) =
    Row(verticalAlignment = CenterVertically) { Items(nav) }

@Composable private fun RowScope.Items(nav: NavController) {
    val (searchDesc, moreDesc) = Pair("Search icon", "More icon")
    val (searchModifier, moreModifier) = Pair(Modifier.align(CenterVertically), Modifier.padding(end = 16.dp))
    IconButton({ nav.navigate(QuerySearchScreen.ROUTE) }, searchModifier) { Icon(Default.Search, searchDesc) }
    IconButton({ nav.navigate(AccountScreen.ROUTE) }, moreModifier) { Icon(Default.MoreVert, moreDesc) }
}
