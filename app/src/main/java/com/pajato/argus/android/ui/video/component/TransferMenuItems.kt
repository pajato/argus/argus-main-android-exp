package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.stringResource
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.video.handler.transferProfile
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.profile.adapter.ArgusProfileRepo

@Composable fun TransferMenuItems(params: DetailParams, dismiss: Handler) {
    ProfileTransferItem(params.video, dismiss)
    NetworkTransferItem(params.video, dismiss)
    HorizontalDivider()
}
