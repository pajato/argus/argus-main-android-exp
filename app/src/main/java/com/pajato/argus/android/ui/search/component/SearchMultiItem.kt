package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.search.MultiSearchViewModel
import com.pajato.argus.android.ui.search.SearchState
import com.pajato.argus.android.ui.search.screen.MultiSearchDetailsScreen
import com.pajato.argus.android.ui.video.component.extend
import com.pajato.tks.search.pager.core.SearchResultMulti

@Composable fun MultiSearchItem(i: Int, nav: NavController, name: String, state: SearchState<SearchResultMulti>) {
    val needsFetch = i >= state.items.size && !state.endReached && !state.isLoading
    LaunchedEffect(key1 = state) { if (needsFetch) MultiSearchViewModel.getNextPage(name, state) }
    SearchMultiItem(state.items[i], nav)
}

@Composable private fun SearchMultiItem(item: SearchResultMulti, nav: NavController) = Row {
    var expanded by remember { mutableStateOf(false) }
    val dismiss: Handler = { expanded = false }
    val onClick: Handler = { navigateToMultiDetailsScreen(nav, item) }
    val onLongClick: Handler = { expanded = true }
    Modifier.fillMaxSize().extend(onClick, onLongClick).SearchMultiItemHolder(item, nav, expanded, dismiss)
}

private fun navigateToMultiDetailsScreen(nav: NavController, item: SearchResultMulti) {
    MultiSearchViewModel.selectedItem = item
    nav.navigate(MultiSearchDetailsScreen.ROUTE)
}
