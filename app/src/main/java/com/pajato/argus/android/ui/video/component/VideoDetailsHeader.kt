package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.component.NavIcon
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo

@Composable fun VideoDetailsHeader(video: Video, nav: NavController) {
    val start = Pair(NavRoute.BackToVideos) { nav.navigate(NavRoute.BackToVideos.route) }
    val title = getName(ArgusInfoRepo, video)
    Row(Modifier.fillMaxWidth().height(52.dp), verticalAlignment = CenterVertically) { Items(title, start) }
}

@Composable private fun Items(title: String, start: Pair<NavRoute, () -> Unit>) {
    NavIcon(Modifier.padding(horizontal = 16.dp), start)
    LazyRow { item { Text(title, Modifier.padding(end = 8.dp), maxLines = 1, fontSize = 20.sp) } }
}
