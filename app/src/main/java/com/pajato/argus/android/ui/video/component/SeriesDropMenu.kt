package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.uc.TvUseCases

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SeriesDropMenu(video: Video, seriesState: MenuState, episodeState: MenuState, modifier: Modifier) {
    val (options, expanded, selected) = seriesState
    fun selectItem(item: String) { seriesSelectItem(item, video, seriesState, episodeState) }
    ExposedDropdownMenuBox(expanded.value, { expanded.value = !expanded.value }, modifier) {
        EditVideoDropMenuLabel((selected.value.toIntOrNull() ?: 1).toString(), "Series", expanded.value)
        EditVideoDropMenuItems(options, expanded.value, { expanded.value = !expanded.value }, ::selectItem)
    }
}

private fun seriesSelectItem(item: String, video: Video, seriesState: MenuState, episodeState: MenuState) {
    val (_, expanded, selected) = seriesState
    selectItem(item, selected, expanded, "series")
    episodeState.options.clear()
    episodeState.options.addAll(getList(TvUseCases.getEpisodeCount(ArgusInfoRepo, video.infoId, item.toInt())))
}
