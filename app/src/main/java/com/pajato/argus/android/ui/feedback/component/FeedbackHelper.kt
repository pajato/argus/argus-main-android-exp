package com.pajato.argus.android.ui.feedback.component

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.NavRouteParam
import com.pajato.argus.android.ui.common.component.NavHeader
import com.pajato.argus.android.ui.video.screen.VideosScreen

private const val SUPPORT_EMAIL = "support@pajato.com"

@Composable internal fun Header(nav: NavHostController, context: Context, title: String, feedbackText: String) {
    val startParam = NavRouteParam(NavRoute.Cancel) { nav.navigate(VideosScreen.ROUTE) }
    val endParam = NavRouteParam(NavRoute.Done) { sendFeedback(nav, context, title, feedbackText) }
    NavHeader(context.getString(R.string.feedback_title), start = startParam, end = endParam)
}

@Composable internal fun HeadingText(context: Context, title: String) {
    val heading = context.getString(R.string.headerPrefix)
    val modifier = Modifier.fillMaxWidth().padding(top = 8.dp)
    Text("$heading $title", modifier, textAlign = TextAlign.Center)
}

@Composable internal fun TextFieldLabel(context: Context) =
    Text(context.getString(R.string.feedback_text_field_label))

private fun sendFeedback(nav: NavHostController, context: Context, title: String, feedbackText: String) {
    val intent = Intent(Intent.ACTION_SEND).apply { getMailParams(title, feedbackText) }
    context.startActivity(Intent.createChooser(intent, "Send Feedback"))
    nav.navigate(VideosScreen.ROUTE)
}

private fun Intent.getMailParams(title: String, feedbackText: String) {
    type = "message/rfc822"
    putExtra(Intent.EXTRA_EMAIL, arrayOf(SUPPORT_EMAIL))
    putExtra(Intent.EXTRA_SUBJECT, "Feedback for $title")
    putExtra(Intent.EXTRA_TEXT, feedbackText)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) { flags = Intent.FLAG_GRANT_READ_URI_PERMISSION }
}
