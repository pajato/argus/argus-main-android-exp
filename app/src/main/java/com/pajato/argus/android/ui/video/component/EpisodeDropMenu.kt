package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuBoxScope
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EpisodeDropMenu(state: MenuState, modifier: Modifier) {
    val expanded = state.expanded
    ExposedDropdownMenuBox(expanded.value, { expanded.value = !expanded.value }, modifier) { MenuContent(state) }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExposedDropdownMenuBoxScope.MenuContent(state: MenuState) {
    val (options, expanded, selected) = state
    fun selectItem(item: String) { selectItem(item, selected, expanded, "episode") }
    EditVideoDropMenuLabel((selected.value.toIntOrNull() ?: 1).toString(), "Episode", expanded.value)
    EditVideoDropMenuItems(options, expanded.value, { expanded.value = !expanded.value }, ::selectItem)
}
