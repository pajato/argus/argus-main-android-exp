package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@Composable fun SearchScroller(nav: NavController, state: LazyListState, modifier: Modifier, name: String) =
    LazyColumn(modifier.padding(horizontal = 8.dp), state) {
        item { TvSearchShelf(nav, name) }
        item { MovieSearchShelf(nav, name) }
    }
