package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults.filterChipColors
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.DarkGray
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.unit.dp

private val genresMap = mapOf(
    10402 to "Music",
    10749 to "Romance",
    10751 to "Family",
    10752 to "War",
    10759 to "Action & Adventure",
    10762 to "Kids",
    10763 to "News",
    10764 to "Reality",
    10765 to "Sci-Fi & Fantasy",
    10766 to "Soap",
    10767 to "Talk",
    10768 to "War & Politics",
    10770 to "TV Movie",
    12 to "Adventure",
    14 to "Fantasy",
    16 to "Animation",
    18 to "Drama",
    27 to "Horror",
    28 to "Action",
    35 to "Comedy",
    36 to "History",
    37 to "Western",
    53 to "Thriller",
    80 to "Crime",
    878 to "Science Fiction",
    9648 to "Mystery",
    99 to "Documentary",
)

private fun getGenre(id: Int) = genresMap[id] ?: "Unknown"

@Composable fun GenreFilterChip(id: Int) {
    val colors = filterChipColors(selectedContainerColor = DarkGray, selectedLabelColor = White)
    val text = @Composable { Text(getGenre(id)) }
    FilterChip(true, {}, text, Modifier.padding(horizontal = 4.dp), colors = colors)
}
