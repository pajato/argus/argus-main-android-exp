package com.pajato.argus.android.ui.video.component

import android.content.Context
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.pajato.argus.android.R.string.mark_watched
import com.pajato.argus.android.R.string.watch_again
import com.pajato.argus.android.R.string.watch_now
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.video.handler.markWatched
import com.pajato.argus.android.ui.video.handler.watchNow
import com.pajato.argus.coordinator.core.Shelves.LastWatched
import com.pajato.argus.coordinator.core.shelf.Shelf

@Composable fun WatchMenuItem(params: DetailParams, dismiss: Handler) {
    val context = LocalContext.current
    DropdownMenuItem({ Text(getRemoveOrRestoreText(params.coordinator.shelf, context)) }, { watchNow(context, params, dismiss) })
    DropdownMenuItem({ Text(context.getString(mark_watched)) }, { markWatched(params, dismiss) })
    HorizontalDivider()
}

private fun getRemoveOrRestoreText(shelf: Shelf, context: Context): String =
    if (shelf.id == LastWatched.name) context.getString(watch_again) else context.getString(watch_now)
