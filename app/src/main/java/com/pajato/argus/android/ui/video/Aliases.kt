package com.pajato.argus.android.ui.video

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.text.TextLayoutResult

typealias OnTextLayout = (TextLayoutResult) -> Unit
typealias Options = SnapshotStateList<String>
