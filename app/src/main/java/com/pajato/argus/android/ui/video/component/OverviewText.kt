package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import com.pajato.argus.android.ui.video.OnTextLayout

@Composable fun OverviewText(modifier: Modifier, overview: MutableState<String>, onTextLayout: OnTextLayout) {
    val textModifier = modifier.fillMaxHeight().verticalScroll(rememberScrollState())
    Text(overview.value, textModifier, onTextLayout = onTextLayout)
}
