package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.uc.MovieUseCases.getMovieBackdropPath
import com.pajato.argus.info.uc.TvUseCases.getTvBackdropPath

@Composable fun ImageOrText(modifier: Modifier, video: Video) {
    val path = remember { mutableStateOf("") }
    val id = "${video.infoId}"
    if (video.isTv()) FetchStillPath(video, path) else useBackdrop(video, path)
    if (path.value.isEmpty()) Text(id, modifier) else InfoImageWithOverlay(video, path.value, id)
}

private fun useBackdrop(video: Video, path: MutableState<String>) {
    fun getMovieBackdropPath() = getMovieBackdropPath(ArgusInfoRepo, video.infoId)
    fun getTvBackdropPath() = getTvBackdropPath(ArgusInfoRepo, video.infoId)
    val backdrop = if (video.isMovie()) getMovieBackdropPath() else getTvBackdropPath()
    if (backdrop.isNotEmpty()) path.value = backdrop
}
