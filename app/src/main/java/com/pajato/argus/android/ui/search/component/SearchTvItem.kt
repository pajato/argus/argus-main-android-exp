package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.search.SearchState
import com.pajato.argus.android.ui.search.TvSearchViewModel
import com.pajato.argus.android.ui.search.screen.TvSearchDetailsScreen
import com.pajato.tks.search.pager.core.SearchResultTv

@Composable fun SearchTvItem(i: Int, nav: NavController, name: String, state: SearchState<SearchResultTv>) {
    val needsFetch = i >= state.items.size && !state.endReached && !state.isLoading
    LaunchedEffect(key1 = state) { if (needsFetch) TvSearchViewModel.getNextPage(name, state) }
    SearchTvItem(state.items[i], nav)
}

@Composable fun SearchTvItem(item: SearchResultTv, nav: NavController) = Row {
    var expanded by remember { mutableStateOf(false) }
    val dismiss: Handler = { expanded = false }
    val onClick: Handler = { navigateToTvDetailsScreen(nav, item) }
    val onLongClick: Handler = { expanded = true }
    Modifier.Companion.fillMaxSize().extend(onClick, onLongClick).SearchTvItemHolder(item, nav, expanded, dismiss)
}

private fun navigateToTvDetailsScreen(nav: NavController, item: SearchResultTv) {
    TvSearchViewModel.selectedItem = item
    nav.navigate(TvSearchDetailsScreen.ROUTE)
}
