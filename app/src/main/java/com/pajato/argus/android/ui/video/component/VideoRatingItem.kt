package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.uc.MovieUseCases.getMovieRating
import com.pajato.tks.common.core.EpisodeKey
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

@Composable fun VideoRatingItem(video: Video, rater: String) {
    val rating = remember { mutableStateOf("N/A") }
    val modifier = Modifier.padding(horizontal = 16.dp, vertical = 4.dp)
    LaunchedEffect(rater) { getRatings(video, rating, rater) }
    Column(modifier, Center, CenterHorizontally) { RatingItems(rating, rater) }
}

@Composable fun RatingItems(rating: MutableState<String>, rater: String) {
    Text(rating.value, Modifier.padding(horizontal = 12.dp), textAlign = TextAlign.Center)
    rater.uppercase().split(" ").forEach { SmallText(it) }
}

suspend fun getRatings(video: Video, rating: MutableState<String>, rater: String) {
    coroutineScope {
        if (rater == "tmdb") if (video.isMovie()) setMovieRating(video, rating) else setEpisodeRating(video, rating)
    }
}

suspend fun setEpisodeRating(video: Video, rating: MutableState<String>) {
    val key = EpisodeKey(video.infoId, video.series, video.episode)
    coroutineScope { launch(Dispatchers.IO) { updateEpisodeRating(key, rating) } }
}

suspend fun updateEpisodeRating(key: EpisodeKey, rating: MutableState<String>) {
    val episode = TmdbEpisodeRepo.getEpisode(key)
    val value = (episode.voteAverage * 10).toInt()
    val count = episode.voteCount
    rating.value = "$value% ($count)"
}

fun setMovieRating(video: Video, rating: MutableState<String>) {
    rating.value = getMovieRating(ArgusInfoRepo, InfoKey(Movie.name, video.infoId))
}
