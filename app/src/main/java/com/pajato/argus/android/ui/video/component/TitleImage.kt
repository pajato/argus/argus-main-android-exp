package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale.Companion.FillBounds
import androidx.compose.ui.layout.ContentScale.Companion.FillWidth
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.pajato.argus.android.R.drawable.ic_photo
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Composable fun TitleImage(video: Video, modifier: Modifier) {
    val name = remember { mutableStateOf("")}
    val path = remember { mutableStateOf("") }
    LaunchedEffect(video) { withContext(Dispatchers.IO) { updateNameAndPath(video, name, path) } }
    AsyncImage(getModel(path), name.value, modifier, painterResource(ic_photo), contentScale = FillBounds)
}

@Composable fun getModel(path: MutableState<String>): ImageRequest? {
    val url = if (path.value != "") "https://image.tmdb.org/t/p/w200${path.value}" else null
    return if (url != null) getImageRequest(url, LocalContext.current) else null
}

fun updateNameAndPath(video: Video, name: MutableState<String>, path: MutableState<String>) {
    name.value = getName(ArgusInfoRepo, video)
    path.value = getPosterPath(ArgusInfoRepo, video)
}

@Composable fun TitleImage(path: String, name: String) {
    val modifier = Modifier.fillMaxHeight()
    val imageUrl = "https://image.tmdb.org/t/p/w780$path"
    @Composable fun getModel(): ImageRequest = getImageRequest(imageUrl, LocalContext.current)
    AsyncImage(getModel(), name, modifier, painterResource(ic_photo), contentScale = FillWidth)
}
