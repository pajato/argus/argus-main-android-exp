package com.pajato.argus.android.ui.video.component

import com.pajato.argus.android.ui.finished.FinishedViewModel
import com.pajato.argus.android.ui.rejected.RejectedViewModel
import com.pajato.argus.android.ui.waiting.WaitingViewModel
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.paused.PausedViewModel
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.android.ui.watchnext.WatchNextViewModel
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.Shelves.valueOf

internal fun getViewModel(coordinator: Coordinator): ShelfViewModel = when (valueOf(coordinator.shelf.id)) {
    Shelves.LastWatched -> HistoryViewModel
    Shelves.WatchNext -> WatchNextViewModel
    Shelves.Paused -> PausedViewModel
    Shelves.Finished -> FinishedViewModel
    Shelves.Rejected -> RejectedViewModel
    Shelves.WatchLater -> WatchLaterViewModel
    Shelves.Waiting -> WaitingViewModel
}
