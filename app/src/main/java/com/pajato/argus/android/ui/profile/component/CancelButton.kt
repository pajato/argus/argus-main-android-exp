package com.pajato.argus.android.ui.profile.component

import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.video.screen.VideosScreen

@Composable fun CancelButton(modifier: Modifier, nav: NavController) {
    Button(onClick = { nav.navigate(VideosScreen.ROUTE) }, modifier) { Text("Cancel") }
}
