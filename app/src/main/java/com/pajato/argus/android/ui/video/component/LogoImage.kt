package com.pajato.argus.android.ui.video.component

import android.content.Context
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale.Companion.Fit
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.pajato.argus.android.R.drawable.ic_photo
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.uc.MovieUseCases.getMovieTitle
import com.pajato.argus.info.uc.TvUseCases.getTvName
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.argus.network.uc.NetworkUseCases.get
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private const val IMAGE_URL_BASE = "https://image.tmdb.org/t/p/original"

@Composable fun LogoImage(video: Video) {
    val modifier = Modifier.width(CardWidth.dp).fillMaxWidth()
    val context: Context = LocalContext.current
    AsyncImage(video, modifier, context)
}

@Composable fun AsyncImage(video: Video, modifier: Modifier, context: Context) {
    val name = remember { mutableStateOf("") }
    val model = getModel(context, video)
    LaunchedEffect(video) { launch(Dispatchers.IO) { name.value = getName(ArgusInfoRepo, video) } }
    AsyncImage(model, name.value, modifier, painterResource(ic_photo), contentScale = Fit)
}

fun getImageUrl(video: Video): String = "$IMAGE_URL_BASE${get(ArgusNetworkRepo, video.networkId)?.network?.iconUrl}"

fun getModel(context: Context, video: Video): ImageRequest = getImageRequest(getImageUrl(video), context)

internal fun getName(repo: InfoRepo, video: Video): String =
    if (video.isMovie()) getMovieTitle(repo, video.infoId) else getTvName(repo, video.infoId)
