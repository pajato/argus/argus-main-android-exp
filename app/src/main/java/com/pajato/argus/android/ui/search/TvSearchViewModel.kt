package com.pajato.argus.android.ui.search

import com.pajato.tks.search.pager.core.SearchResultTv
import com.pajato.tks.search.tv.adapter.TmdbSearchTvRepo

object TvSearchViewModel : AbstractSearchViewModel<SearchResultTv>(TmdbSearchTvRepo::getSearchPageTv)
