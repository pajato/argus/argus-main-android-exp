package com.pajato.argus.android.ui.rejected

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.genre.GenreViewModel.selectedGenreIds
import com.pajato.argus.android.ui.network.NetworkViewModel.selectedNetworkIds
import com.pajato.argus.android.ui.profile.ProfileViewModel.selectedProfileIds
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.cache
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_UNREGISTERED_ERROR
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

object RejectedViewModel : ShelfViewModel, ViewModel() {
    override val id: String = Shelves.Rejected.name

    override val coordinator: Coordinator = cache[id] ?: throw CoordinatorError(getUnregisteredErrorMessage(id))

    private fun getUnregisteredErrorMessage(id: String): String = get(COORDINATOR_UNREGISTERED_ERROR, Arg("id", id))

    override val videos: List<Video> get() = videosStateList.reversed()
    private var videosStateList = listOf<Video>().toMutableStateList() // aka _videos

    override fun loadData() {
        val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds)
        val list = coordinator.repo.filter(filters, ArgusInfoRepo).toMutableStateList()
        resetStateLists(list)
    }

    private fun resetStateLists(videosToAdd: SnapshotStateList<Video>) {
        videosStateList.clear()
        videosStateList.addAll(videosToAdd)
    }

    override fun runUseCase(handler: suspend () -> Unit) {
        viewModelScope.launch(IO) { performAction(handler) }
    }

    private suspend fun performAction(handler: suspend () -> Unit) {
        val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds)
        handler()
        resetStateLists(coordinator.repo.filter(filters, ArgusInfoRepo).toMutableStateList())
        reloadShelfDataInOrder()
    }
}
