package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Video

@Composable fun EditVideoContentItems(shelf: Shelf?, video: Video, height: Dp) {
    VideoImageOrName(video, Modifier.fillMaxWidth().heightIn(height, height))
    if (video.isTv()) SeriesEpisodeRow(video)
    ProfileNetworkRow(video)
    if (isHistoryShelf(shelf)) TimestampWithErrorMaybe(video)
}

private fun isHistoryShelf(shelf: Shelf?): Boolean = shelf != null && shelf.id == Shelves.LastWatched.name
