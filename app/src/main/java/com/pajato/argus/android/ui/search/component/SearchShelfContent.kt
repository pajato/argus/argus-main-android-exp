package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.search.SearchState
import com.pajato.argus.android.ui.search.screen.CardHeight
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultTv

@Composable fun MovieSearchShelfContent(nav: NavController, name: String, state: SearchState<SearchResultMovie>) {
    Text("Movies (${state.totalItems})", Modifier.padding(4.dp), Color.White.copy(alpha = 0.78F))
    LazyRow(modifier = Modifier.height(CardHeight.dp).fillMaxWidth()) {
        items(state.items.size) { i -> SearchMovieItem(i, nav, name, state) }
        item { if (state.isLoading) LoadingIndicator() }
    }
}

@Composable fun TvSearchShelfContent(nav: NavController, name: String, state: SearchState<SearchResultTv>) {
    Text("Tv Shows (${state.totalItems})", Modifier.padding(4.dp), Color.White.copy(alpha = 0.78F))
    LazyRow(modifier = Modifier.height(CardHeight.dp).fillMaxWidth()) {
        items(state.items.size) { i -> SearchTvItem(i, nav, name, state) }
        item { if (state.isLoading) LoadingIndicator() }
    }
}
