package com.pajato.argus.android.ui.account.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.account.component.AboutScreenItems
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.NavRouteParam
import com.pajato.argus.android.ui.common.component.NavIcon
import com.pajato.argus.android.ui.search.component.TextParams
import com.pajato.argus.android.ui.video.screen.VideosScreen.ROUTE

object AboutScreen { const val ROUTE = "AboutScreen" }

@Composable fun AboutScreen(nav: NavController) = Column(Modifier.fillMaxSize()) {
    AboutHeader(nav, Modifier.fillMaxWidth().height(60.dp).padding(vertical = 8.dp))
    AboutScreenBody()
}

@Composable fun AboutHeader(nav: NavController, modifier: Modifier) = Box(modifier) { AboutHeaderItems(nav) }

@Composable private fun BoxScope.AboutHeaderItems(nav: NavController) {
    NavIcon(Modifier.padding(start = 8.dp).align(CenterStart), NavRouteParam(NavRoute.Cancel) { nav.navigate(ROUTE) })
    Row(Modifier.align(Alignment.Center)) {
        Image(painterResource(R.drawable.argus_reverso), "Argus icon", Modifier, contentScale = ContentScale.Fit)
        Text("Argus", Modifier.align(CenterVertically).padding(8.dp), textAlign = Center, fontSize = 20.sp)
    }
}

@Composable private fun AboutScreenBody() {
    val modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp, vertical = 8.dp)
    val params = TextParams(stringResource(R.string.argus_description), modifier, LocalTextStyle.current, 14.sp)
    LazyColumn(Modifier.fillMaxSize()) { AboutScreenItems(modifier, params) }
}
