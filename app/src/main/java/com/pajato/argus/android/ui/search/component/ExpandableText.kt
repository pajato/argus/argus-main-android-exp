package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight

private val defaultShowControls = getDefaultShowControls()

private fun getDefaultShowControls(): ShowControls {
    val moreText = "... More"
    val lessText = " Less"
    val style = SpanStyle(fontWeight = FontWeight.W500, fontStyle = FontStyle.Italic)
    return ShowControls(moreText, style, lessText, style)
}

/**
 * An expandable text component that provides access to truncated text with a dynamic ... Show More/ Show Less button.
 *
 * @param modifier Modifier for the composable containing the text.
 * @param params Parameters applicable to the Text component
 * @param controls The ShowControls configuration for "Show More" and "Show Less" buttons.
 */
@Composable
internal fun ExpandableText(modifier: Modifier, params: TextParams, controls: ShowControls = defaultShowControls) {
    val state = getShowState()
    val boxModifier = getBoxModifier(state.clickable, state.isExpanded, modifier)
    Box(boxModifier) { DecoratedText(state, params, controls, params.maxLines) }
}

@Composable private fun getShowState(): ShowState {
    val clickable = remember { mutableStateOf(false) }
    val isExpanded = remember { mutableStateOf(false) }
    val lastCharIndex = remember { mutableIntStateOf(0) }
    return ShowState(clickable, isExpanded, lastCharIndex)
}

@Composable private fun getBoxModifier(clickable: MutableBoolean, isExpanded: MutableBoolean, modifier: Modifier) =
    Modifier.clickable(clickable.value) { isExpanded.value = !isExpanded.value }.then(modifier)
