package com.pajato.argus.android.ui.common.component

import androidx.compose.foundation.layout.Arrangement.Start
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.NavRouteParam
import com.pajato.argus.android.ui.profile.component.BackButton

@Composable fun NavHeader(nav: NavController, title: String) =
    Row(Modifier.padding(horizontal = 16.dp), Start, CenterVertically) {
        BackButton(nav)
        Text(title, fontSize = 20.sp)
    }

@Composable fun NavHeader(title: String, start: NavRouteParam?, end: NavRouteParam?) =
    Box(Modifier.fillMaxWidth().height(52.dp)) { NavHeaHeaderItems(title, start, end) }

@Composable private fun BoxScope.NavHeaHeaderItems(title: String, start: NavRouteParam?, end: NavRouteParam?) {
    if (start != null) NavIcon(Modifier.padding(start = 8.dp).align(CenterStart), start)
    Text(title, Modifier.align(Alignment.Center), textAlign = Center, fontSize = 20.sp)
    if (end != null) NavIcon(Modifier.padding(end = 8.dp).align(CenterEnd), end)
}

@Composable internal fun CancelDoneHeader(nav: NavController, title: String) =
    Box(Modifier.fillMaxWidth().height(52.dp)) { CancelHeaderItems(nav, title) }

@Composable private fun BoxScope.CancelHeaderItems(nav: NavController, title: String) {
    CancelButton(nav)
    Text(title, Modifier.align(Alignment.Center), textAlign = Center, fontSize = 20.sp)
    DoneButton(nav)
}
