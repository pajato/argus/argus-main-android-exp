package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.layout.Arrangement.Start
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.profile.I18nStrings
import com.pajato.i18n.strings.StringsResource

@Composable fun HeaderRow(nav: NavController) = Row(Modifier.padding(horizontal = 16.dp), Start, CenterVertically) {
    val text = StringsResource.get(I18nStrings.MANAGE_PROFILES_SCREEN_TITLE)
    BackButton(nav)
    Text(text, fontSize = 20.sp)
}
