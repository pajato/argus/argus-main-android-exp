package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable fun EpisodeDetailsAction(params: ActionParams, modifier: Modifier) {
    val columnModifier = modifier.clickable { params.action() }
    Column(columnModifier, Center, CenterHorizontally) { ColumnItems(params) }
}

@Composable private fun ColumnItems(params: ActionParams) {
    val context = LocalContext.current
    val desc = context.getString(params.desc)
    Icon(params.icon, desc, Modifier.width(28.dp).height(28.dp))
    Text(context.getString(params.text), fontSize = 12.sp)
}
