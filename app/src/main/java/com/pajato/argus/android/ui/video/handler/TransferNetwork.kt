package com.pajato.argus.android.ui.video.handler

import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.getRepo
import com.pajato.argus.network.core.Network

fun transferNetwork(network: Network, video: Video, dismiss: Handler, mainDismiss: Handler) {
    val repo = getRepo(ArgusCoordinatorRepo, Shelves.LastWatched.name)
    val list = repo.cache.values.filter { it.infoId == video.infoId }.map { copyOf(it, network) }
    transfer(list, dismiss, mainDismiss)
}
