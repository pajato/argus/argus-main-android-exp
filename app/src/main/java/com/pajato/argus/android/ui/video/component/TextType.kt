package com.pajato.argus.android.ui.video.component

internal enum class TextType { Actions, AddLastWatched, AddWatchLater, Basics, MoveDown, MoveUp, Purpose, Title }
