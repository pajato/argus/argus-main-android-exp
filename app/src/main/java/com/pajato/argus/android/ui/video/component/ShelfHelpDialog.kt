package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.pajato.argus.android.R
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.Shelves

@Composable internal fun ShelfHelpDialog(coordinator: Coordinator, showDialog: Boolean, onDismiss: () -> Unit) {
    val modifier = Modifier.padding(16.dp)
    if (showDialog) Dialog(onDismiss) { Card { Column(modifier) { ShelfHelpBody(coordinator, onDismiss) } } }
}

@Composable private fun ShelfHelpBody(coordinator: Coordinator, onDismiss: () -> Unit) {
    val shelf = Shelves.valueOf(coordinator.shelf.id)
    ShelfHelpItems(shelf)
    ShelfHelpActions(coordinator, shelf)
    Spacer(modifier = Modifier.height(8.dp))
    Button(onDismiss) { Text(LocalContext.current.getString(R.string.got_it)) }
}

@Composable private fun ShelfHelpItems(shelf: Shelves) {
    ShelfHelpItem(shelf, TextType.Title)
    ShelfHelpItem(shelf, TextType.Purpose)
    ShelfHelpItem(shelf, TextType.Basics)
    ShelfHelpItem(shelf, TextType.Actions)
}
