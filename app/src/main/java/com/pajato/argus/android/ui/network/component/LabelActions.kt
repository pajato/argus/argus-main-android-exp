package com.pajato.argus.android.ui.network.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.network.core.SelectableNetwork

@Composable fun LabelActions(modifier: Modifier, nav: NavController, network: SelectableNetwork, hasError: Boolean) =
    Row(Modifier.padding(top = 8.dp).fillMaxWidth(), Arrangement.End) { CancelButton(modifier, nav) }
