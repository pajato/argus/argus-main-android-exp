package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

internal const val CardWidth = 130

@Composable fun VideoItem(params: DetailParams) =
    Column(Modifier.width(CardWidth.dp), Center) { VideoItemContent(params) }
