package com.pajato.argus.android.ui.common.component

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.ui.graphics.Color

internal fun getColor(state: Boolean) =
    if (state) Color(0xFFA0A1C2) else Color(0XFF423460).copy(alpha = 0.5F)

internal fun getSpec(state: Boolean): AnimationSpec<Color> =
    tween(if (state) 100 else 50, 0, LinearOutSlowInEasing)
