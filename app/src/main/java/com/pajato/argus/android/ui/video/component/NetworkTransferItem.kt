package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.coordinator.core.video.Video

@Composable fun NetworkTransferItem(video: Video, dismiss: Handler) {
    var networksMenuExpanded by remember { mutableStateOf(false) }
    val transferDismiss: Handler = { networksMenuExpanded = false }
    DropdownMenuItem({ Text("Transfer Network") }, { networksMenuExpanded = true })
    DropdownMenu(networksMenuExpanded, transferDismiss) { NetworkMenuItems(video, transferDismiss, dismiss) }
}
