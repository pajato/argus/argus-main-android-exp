package com.pajato.argus.android.ui.video.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.video.EditVideoViewModel
import com.pajato.argus.android.ui.video.VideoViewModel.detailArgs
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.android.ui.video.component.EditVideoContent
import com.pajato.argus.android.ui.video.component.EditVideoHeader

object EditVideoScreen { const val ROUTE = "EditVideoScreen" }

@Composable fun EditVideoScreen(nav: NavController) = Column(Modifier.fillMaxSize()) {
    val params = DetailParams(detailArgs.coordinator, detailArgs.video, nav)
    EditVideoViewModel.map.clear()
    EditVideoHeader(params)
    EditVideoContent(params.coordinator, params.video)
}
