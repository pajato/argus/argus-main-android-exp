package com.pajato.argus.android.ui.common.component

import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.ManagedViewModel
import com.pajato.argus.android.ui.common.Widget

data class ManageParams(
    val nav: NavController,
    val viewModel: ManagedViewModel,
    val title: String,
    val drawable: Int,
    val list: List<String>,
    val extra: Widget = {}
)
