package com.pajato.argus.android.ui.video

import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.video.Video

interface ShelfViewModel {
    val id: String
    val coordinator: Coordinator
    val videos: List<Video>
    fun loadData()
    fun runUseCase(handler: suspend () -> Unit)
}
