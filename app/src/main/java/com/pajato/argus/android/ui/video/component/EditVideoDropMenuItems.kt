package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.pajato.argus.android.ui.common.Handler

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Scope.EditVideoDropMenuItems(options: List<String>, expanded: Boolean, dismiss: Handler, onClick: StringHandler) {
    ExposedDropdownMenu(expanded, dismiss) { options.forEach { DropdownMenuItem({ Text(it) }, { onClick(it) }) } }
}
