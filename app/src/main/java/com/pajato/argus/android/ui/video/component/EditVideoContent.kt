package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.video.Video

@Composable fun EditVideoContent(coordinator: Coordinator, video: Video) {
    val metrics = LocalContext.current.resources.displayMetrics
    val height = ((metrics.widthPixels / metrics.density) * (438.75f / 780f)).dp
    Column(Modifier.fillMaxSize()) { EditVideoContentItems(coordinator.shelf, video, height) }
}
