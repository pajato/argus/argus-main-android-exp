package com.pajato.argus.android.ui.search

import com.pajato.tks.search.pager.core.PagerState


data class SearchState<T>(
    override val isLoading: Boolean = false,
    override val items: List<T> = emptyList(),
    override val error: String = "",
    override val endReached: Boolean = false,
    override val page: Int = 0,
    override val totalItems: Int = 0,
) : PagerState<T>
