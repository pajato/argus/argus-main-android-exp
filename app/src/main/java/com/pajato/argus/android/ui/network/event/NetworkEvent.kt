package com.pajato.argus.android.ui.network.event

import com.pajato.argus.network.core.SelectableNetwork


sealed class NetworkEvent

data class ToggleSelectedNetwork(val network: SelectableNetwork) : NetworkEvent()
data class ToggleHiddenNetwork(val network: SelectableNetwork) : NetworkEvent()
