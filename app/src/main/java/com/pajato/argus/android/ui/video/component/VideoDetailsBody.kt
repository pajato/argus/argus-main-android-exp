package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp

@Composable fun VideoDetailsBody(params: DetailParams) {
    val metrics = LocalContext.current.resources.displayMetrics
    val height = ((metrics.widthPixels / metrics.density) * (438.75f / 780f)).dp
    Column(Modifier.fillMaxSize()) { VideoDetailsBodyItems(params, height) }
}
