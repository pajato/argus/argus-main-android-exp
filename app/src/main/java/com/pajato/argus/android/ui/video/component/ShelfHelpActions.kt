package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowDownward
import androidx.compose.material.icons.filled.ArrowUpward
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.Shelves

@Composable internal fun ShelfHelpActions(coordinator: Coordinator, shelf: Shelves) {
    if (isNotLastShelf(coordinator)) ActionItems(shelf, TextType.MoveDown, Icons.Filled.ArrowDownward)
    if (isNotFirstShelf(coordinator)) ActionItems(shelf, TextType.MoveUp, Icons.Filled.ArrowUpward)
    if (shelf == Shelves.LastWatched) ActionItems(shelf, TextType.AddLastWatched, Icons.Filled.Add)
    if (shelf == Shelves.WatchLater) ActionItems(shelf, TextType.AddWatchLater, Icons.Filled.Add)
}

@Composable private fun ActionItems(shelf: Shelves, type: TextType, icon: ImageVector) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Icon(icon, contentDescription = null)
        Text(getShelfHelpText(shelf, type, context = LocalContext.current), fontSize = 14.sp)
    }
    Spacer(modifier = Modifier.height(8.dp))
}
