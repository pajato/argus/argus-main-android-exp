package com.pajato.argus.android.ui.search.component

import android.content.Context
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.TopCenter
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale.Companion.FillWidth
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.search.screen.CardWidth
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultTv

@Composable fun SearchItemImage(item: SearchResultMovie) = SearchImage(item.title, item.posterPath)

@Composable fun SearchItemImage(item: SearchResultTv) = SearchImage(item.name, item.posterPath)

@Composable internal fun SearchImage(name: String, path: String, width: Int = CardWidth) {
    val model = getImageRequest("https://image.tmdb.org/t/p/w200$path", LocalContext.current)
    val placeholder = painterResource(id = R.drawable.ic_photo)
    val modifier = Modifier.width(width.dp).fillMaxWidth()
    AsyncImage(model, name, modifier, placeholder, alignment = TopCenter, contentScale = FillWidth)
}

internal fun getImageRequest(url: String, context: Context): ImageRequest =
    ImageRequest.Builder(context).data(url).crossfade(true).build()
