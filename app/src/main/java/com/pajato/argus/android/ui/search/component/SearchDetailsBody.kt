package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.network.component.NetworkChooser
import com.pajato.argus.android.ui.search.I18nStrings.SEARCH_CAST_CREW
import com.pajato.argus.android.ui.search.I18nStrings.SEARCH_REVIEWS
import com.pajato.argus.android.ui.search.I18nStrings.SEARCH_SEASON_SHELF
import com.pajato.i18n.strings.StringsResource.get

@Composable fun SearchDetailsBody(item: SearchData) {
    val metrics = LocalContext.current.resources.displayMetrics
    val height = ((metrics.widthPixels / metrics.density) * (438.75f / 780f)).dp
    val modifier = Modifier.fillMaxWidth().heightIn(height, height)
    LazyColumn(Modifier.fillMaxSize()) { showDetailItems(item, modifier, Modifier.padding(16.dp)) }
}

private fun LazyListScope.showDetailItems(item: SearchData, modifier: Modifier, textModifier: Modifier) {
    item { Box(modifier) { SearchImageOrText(item, Modifier.align(Center)) } }
    item { Column { SearchInfo(item, Modifier.fillMaxWidth()) } }
    item { Row(Modifier.padding(8.dp, 0.dp, 0.dp, 4.dp)) { NetworkChooser() } }
    if (item.hasSeasons) item { Box(modifier) { Text(get(SEARCH_SEASON_SHELF), textModifier.align(Center)) } }
    item { Box(modifier) { Text(get(SEARCH_CAST_CREW), textModifier.align(Center)) } }
    item { Box(modifier) { Text(get(SEARCH_REVIEWS), textModifier.align(Center)) } }
}
