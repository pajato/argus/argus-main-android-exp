package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.HelpOutline
import androidx.compose.material.icons.automirrored.outlined.Help
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowDownward
import androidx.compose.material.icons.filled.ArrowUpward
import androidx.compose.material.icons.filled.Help
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.input.key.Key.Companion.Help
import com.pajato.argus.android.ui.common.Nav
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.search.screen.VideoSearchScreen
import com.pajato.argus.android.ui.video.CoordinatorViewModel
import com.pajato.argus.android.ui.video.handler.moveShelfDownHandler
import com.pajato.argus.android.ui.video.handler.moveShelfUpHandler
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.core.Coordinator

private const val SHELF_UP_DESC = "Move shelf up"
private const val SHELF_DOWN_DESC = "Move shelf down"
private const val ADD_DESC = "Add video"

@Composable internal fun MoveShelfUpIcon(coordinator: Coordinator, nav: Nav, modifier: Modifier) =
    Icon(Icons.Default.ArrowUpward, SHELF_UP_DESC, modifier.moveShelfUp(nav, coordinator))

@Composable internal fun MoveShelfDownIcon(coordinator: Coordinator, nav: Nav, modifier: Modifier) =
    Icon(Icons.Default.ArrowDownward, SHELF_DOWN_DESC, modifier.moveShelfDown(nav, coordinator))

@Composable internal fun AddToShelfIcon(coordinator: Coordinator, nav: Nav, modifier: Modifier) =
    Icon(Icons.Default.Add, ADD_DESC, modifier.addVideoMaybe(nav, coordinator))

private fun Modifier.moveShelfUp(nav: Nav, coordinator: Coordinator) = composed {
    val source: MutableInteractionSource = remember { MutableInteractionSource() }
    this.clickable(source, indication = null) {
        moveShelfUpHandler(coordinator.shelf.id)
        CoordinatorViewModel.register(ArgusCoordinatorRepo.orderList.toMutableStateList())
        goToVideosScreen(nav) {}
    }
}

private fun Modifier.moveShelfDown(nav: Nav, coordinator: Coordinator) = composed {
    val source: MutableInteractionSource = remember { MutableInteractionSource() }
    this.clickable(source, indication = null) {
        moveShelfDownHandler(coordinator.shelf.id)
        CoordinatorViewModel.register(ArgusCoordinatorRepo.orderList.toMutableStateList())
        goToVideosScreen(nav) {}
    }
}

private fun Modifier.addVideoMaybe(nav: Nav, coordinator: Coordinator): Modifier = composed {
    val source: MutableInteractionSource = remember { MutableInteractionSource() }
    this.clickable(source, indication = null) { navigateToSearchScreen(nav, coordinator) }
}

private fun navigateToSearchScreen(nav: Nav, coordinator: Coordinator) {
    VideoSearchScreen.originShelf = coordinator.shelf
    nav.navigate(VideoSearchScreen.ROUTE)
}
