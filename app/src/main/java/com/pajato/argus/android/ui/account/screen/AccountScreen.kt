package com.pajato.argus.android.ui.account.screen

import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.account.component.AccountScreenBodyItems
import com.pajato.argus.android.ui.account.component.cancel
import com.pajato.argus.android.ui.common.NavRoute.Cancel
import com.pajato.argus.android.ui.common.component.NavHeader

object AccountScreen { const val ROUTE = "AccountScreen" }

@Composable fun AccountScreen(nav: NavController) = Column(Modifier.fillMaxSize()) {
    val modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp, vertical = 8.dp)
    val context = LocalContext.current
    NavHeader("Account", Pair(Cancel) { cancel(nav) }, null)
    Column(Modifier.fillMaxSize(), SpaceBetween) { AccountScreenBodyItems(modifier, nav, context) }
}
