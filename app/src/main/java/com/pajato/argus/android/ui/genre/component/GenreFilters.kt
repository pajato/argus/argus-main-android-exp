package com.pajato.argus.android.ui.genre.component

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults.filterChipColors
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.genre.GenreViewModel
import com.pajato.argus.android.ui.genre.event.ToggleSelectedGenre
import com.pajato.argus.genre.core.SelectableGenre

@Composable fun GenreFilters() {
    val list: List<SelectableGenre> = GenreViewModel.genres.filter { !it.isHidden }
    val listState = rememberLazyListState()
    val modifier = Modifier.padding(horizontal = 4.dp)
    LazyRow(Modifier.padding(start = 2.dp), listState) { items(list.size) { GenreFilterChip(list, it, modifier) } }
}

@Composable private fun GenreFilterChip(list: List<SelectableGenre>, it: Int, modifier: Modifier) {
    val colors = filterChipColors(selectedContainerColor = White, selectedLabelColor = Black)
    val text = @Composable { Text(list[it].genre.name) }
    fun onClick() { GenreViewModel.onEvent(ToggleSelectedGenre(list[it])) }
    FilterChip(list[it].isSelected, ::onClick, text, modifier, colors = colors)
}
