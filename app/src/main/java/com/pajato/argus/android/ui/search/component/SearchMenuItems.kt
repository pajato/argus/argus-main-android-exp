package com.pajato.argus.android.ui.search.component

import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.search.I18nStrings.SEARCH_WATCH_LATER_ADD
import com.pajato.argus.android.ui.search.I18nStrings.SEARCH_WATCH_NOW
import com.pajato.argus.android.ui.video.handler.addToWatchLater
import com.pajato.argus.android.ui.video.handler.markWatched
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultMulti
import com.pajato.tks.search.pager.core.SearchResultTv

@Composable fun SearchMenuItems(item: SearchResultMovie, nav: NavController, dismiss: Handler) {
    DropdownMenuItem({ Text(text = get(SEARCH_WATCH_NOW)) }, { markWatched(item, nav, dismiss) })
    DropdownMenuItem({ Text(text = get(SEARCH_WATCH_LATER_ADD)) }, { addToWatchLater(item, nav, dismiss) })
}

@Composable fun SearchMenuItems(item: SearchResultMulti, nav: NavController, dismiss: Handler) {
    DropdownMenuItem({ Text(text = get(SEARCH_WATCH_NOW)) }, { markWatched(item, nav, dismiss) })
    DropdownMenuItem({ Text(text = get(SEARCH_WATCH_LATER_ADD)) }, { addToWatchLater(item, nav, dismiss) })
}

@Composable fun SearchMenuItems(item: SearchResultTv, nav: NavController, dismiss: Handler) {
    DropdownMenuItem({ Text(text = get(SEARCH_WATCH_NOW)) }, { markWatched(item, nav, dismiss) })
    DropdownMenuItem({ Text(text = get(SEARCH_WATCH_LATER_ADD)) }, { addToWatchLater(item, nav, dismiss) })
}
