package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.video.VideoDetailArgs
import com.pajato.argus.android.ui.video.VideoViewModel
import com.pajato.argus.android.ui.video.screen.VideoDetailsScreen
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.uc.MovieUseCases.getMoviePosterPath
import com.pajato.argus.info.uc.TvUseCases.getTvPosterPath
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Composable fun TitleBox(modifier: Modifier, params: DetailParams) {
    var expanded by remember { mutableStateOf(false) }
    val onLongClick = { expanded = true }
    fun extend() = modifier.padding(horizontal = 1.dp).extend({ showDetails(params) }, onLongClick)
    extend().TitleBox(params, expanded) { expanded = false }
}

@Composable private fun Modifier.TitleBox(params: DetailParams, expanded: Boolean, dismiss: Handler) {
    val path = remember { mutableStateOf("") }
    fun updatePathForVideoMaybe() { path.value = getPosterPath(ArgusInfoRepo, params.video) }
    LaunchedEffect(params.video) { withContext(Dispatchers.IO) { updatePathForVideoMaybe() } }
    Box(this, Center) { Title(params, path.value, expanded, dismiss) }
}

private fun showDetails(params: DetailParams) {
    VideoViewModel.detailArgs = VideoDetailArgs(params.coordinator, params.video)
    params.nav.navigate(VideoDetailsScreen.ROUTE)
}

@OptIn(ExperimentalFoundationApi::class)
fun Modifier.extend(onClick: Handler, onLongClick: Handler): Modifier =
    fillMaxSize().combinedClickable(onClick = onClick, onLongClick = onLongClick)

internal fun getPosterPath(repo: InfoRepo, video: Video) =
    if (video.isMovie()) getMoviePosterPath(repo, video.infoId) else getTvPosterPath(repo, video.infoId)
