package com.pajato.argus.android.ui.video.component

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.coordinator.core.Shelves

@Composable fun DropDownMenuItems(params: DetailParams, dismiss: Handler) {
    WatchMenuItem(params, dismiss)
    RejectMenuItem(params, dismiss)
    if (hasWaitListMenuItem(params)) WaitlistMenuItem(params, dismiss)
    TransferMenuItems(params, dismiss)
    OtherMenuItems(params, dismiss, LocalContext.current)
}

fun hasWaitListMenuItem(params: DetailParams): Boolean = isWatchNextShelf(params)

fun isWatchNextShelf(params: DetailParams): Boolean = params.coordinator.shelf.id == Shelves.WatchNext.name
