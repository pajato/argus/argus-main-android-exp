package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.navigation.NavController
import com.pajato.argus.android.R

@Composable fun ArgusMainScreen(nav: NavController) {
    Column(Modifier.fillMaxSize().background(Color(0xFF180E36))) { ArgusMainScreenContent(nav) }
}

@Composable private fun ArgusMainScreenContent(nav: NavController) {
    ShelvesHeader(title = "Argus", nav, painterResource(R.drawable.argus_reverso))
    FilterBar(nav)
    VideoShelvesScroller(nav, rememberLazyListState())
}
