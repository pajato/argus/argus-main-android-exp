package com.pajato.argus.android.ui.common.component

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.ui.graphics.Color

internal val selectedPair: Pair<Color, AnimationSpec<Color>> =
    Pair(Color(0xFFA0A1C2), tween(100, 0, LinearOutSlowInEasing))

internal val notSelectedPair: Pair<Color, AnimationSpec<Color>> =
    Pair(Color(0XFF423460).copy(0.5f), tween(50, 0, LinearOutSlowInEasing))
