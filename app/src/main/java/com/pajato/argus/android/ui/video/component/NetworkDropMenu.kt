package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NetworkDropMenu(networkState: MenuState, modifier: Modifier) {
    val (options, expanded, selected) = networkState
    fun selectItem(item: String) { networkSelectItem(item, networkState) }
    ExposedDropdownMenuBox(expanded.value, { expanded.value = !expanded.value }, modifier) {
        EditVideoDropMenuLabel(selected.value, "Network", expanded.value)
        EditVideoDropMenuItems(options, expanded.value, { expanded.value = !expanded.value }, ::selectItem)
    }
}

private fun networkSelectItem(item: String, profileState: MenuState) {
    val (_, expanded, selected) = profileState
    selectItem(item, selected, expanded, "network")
}
