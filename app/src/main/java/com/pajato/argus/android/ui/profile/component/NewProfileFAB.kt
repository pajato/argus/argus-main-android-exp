package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.profile.I18nStrings.PROFILE_ADD_FAB_DESCRIPTION
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.android.ui.profile.screen.EditProfileScreen
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.core.Profile
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.i18n.strings.StringsResource

@Composable fun NewProfileFAB(nav: NavController) {
    val fabDesc = StringsResource.get(PROFILE_ADD_FAB_DESCRIPTION)
    val fabModifier = Modifier.padding(top = 48.dp, end = 16.dp)
    FloatingActionButton( { addNewProfile(nav) }, fabModifier) { Icon(Icons.Filled.Add, fabDesc) }
}

private fun addNewProfile(nav: NavController) {
    val profile = Profile(1 + ArgusProfileRepo.cache.values.maxOf { it.profile.id })
    ProfileViewModel.itemUnderEdit = SelectableProfile(true, false, profile)
    nav.navigate(EditProfileScreen.ROUTE)
}
