package com.pajato.argus.android.ui.video.component

import androidx.navigation.NavController
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.video.Video

data class DetailParams(val coordinator: Coordinator, val video: Video, val nav: NavController)
