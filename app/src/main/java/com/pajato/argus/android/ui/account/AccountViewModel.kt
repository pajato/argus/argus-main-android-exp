package com.pajato.argus.android.ui.account

import android.Manifest
import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import android.content.pm.PackageManager
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object AccountViewModel : ViewModel() {
    private val _defaultAccount = mutableStateOf<Account?>(null)
    // val defaultAccount get() = _defaultAccount

    private val _permissionState = mutableStateOf(false)
    // val permissionState get() = _permissionState

    fun initAccountViewModel(context: Context) {
        when (PackageManager.PERMISSION_GRANTED) {
            context.checkSelfPermission(Manifest.permission.GET_ACCOUNTS) -> { _permissionState.value = true }
            else -> {}
        }
        viewModelScope.launch(Dispatchers.IO) {
            val accountManager = AccountManager.get(context)
            val accounts = accountManager.accounts
            if (accounts.isNotEmpty()) { _defaultAccount.value = accounts[0] }
        }
    }
}
