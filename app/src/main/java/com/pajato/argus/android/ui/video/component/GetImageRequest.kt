package com.pajato.argus.android.ui.video.component

import android.content.Context
import coil.request.ImageRequest

internal fun getImageRequest(url: String, context: Context): ImageRequest {
    return ImageRequest.Builder(context).data(url).crossfade(true).build()
}
