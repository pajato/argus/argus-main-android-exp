package com.pajato.argus.android.ui.common.component

import android.content.Context
import android.widget.Toast

internal fun showToast(context: Context, text: String) = Toast.makeText(context, text, Toast.LENGTH_LONG).show()
