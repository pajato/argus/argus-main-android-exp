package com.pajato.argus.android.ui.network.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@ExperimentalMaterial3Api
@Composable
fun LabelTextField(labelState: String, onValueChanged: (String) -> Unit) {
    val label = @Composable { Text("Enter label") }
    OutlinedTextField(labelState, onValueChanged, Modifier.padding(8.dp).fillMaxWidth(), label = label)
}
