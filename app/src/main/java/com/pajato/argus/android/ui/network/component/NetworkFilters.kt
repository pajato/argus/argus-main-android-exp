package com.pajato.argus.android.ui.network.component

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults.filterChipColors
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.network.NetworkViewModel
import com.pajato.argus.android.ui.network.NetworkViewModel.networks
import com.pajato.argus.android.ui.network.event.ToggleSelectedNetwork
import com.pajato.argus.network.core.SelectableNetwork

@Composable fun NetworkFilters() {
    val list: List<SelectableNetwork> = networks.filter { !it.isHidden }
    val listState = rememberLazyListState()
    LazyRow(Modifier.padding(start = 2.dp), listState) { items(list.size) { NetworkFilterChip(list, it) } }
}

@Composable private fun NetworkFilterChip(list: List<SelectableNetwork>, it: Int) {
    val colors = filterChipColors(selectedContainerColor = White, selectedLabelColor = Black)
    val text = @Composable { Text(list[it].network.shortLabel) }
    fun onClick() { NetworkViewModel.onEvent(ToggleSelectedNetwork(list[it])) }
    FilterChip(list[it].isSelected, ::onClick, text, Modifier.padding(horizontal = 4.dp), colors = colors)
}
