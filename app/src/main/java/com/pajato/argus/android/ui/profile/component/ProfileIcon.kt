package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.android.ui.theme.PurpleGrey40
import java.util.Locale

@Composable fun ProfileIcon(label: String, modifier: Modifier, size: Int = -1) {
    val circleModifier = modifier.drawBehind { drawCircle(color = PurpleGrey40, radius = getRadius(size)) }
    val fontSize: TextUnit = if (size > 0) 16.sp else 96.sp
    if (label.isEmpty()) return
    Text(getInitials(label), circleModifier.width(56.dp), textAlign = Center, fontSize = fontSize)
}

private fun DrawScope.getRadius(size: Int): Float {
    val radius = if (size > 0) size.toFloat() else this.size.minDimension * 2
    return radius / 2
}

private fun getInitials(label: String): String {
    val parts = label.split(" ")
    val initials = parts.joinToString("") { if (it.length > 1) it.substring(0, 1) else "" }
    val result = if (initials.length > 2) "${initials.substring(0, 2)}.." else initials
    return result.uppercase(Locale.getDefault())
}
