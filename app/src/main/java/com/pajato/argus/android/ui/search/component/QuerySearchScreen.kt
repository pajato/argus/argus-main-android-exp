package com.pajato.argus.android.ui.search.component

import android.annotation.SuppressLint
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.focus.FocusRequester
import androidx.navigation.NavController
import com.pajato.argus.android.ui.search.SearchBarParams

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun QuerySearchScreen(nav: NavController) = Scaffold {
    val text = remember { mutableStateOf("") }
    val active = remember { mutableStateOf(false) }
    val focusRequester = remember { FocusRequester() }
    SearchBarParams(nav, text, active).ArgusSearchBar(focusRequester)
}
