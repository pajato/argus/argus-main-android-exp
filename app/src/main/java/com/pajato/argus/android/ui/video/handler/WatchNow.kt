package com.pajato.argus.android.ui.video.handler

import android.content.Context
import android.content.Intent
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.common.component.showToast
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.android.ui.video.component.MessageParams
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.argus.network.uc.NetworkUseCases.getAll

fun watchNow(context: Context, params: DetailParams, dismiss: Handler) {
    val label = getAll(ArgusNetworkRepo).find { it.network.id == params.video.networkId }?.network?.label ?: ""
    val name = getAll(ArgusNetworkRepo).find { it.network.id == params.video.networkId }?.network?.packageId ?: ""
    val intent = context.packageManager.getLaunchIntentForPackage(name)
    if (launch(label, context, intent)) markWatched(params, dismiss) else dismiss()
}

private fun launch(label: String, context: Context, intent: Intent?): Boolean {
    val messageParams = getMessageParams(context, label, intent)
    return isValid(messageParams)
}

private fun getMessageParams(context: Context, label: String, intent: Intent?): MessageParams {
    val invalidMessage = context.getString(R.string.network_invalid)
    val notInstalledMessage = "$label ${context.getString(R.string.network_not_installed)}"
    return MessageParams(context, label, intent, invalidMessage, notInstalledMessage)
}

private fun isValid(params: MessageParams) = when {
    params.label.isEmpty() -> { showToast(params.context, params.invalidMessage); false }
    params.intent == null -> { showToast(params.context, params.notInstalledMessage); false }
    startActivity(params.context, params.intent) -> { showToast(params.context, params.notInstalledMessage); false }
    else -> true
}

private fun startActivity(context: Context, intent: Intent): Boolean {
    var errorEncountered = false
    try { context.startActivity(intent) } catch (exc: Exception) { errorEncountered = true }
    return errorEncountered
}
