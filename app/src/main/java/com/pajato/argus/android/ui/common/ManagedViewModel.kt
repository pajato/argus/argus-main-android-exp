package com.pajato.argus.android.ui.common

interface ManagedViewModel {
    fun getSelected(label: String): Boolean
    fun setSelected(label: String, selected: Boolean)
    fun getHidden(label: String): Boolean
    fun setHidden(label: String, hidden: Boolean)
}
