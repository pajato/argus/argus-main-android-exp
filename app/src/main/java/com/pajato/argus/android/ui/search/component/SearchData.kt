package com.pajato.argus.android.ui.search.component

data class SearchData(
    val id: Int,
    val backdropPath: String,
    val posterPath: String,
    val overview: String,
    val name: String,
    val date: String,
    val voteAverage: Double,
    val voteCount: Int,
    val votePopularity: Double,
    val hasSeasons: Boolean
)
