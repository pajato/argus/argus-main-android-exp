package com.pajato.argus.android.ui.profile

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.common.ManagedViewModel
import com.pajato.argus.android.ui.profile.event.CreateProfile
import com.pajato.argus.android.ui.profile.event.HideProfile
import com.pajato.argus.android.ui.profile.event.ModifyProfile
import com.pajato.argus.android.ui.profile.event.ProfileEvent
import com.pajato.argus.android.ui.profile.event.ToggleProfileSelection
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.adapter.ArgusProfileRepo.cache
import com.pajato.argus.profile.adapter.ArgusProfileRepo.register
import com.pajato.argus.profile.core.Profile
import com.pajato.argus.profile.core.SelectableProfile
import java.net.URI
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

object ProfileViewModel : ManagedViewModel, ViewModel() {
    private var profileStateList = listOf<SelectableProfile>().toMutableStateList() // aka _profiles

    val profiles: List<SelectableProfile> get() = profileStateList
    val selectedProfileIds: List<Int> get() = profiles.filter { it.isSelected }.map { it.profile.id }

    val defaultProfile: SelectableProfile get() = profiles.firstOrNull { it.isSelected } ?: profiles.first()

    private val defaultItem: SelectableProfile = SelectableProfile(false, false, Profile(-1))

    var itemUnderEdit: SelectableProfile = defaultItem

    private var errorState: MutableState<Boolean> = mutableStateOf(false)

    override fun getSelected(label: String): Boolean =
        profileStateList.find { it.profile.label == label }?.isSelected ?: false

    override fun setSelected(label: String, selected: Boolean) {
        val item = profileStateList.find { it.profile.label == label } ?: return
        handleAddProfile(item.copy(isSelected = selected))
    }

    override fun getHidden(label: String): Boolean =
        profileStateList.find { it.profile.label == label }?.isHidden ?: false

    override fun setHidden(label: String, hidden: Boolean) {
        val item = profileStateList.find { it.profile.label == label } ?: return
        handleAddProfile(item.copy(isHidden = hidden))
    }

    fun hasError(label: String): Boolean {
        errorState.value = profiles.any { it.profile.label == label }
        return errorState.value
    }

    fun loadProfileData(uri: URI) {
        viewModelScope.launch(IO) {
            ArgusProfileRepo.injectDependency(uri)
            profileStateList.clear()
            profileStateList.addAll(cache.values)
        }
    }

    fun onEvent(event: ProfileEvent) = when (event) {
        is CreateProfile -> handleAddProfile(event.profile)
        is HideProfile -> hideProfile(event.profile)
        is ModifyProfile -> handleModifyProfile(event.profile)
        is ToggleProfileSelection -> toggleProfileSelection(event.profile)
    }

    private fun handleAddProfile(profile: SelectableProfile) {
        register(profile)
        reloadAll()
    }

    private fun hideProfile(profile: SelectableProfile) {
        register(profile.copy(isHidden = true))
        reloadAll()
    }

    private fun toggleProfileSelection(item: SelectableProfile) {
        if (item.isSelected) register(item.copy(isSelected = false)) else selectItem(item)  // for single select
        reloadAll()
    }

    private fun selectItem(item: SelectableProfile) {
        cache.values.forEach { register(it.copy(isSelected = false)) }
        register(item.copy(isSelected = true))
    }

    private fun reloadAll() {
        profileStateList.clear()
        profileStateList.addAll(cache.values.toMutableStateList())
        viewModelScope.launch(IO) { reloadShelfDataInOrder() }
    }

    private fun handleModifyProfile(profile: SelectableProfile) { register(profile) }

    fun select(profile: SelectableProfile) { toggleProfileSelection(profile) }

    fun deselect(profile: SelectableProfile) { toggleProfileSelection(profile) }
}
