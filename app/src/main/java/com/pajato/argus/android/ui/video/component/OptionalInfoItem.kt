package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.Timestamp
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases
import com.pajato.argus.info.uc.CommonUseCases.getNetworkName
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant.Companion.fromEpochMilliseconds
import kotlinx.datetime.TimeZone.Companion.currentSystemDefault
import kotlinx.datetime.toLocalDateTime

@Composable internal fun OptionalInfoItem(shelf: Shelf, video: Video) = when (shelf.id) {
    Shelves.WatchNext.name -> NetworkText(video)
    Shelves.WatchLater.name -> NetworkText(video)
    Shelves.LastWatched.name -> DateText(video)
    else -> {}
}

@Composable private fun NetworkText(video: Video) {
    Text(getNetworkName(ArgusNetworkRepo, video.networkId), Modifier, White, 12.sp, textAlign = Center)
}

@Composable private fun DateText(video: Video) {
    val modifier = Modifier.width(CardWidth.dp)
    Text(formatDay(video), modifier, White, 12.sp, textAlign = Center, overflow = TextOverflow.Ellipsis, maxLines = 1)
}

fun formatDay(video: Video): String {
    val today = getEpochDay(Clock.System.now().toEpochMilliseconds())
    val videoDay = getEpochDay(video.timestamp)
    return getShortestDateString(videoDay, today, video)
}

private fun getShortestDateString(videoDay: Int, today: Int, video: Video) = when {
    videoDay == today -> "Today"
    videoDay == today - 1 -> "Yesterday"
    today.minus(videoDay) <= 7 -> "${today - videoDay} days ago"
    else -> CoordinatorUseCases.formatTimestamp(video)
}

private fun getEpochDay(timestamp: Timestamp): Int =
    fromEpochMilliseconds(timestamp).toLocalDateTime(currentSystemDefault()).date.toEpochDays()
