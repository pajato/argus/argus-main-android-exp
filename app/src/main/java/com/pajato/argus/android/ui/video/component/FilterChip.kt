package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.common.Handler

@Composable fun FilterChip(text: String, onClick: Handler) {
    val modifier = Modifier.height(32.dp).padding(start = 16.dp).clickable { onClick() }
    Box(modifier, Alignment.Center) { Text(text, Modifier.padding(horizontal = 8.dp)) }}
