package com.pajato.argus.android.ui.filter.component

import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.component.NavHeader
import com.pajato.argus.android.ui.filter.Done
import com.pajato.argus.android.ui.filter.FilterViewModel
import com.pajato.argus.android.ui.filter.FilterViewModel.selectedQuery
import com.pajato.argus.android.ui.filter.QueryCB
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.network.component.NetworkFilters

@Composable fun SelectFiltersScreen(nav: NavController) = Column(Modifier.fillMaxSize()) {
    val query = remember { mutableStateOf(selectedQuery.value.text.ifEmpty { "" }) }
    val onValueChanged: QueryCB = { query.value = it }
    SelectFiltersHeader("Select Video Filters", nav, query)
    SelectFiltersBody(nav, query.value, onValueChanged) { onDone(nav, query.value) }
}

@Composable fun SelectFiltersHeader(title: String, nav: NavController, query: MutableState<String>) {
    val start = Pair(NavRoute.BackToVideos) { nav.navigate(NavRoute.BackToVideos.route) }
    val end = Pair(NavRoute.Done) { onDone(nav, query.value) }
    Row(Modifier.fillMaxWidth().height(52.dp), SpaceBetween, CenterVertically) { NavHeader(title, start, end) }
}

@Composable fun SelectFiltersBody(nav: NavController, value: String, onValueChanged: QueryCB, onDone: Done) {
    Column(Modifier.fillMaxSize()) {
        Text("Genres: coming soon...")
        Row(Modifier.padding(8.dp, 12.dp, 0.dp, 4.dp)) { NetworkFilters() }
        Text("Profiles: coming soon...")
        QueryFilter(value, onValueChanged, onDone)
    }
}

private fun onDone(nav: NavController, query: String) {
    FilterViewModel.setSearchQuery(query)
    goToVideosScreen(nav)
}

@Composable internal fun QueryFilter(query: String, changed: QueryCB, onDone: Done) {
    Column(Modifier.fillMaxSize()) { QueryFilterItems(query, changed, onDone) }
}
