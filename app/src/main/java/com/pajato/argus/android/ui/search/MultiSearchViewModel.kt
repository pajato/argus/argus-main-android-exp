package com.pajato.argus.android.ui.search

import com.pajato.tks.search.multi.adapter.TmdbSearchMultiRepo
import com.pajato.tks.search.pager.core.SearchResultMulti

object MultiSearchViewModel : AbstractSearchViewModel<SearchResultMulti>(TmdbSearchMultiRepo::getSearchPageMulti)
