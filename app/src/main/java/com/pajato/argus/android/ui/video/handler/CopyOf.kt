package com.pajato.argus.android.ui.video.handler

import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.network.core.Network
import com.pajato.argus.profile.core.Profile

internal fun copyOf(video: Video, network: Network) = video.copy(networkId = network.id)

internal fun copyOf(video: Video, profile: Profile) = video.copy(profileId = profile.id)
