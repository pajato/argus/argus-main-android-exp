package com.pajato.argus.android.ui.search.component

import androidx.compose.material3.DropdownMenu
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultMulti
import com.pajato.tks.search.pager.core.SearchResultTv

@Composable fun SearchItemMenu(item: SearchResultMovie, nav: NavController, expanded: Boolean, dismiss: Handler) =
    DropdownMenu(expanded, dismiss) { SearchMenuItems(item, nav, dismiss) }

@Composable fun SearchItemMenu(item: SearchResultMulti, nav: NavController, expanded: Boolean, dismiss: Handler) =
    DropdownMenu(expanded, dismiss) { SearchMenuItems(item, nav, dismiss) }

@Composable fun SearchItemMenu(item: SearchResultTv, nav: NavController, expanded: Boolean, dismiss: Handler) =
    DropdownMenu(expanded, dismiss) { SearchMenuItems(item, nav, dismiss) }
