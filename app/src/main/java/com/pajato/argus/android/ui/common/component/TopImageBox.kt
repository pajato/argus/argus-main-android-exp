package com.pajato.argus.android.ui.common.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable fun TopImageBox(drawable: Int, description: String) =
    Box(modifier = Modifier.fillMaxWidth().height(256.dp)) {
        Image(painterResource(drawable), description, Modifier.fillMaxWidth(), contentScale = ContentScale.FillWidth)
    }
