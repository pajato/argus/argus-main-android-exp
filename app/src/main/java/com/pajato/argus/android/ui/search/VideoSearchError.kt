package com.pajato.argus.android.ui.search

class VideoSearchError(message: String) : Exception(message)
