package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.search.screen.CardWidth

@OptIn(ExperimentalFoundationApi::class)
internal fun Modifier.extend(onClick: Handler, onLongClick: Handler): Modifier = this.fillMaxSize()
    .width(CardWidth.dp).combinedClickable(onClick = onClick, onLongClick = onLongClick)
