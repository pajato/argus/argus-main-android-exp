package com.pajato.argus.android.ui.video.handler

import com.pajato.argus.coordinator.core.video.Video
import java.time.Instant

internal fun Video.copyAsNow(): Video = copy(Instant.now().toEpochMilli())
