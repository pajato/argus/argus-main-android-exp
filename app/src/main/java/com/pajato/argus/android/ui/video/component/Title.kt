package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.DropdownMenu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.common.Handler

@Composable fun Title(params: DetailParams, path: String, expanded: Boolean, dismiss: Handler) {
    val modifier = Modifier.width(CardWidth.dp).fillMaxWidth()
    val video = params.video
    if (path != "") TitleImage(video, modifier) else TitleText(video, Modifier.padding(horizontal = 8.dp))
    DropdownMenu(expanded, dismiss) { DropDownMenuItems(params, dismiss) }
}
