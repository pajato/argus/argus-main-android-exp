package com.pajato.argus.android.ui.network

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.common.ManagedViewModel
import com.pajato.argus.android.ui.network.event.NetworkEvent
import com.pajato.argus.android.ui.network.event.ToggleHiddenNetwork
import com.pajato.argus.android.ui.network.event.ToggleSelectedNetwork
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.argus.network.core.SelectableNetwork
import java.io.File
import java.net.URI
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

object NetworkViewModel : ManagedViewModel, ViewModel() {
    private var networkStateList = listOf<SelectableNetwork>().toMutableStateList() // aka _networks

    val networks: List<SelectableNetwork> get() = networkStateList
    val selectedNetworkIds: List<Int> get() = networks.filter { it.isSelected }.map { it.network.id }

    val selectedNetwork: SelectableNetwork get() = networks.firstOrNull { it.isSelected } ?: networks.first()

    private var errorState: MutableState<Boolean> = mutableStateOf(false)

    override fun getSelected(label: String): Boolean =
        networkStateList.find { it.network.label == label }?.isSelected ?: false

    override fun setSelected(label: String, selected: Boolean) {
        val item = networkStateList.find { it.network.label == label } ?: return
        reloadData(item.copy(isSelected = selected))
    }

    override fun getHidden(label: String): Boolean =
        networkStateList.find { it.network.label == label }?.isHidden ?: false

    override fun setHidden(label: String, hidden: Boolean) {
        val item = networkStateList.find { it.network.label == label } ?: return
        reloadData(item.copy(isHidden = hidden))
    }

    fun hasError(label: String): Boolean {
        errorState.value = ArgusNetworkRepo.cache.values.none { it.network.label == label }
        return errorState.value
    }

    fun loadNetworkData(uri: URI) {
        viewModelScope.launch(IO) {
            runTemporaryHack(uri)
            ArgusNetworkRepo.injectDependency(uri)
            networkStateList.clear()
            networkStateList.addAll(ArgusNetworkRepo.cache.values.toMutableStateList())
        }
    }

    private fun runTemporaryHack(uri: URI) {
        val file: File = uri.toURL().file.let { File(it) }
        file.writeText("")
        val networks = getBruteForceNetworks()
        networks.forEach { file.appendText("$it\n") }
    }

    fun onEvent(event: NetworkEvent) = when (event) {
        is ToggleSelectedNetwork -> toggleSelected(event.network)
        is ToggleHiddenNetwork -> toggleHidden(event.network)
    }

    private fun toggleSelected(item: SelectableNetwork) = reloadData(item.copy(isSelected = item.isSelected.not()))

    private fun toggleHidden(item: SelectableNetwork) = reloadData(item.copy(isHidden = item.isHidden.not()))

    private fun reloadData(item: SelectableNetwork) {
        ArgusNetworkRepo.register(item)
        networkStateList.clear()
        networkStateList.addAll(ArgusNetworkRepo.cache.values.toMutableStateList())
        viewModelScope.launch(IO) { reloadShelfDataInOrder() }
    }

    private fun getBruteForceNetworks(): List<String> = listOf(
        """{"isSelected":false,"isHidden":false,"network":{"id":87,"packageId":"","label":"Acorn TV","shortLabel":"Acorn","iconUrl":"https://www.themoviedb.org/t/p/original/doCc555FPPgGtuaZJxf9QZVpIp5.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":80,"packageId":"","label":"AMC","shortLabel":"AMC","iconUrl":"https://www.themoviedb.org/t/p/original/92Kx25Od0habmgRBTqT6XWgwgKt.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":526,"packageId":"","label":"AMC+","shortLabel":"AMC+","iconUrl":"https://www.themoviedb.org/t/p/original/ovmu6uot1XVvsemM2dDySXLiX57.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":2,"packageId":"","label":"Apple TV","shortLabel":"AppleTV","iconUrl":"https://www.themoviedb.org/t/p/original/9ghgSC0MA082EL6HLCW3GalykFD.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":350,"packageId":"com.apple.atve.androidtv.appletv","label":"Apple TV Plus","shortLabel":"AppleTV+","iconUrl":"https://www.themoviedb.org/t/p/original/2E03IAZsX4ZaUqM7tXlctEPMGWS.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":151,"packageId":"","label":"BritBox","shortLabel":"BritBox","iconUrl":"https://www.themoviedb.org/t/p/original/d2Kx5XtHcd0DBwVwIe7D3X7Sifp.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234501,"packageId":"","label":"Criterion Channel","shortLabel":"Criterion","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234502,"packageId":"","label":"Crunchy Roll","shortLabel":"Crunchy","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234503,"packageId":"","label":"DirecTv Stream","shortLabel":"DirectTv","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":2739,"packageId":"com.disney.disneyplus","label":"Disney+","shortLabel":"Disney+","iconUrl":"https://www.themoviedb.org/t/p/original/7rwgEs15tFwyR9NPQ5vpzxTj19Q.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234504,"packageId":"","label":"ESPN+","shortLabel":"ESPN+","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234505,"packageId":"","label":"fuboTV","shortLabel":"fuboTV","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234506,"packageId":"","label":"Funimation","shortLabel":"Funimation","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":2853,"packageId":"com.feeln.android","label":"Hallmark Movies Now","shortLabel":"HMN","iconUrl":"https://www.themoviedb.org/t/p/original/llEJ6av9kAniTQUR9hF9mhVbzlB.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":453,"packageId":"com.hulu.plus","label":"Hulu","shortLabel":"Hulu","iconUrl":"https://www.themoviedb.org/t/p/original/zxrVdFjIjLqkfnwyghnfywTn3Lh.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":49,"packageId":"com.hbo.hbonow","label":"Max","shortLabel":"Max","iconUrl":"https://www.themoviedb.org/t/p/original/Ajqyt5aNxNGjmF9uOfxArGrdf3X.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":209,"packageId":"com.pbs.video","label":"PBS","shortLabel":"PBS","iconUrl":"https://www.themoviedb.org/t/p/original/bbxgdl6B5T75wJE713BiTCIBXyS.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":6,"packageId":"com.nbcuni.nbc","label":"NBC","shortLabel":"NBC","iconUrl":"https://www.themoviedb.org/t/p/original/wSAxtofaArEuTOsqBmghVuJx7eP.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":213,"packageId":"com.netflix.mediaclient","label":"Netflix","shortLabel":"Netflix","iconUrl":"https://www.themoviedb.org/t/p/original/t2yyOv40HZeVlLjYsCsPHnWLk4W.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":31234,"packageId":"com.cbs.app","label":"Paramount +","shortLabel":"Paramount+","iconUrl":"https://www.themoviedb.org/t/p/original/8VCV78prwd9QzZnEm0ReO6bERDa.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":3353,"packageId":"com.peacocktv.peacockandroid","label":"Peacock","shortLabel":"Peacock","iconUrl":"https://www.themoviedb.org/t/p/original/8VCV78prwd9QzZnEm0ReO6bERDa.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1024,"packageId":"com.amazon.avod.thirdpartyclient","label":"Prime Video","shortLabel":"PrimeVideo","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234507,"packageId":"","label":"Pluto TV","shortLabel":"Pluto","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234508,"packageId":"","label":"Shudder","shortLabel":"Shudder","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234509,"packageId":"","label":"Sling TV","shortLabel":"Sling","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234510,"packageId":"","label":"Tubi","shortLabel":"Tubi","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
        """{"isSelected":false,"isHidden":false,"network":{"id":1234511,"packageId":"","label":"YouTube TV","shortLabel":"YT TV","iconUrl":"https://www.themoviedb.org/t/p/original/emthp39XA2YScoYL1p0sdbAH2WA.jpg"}}""",
    )
}
