package com.pajato.argus.android.main

import android.util.Log
import com.pajato.tks.common.adapter.TmdbFetcher
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.get
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.net.URI

internal fun initInfoService(uri: URI) = runBlocking {
    val apiKey = getApiKey(uri)
    TmdbFetcher.inject(::getResponse, ::handleError)
    TmdbFetcher.inject(apiKey)
}

private fun getApiKey(uri: URI): String = "dfd0ee93d011ad9182a9d59930215c02"

private fun handleError(message: String, exc: Exception?) {
    val module = "AndroidMain#handleError"
    if (exc != null) Log.d(module, "Failed with message: '$message' and exception: '$exc'")
    if (exc != null) Log.d(module, exc.stackTraceToString())
}

private fun getResponse(tmdbUrl: String): String {
    val result = runBlocking(Dispatchers.IO) { HttpClient(CIO).get(tmdbUrl).body<String>() }
    if (result.isEmpty()) Log.d("AndroidMain", "Empty response for request: $tmdbUrl")
    return result
}
