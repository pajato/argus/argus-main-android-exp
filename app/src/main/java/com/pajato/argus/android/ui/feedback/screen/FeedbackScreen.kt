package com.pajato.argus.android.ui.feedback.screen

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.OutlinedTextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.component.TopImageBox
import com.pajato.argus.android.ui.feedback.component.Header
import com.pajato.argus.android.ui.feedback.component.HeadingText
import com.pajato.argus.android.ui.feedback.component.TextFieldLabel

object FeedbackScreen { const val ROUTE = "FeedbackScreen" }

@Composable fun FeedbackScreen(nav: NavHostController) {
    val context = LocalContext.current
    val title = context.getString(R.string.app_name)
    val modifier = Modifier.fillMaxWidth().padding(vertical = 16.dp)
    Column(Modifier.fillMaxWidth()) { Sections(nav, context, title, modifier) }
}

@Composable private fun Sections(nav: NavHostController, context: Context, title: String, modifier: Modifier) {
    var feedbackText by remember { mutableStateOf("") }
    Header(nav, context, title, feedbackText)
    TopImageBox(R.drawable.feedback, context.getString(R.string.feedback_image_description))
    HeadingText(context, title)
    OutlinedTextField(feedbackText, { feedbackText = it }, modifier, label = { TextFieldLabel(context) })
}
