package com.pajato.argus.android.ui.account.component

import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material3.Text
import androidx.compose.ui.Modifier
import com.pajato.argus.android.BuildConfig
import com.pajato.argus.android.ui.search.component.ExpandableText
import com.pajato.argus.android.ui.search.component.TextParams

internal fun LazyListScope.AboutScreenItems(modifier: Modifier, params: TextParams) {
    item { ExpandableText(modifier, params) }
    item { Text("Version: ${BuildConfig.VERSION_NAME}", modifier) }
    item { Text("Developer: Pajato Technologies LLC", modifier) }
    item { Text("Contact: support@pajato.com", modifier) }
    item { Text("Features: ", modifier) }
    item { Text("Acknowledgements", modifier) }
    item { Text("Special Contributions", modifier) }
    item { Text("Third Party Libraries Used", modifier) }
    item { Text("Influential Third Party Apps", modifier) }
}
