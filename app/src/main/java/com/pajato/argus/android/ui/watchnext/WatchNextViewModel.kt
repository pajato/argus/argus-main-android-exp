package com.pajato.argus.android.ui.watchnext

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.finished.FinishedViewModel
import com.pajato.argus.android.ui.genre.GenreViewModel.selectedGenreIds
import com.pajato.argus.android.ui.rejected.RejectedViewModel
import com.pajato.argus.android.ui.history.HistoryViewModel.lastWatchedSeries
import com.pajato.argus.android.ui.network.NetworkViewModel.selectedNetworkIds
import com.pajato.argus.android.ui.paused.PausedViewModel
import com.pajato.argus.android.ui.profile.ProfileViewModel.selectedProfileIds
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.android.ui.waiting.WaitingViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.cache
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.isLastEpisodeInSeries
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

object WatchNextViewModel : ShelfViewModel, ViewModel() {
    override val id: String = Shelves.WatchNext.name

    override val coordinator: Coordinator = cache.values.first { it.shelf.id == id }

    override val videos: List<Video> get() = videosStateList.reversed()
    private var videosStateList = listOf<Video>().toMutableStateList() // aka _videos

    override fun loadData() {
        val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds )
        val list = coordinator.repo.filter(filters, ArgusInfoRepo, getWatchNextVideos())
        val videosToAdd = list.toMutableStateList()
        list.forEach { coordinator.repo.register(it) }
        resetStateLists(videosToAdd)
    }

    private fun resetStateLists(videosToAdd: SnapshotStateList<Video>) {
        videosStateList.clear()
        videosStateList.addAll(videosToAdd)
    }

    override fun runUseCase(handler: suspend () -> Unit) {
        val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds )
        viewModelScope.launch(IO) {
            handler()
            resetStateLists(coordinator.repo.filter(filters, ArgusInfoRepo).toMutableStateList())
        }
    }

    private fun getWatchNextVideos(): List<Video> =
        lastWatchedSeries.filter { it.isActive() }.map { it.getNextVideo() }

    private fun Video.isActive(): Boolean = (RejectedViewModel.videos.any { it.infoId == this.infoId }
            || WaitingViewModel.videos.any { it.infoId == this.infoId }
            || FinishedViewModel.videos.any { it.infoId == this.infoId }
            || PausedViewModel.videos.any { it.infoId == this.infoId }).not()

    private fun Video.getNextVideo(): Video {
        fun bumpSeriesMaybe() = isLastEpisodeInSeries(ArgusInfoRepo, InfoKey(Tv.name, infoId), series, episode)
        return if (bumpSeriesMaybe()) copy(series = series + 1, episode = 1) else copy(episode = episode + 1)
    }

    internal fun getPreviousVideo(video: Video): Video =
        lastWatchedSeries.filter { it.isActive() }.first { it.infoId == video.infoId }
}
