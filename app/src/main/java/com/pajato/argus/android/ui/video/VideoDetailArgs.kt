package com.pajato.argus.android.ui.video

import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.video.Video

data class VideoDetailArgs(val coordinator: Coordinator, val video: Video)
