package com.pajato.argus.android.ui.history

import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.genre.GenreViewModel.selectedGenreIds
import com.pajato.argus.android.ui.rejected.RejectedViewModel
import com.pajato.argus.android.ui.network.NetworkViewModel.selectedNetworkIds
import com.pajato.argus.android.ui.profile.ProfileViewModel.selectedProfileIds
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.cache
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_UNREGISTERED_ERROR
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.modifyVideo
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object HistoryViewModel : ShelfViewModel, ViewModel() {
    override val id = Shelves.LastWatched.name

    override val coordinator: Coordinator = cache[id] ?: throw CoordinatorError(getUnregisteredErrorMessage(id))

    private fun getUnregisteredErrorMessage(id: String): String = get(COORDINATOR_UNREGISTERED_ERROR, Arg("id", id))

    override val videos: List<Video> get() = videosStateList.reversed().groupBy { it.infoId }.map { it.value[0] }
    private var videosStateList = listOf<Video>().toMutableStateList() // aka _videos

    private val repo = coordinator.repo

    val lastWatchedSeries: List<Video> get() = videos.filter { it.isTv() && it.isNotHidden() }

    private fun Video.isNotHidden(): Boolean = RejectedViewModel.videos.none { it.infoId == this.infoId }

    override fun loadData() {
        val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds)
        val list = repo.filter(filters, ArgusInfoRepo).filter { it.isNotHidden() }.toMutableStateList()
        videosStateList.clear()
        videosStateList.addAll(list)
    }

    internal fun update(video: Video, newVideo: Video) { runUseCase { modifyVideo(repo, video, newVideo) } }

    override fun runUseCase(handler: suspend () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) { handler(); loadData(); reloadShelfDataInOrder() }
    }
}
