package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.navigation.NavController
import com.pajato.argus.android.ui.search.MovieSearchViewModel
import com.pajato.argus.android.ui.search.MultiSearchViewModel
import com.pajato.argus.android.ui.search.SearchState
import com.pajato.argus.android.ui.search.TvSearchViewModel
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultMulti
import com.pajato.tks.search.pager.core.SearchResultTv
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

@Composable fun MovieSearchShelf(nav: NavController, name: String) {
    var state: SearchState<SearchResultMovie> by remember { mutableStateOf(MovieSearchViewModel.screenState) }
    LaunchedEffect(name) { withContext(IO) { if (name.isNotEmpty()) state = MovieSearchViewModel.getFirstPage(name) } }
    if (state.items.isNotEmpty()) Column { MovieSearchShelfContent(nav, name, state) }
}

@Composable fun TvSearchShelf(nav: NavController, name: String) {
    var state: SearchState<SearchResultTv> by remember { mutableStateOf(TvSearchViewModel.screenState) }
    LaunchedEffect(name) { withContext(IO) { if (name.isNotEmpty()) state = TvSearchViewModel.getFirstPage(name) } }
    if (state.items.isNotEmpty()) Column { TvSearchShelfContent(nav, name, state) }
}

@Composable fun QueryResults(nav: NavController, name: String) {
    var state: SearchState<SearchResultMulti> by remember { mutableStateOf(MultiSearchViewModel.screenState) }
    LaunchedEffect(name) { withContext(IO) { if (name.isNotEmpty()) state = MultiSearchViewModel.getFirstPage(name) } }
    if (state.items.isNotEmpty()) LazyColumn { items(state.items.size) { i -> MultiSearchItem(i, nav, name, state) } }
}
