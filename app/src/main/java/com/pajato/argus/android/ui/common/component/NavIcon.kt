package com.pajato.argus.android.ui.common.component

import androidx.compose.foundation.layout.BoxScope
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Modifier
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.common.NavRouteParam
import com.pajato.argus.android.ui.video.component.getNavModifier

@Composable fun BoxScope.NavIcon(modifier: Modifier, param: NavRouteParam) {
    val handler: Handler = param.second
    val iconModifier = modifier.getNavModifier(handler).align(CenterStart)
    Icon(param.first.icon, param.first.desc, iconModifier)
}

@Composable fun NavIcon(modifier: Modifier, param: NavRouteParam) {
    val handler: Handler = param.second
    val iconModifier = modifier.getNavModifier(handler)
    Icon(param.first.icon, param.first.desc, iconModifier)
}
