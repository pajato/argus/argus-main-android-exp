package com.pajato.argus.android.ui.common.component

import androidx.compose.material3.FilterChipDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@Composable internal fun getFilterChipColors() = FilterChipDefaults.filterChipColors(
    disabledContainerColor = Color.DarkGray, selectedContainerColor = Color.White, selectedLabelColor = Color.Black
)

internal fun getSelected(params: ManageParams, index: Int): Boolean {
    val viewModel = params.viewModel
    return viewModel.getSelected(params.list[index])
}

internal fun getHidden(params: ManageParams, index: Int): Boolean {
    val viewModel = params.viewModel
    return viewModel.getHidden(params.list[index])
}

internal fun toggleSelected(params: ManageParams, index: Int) {
    val viewModel = params.viewModel
    val label = params.list[index]
    viewModel.setSelected(label, !viewModel.getSelected(label))
}

internal fun toggleHidden(params: ManageParams, index: Int) {
    val viewModel = params.viewModel
    val label = params.list[index]
    viewModel.setHidden(label, !viewModel.getHidden(label))
}
