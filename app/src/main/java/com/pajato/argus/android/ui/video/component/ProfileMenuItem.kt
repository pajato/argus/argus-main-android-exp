package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.video.handler.transferProfile
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.uc.ProfileUseCases

@Composable fun ProfileMenuItem(video: Video, dismiss: Handler, mainDismiss: Handler) =
    ProfileUseCases.getAll(ArgusProfileRepo).filter { it.profile.id != video.profileId }.forEach {
        DropdownMenuItem({ Text(it.profile.label) }, { transferProfile(it.profile, video, dismiss, mainDismiss) })
    }
