package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Nav
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.video.Video

@Composable fun ScrollableVideoItems(coordinator: Coordinator, list: List<Video>, nav: Nav) =
    LazyRow(state = rememberLazyListState()) {
        items(list.size) { index -> VideoItem(DetailParams(coordinator, list[index], nav)) }
    }
