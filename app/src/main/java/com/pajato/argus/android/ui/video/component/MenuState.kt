package com.pajato.argus.android.ui.video.component

import com.pajato.argus.android.ui.common.MutableBoolean
import com.pajato.argus.android.ui.common.MutableString
import com.pajato.argus.android.ui.video.Options

data class MenuState(val options: Options, val expanded: MutableBoolean, val selected: MutableString)
