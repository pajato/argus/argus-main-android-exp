package com.pajato.argus.android.ui.search.component

import com.pajato.argus.coordinator.core.InfoId
import com.pajato.argus.coordinator.core.video.Video

internal fun getMovieVideo(id: InfoId): Video = Video(0L, id, 1, -1, 0, 0)

internal fun getTvVideo(id: InfoId): Video = Video(0L, id, 1, 1, 0, 0)
