package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Arrangement.SpaceAround
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.argus.network.uc.NetworkUseCases
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.uc.ProfileUseCases

@Composable fun ProfileNetworkRow(video: Video) {
    val profileState = getProfileMenuState(video)
    val networkState = getNetworkMenuState(video)
    Row(Modifier.fillMaxWidth(), SpaceAround) { RowContent(profileState, networkState) }
}

@Composable private fun getProfileMenuState(video: Video): MenuState {
    val list = ProfileUseCases.getAll(ArgusProfileRepo) // getLabelList(getProfileCount(ArgusInfoRepo, InfoKey(Tv.name, video.infoId))).toMutableStateList()
    val labelList = list.map { it.profile.label }.toMutableStateList()
    val selected = list.find { it.profile.id == video.profileId }?.profile?.label?: ""
    return MenuState(labelList, remember { mutableStateOf(false) }, remember { mutableStateOf(selected) })
}

@Composable private fun getNetworkMenuState(video: Video): MenuState {
    val list = NetworkUseCases.getAll(ArgusNetworkRepo).toMutableStateList()
    val labelList = list.map { it.network.label }.toMutableStateList()
    val selected = list.find { it.network.id == video.networkId }?.network?.label?: ""
    return MenuState(labelList, remember { mutableStateOf(false) }, remember { mutableStateOf(selected) })
}

@Composable private fun RowContent(profileState: MenuState, networkState: MenuState) {
    val modifier = Modifier.padding(all = 4.dp).width(192.dp)
    ProfileDropMenu(profileState, modifier)
    NetworkDropMenu(networkState, modifier)
}
