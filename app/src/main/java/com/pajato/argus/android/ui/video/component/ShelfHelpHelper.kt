package com.pajato.argus.android.ui.video.component

import android.content.Context
import com.pajato.argus.android.R
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.Shelves

internal fun getShelfHelpText(shelf: Shelves, type: TextType, context: Context): String = when (type) {
    TextType.Actions -> context.getString(R.string.shelf_help_actions)
    TextType.Basics -> context.getString(R.string.shelf_help_basics)
    TextType.MoveDown -> context.getString(R.string.shelf_help_actions_move_down)
    TextType.MoveUp -> context.getString(R.string.shelf_help_actions_move_up)
    TextType.Purpose -> context.getString(getShelfPurposeResId(shelf))
    TextType.Title -> getTitleText(shelf, context)
    TextType.AddLastWatched -> context.getString(R.string.shelf_help_actions_add)
    TextType.AddWatchLater -> context.getString(R.string.shelf_help_actions_add)
}

internal fun isNotFirstShelf(coordinator: Coordinator): Boolean = coordinator.shelf.order != 0

internal fun isNotLastShelf(coordinator: Coordinator): Boolean = coordinator.shelf.order != Shelves.entries.size - 1

internal fun getShelfPurposeResId(shelf: Shelves): Int = when (shelf) {
    Shelves.LastWatched -> R.string.shelf_help_last_watched_purpose
    Shelves.WatchNext -> R.string.shelf_help_watch_next_purpose
    Shelves.WatchLater -> R.string.shelf_help_watch_later_purpose
    Shelves.Paused -> R.string.shelf_help_paused_purpose
    Shelves.Finished -> R.string.shelf_help_finished_purpose
    Shelves.Rejected -> R.string.shelf_help_rejected_purpose
    Shelves.Waiting -> R.string.shelf_help_waiting_purpose
}

internal fun getTitleText(shelf: Shelves, context: Context): String = when (shelf) {
    Shelves.LastWatched -> context.getString(R.string.shelf_help_last_watched_title)
    Shelves.WatchNext -> context.getString(R.string.shelf_help_watch_next_title)
    Shelves.WatchLater -> context.getString(R.string.shelf_help_watch_later_title)
    Shelves.Paused -> context.getString(R.string.shelf_help_paused_title)
    Shelves.Finished -> context.getString(R.string.shelf_help_finished_title)
    Shelves.Rejected -> context.getString(R.string.shelf_help_rejected_title)
    Shelves.Waiting -> context.getString(R.string.shelf_help_waiting_title)
}
