package com.pajato.argus.android.ui.filter.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FilterList
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable internal fun SavedQueryRow(text: String, modifier: Modifier) =
    Row(modifier, Arrangement.SpaceBetween, Alignment.CenterVertically) { SavedQueryRowItems(text) }

@Composable private fun SavedQueryRowItems(text: String) {
    val desc = "filter icon"
    IconButton(onClick = {}, Modifier.padding(horizontal = 8.dp), false) { Icon(Icons.Default.FilterList, desc) }
    Text(text)
}
