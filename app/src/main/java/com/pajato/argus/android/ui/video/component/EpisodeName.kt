package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.tks.common.core.EpisodeKey
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable fun EpisodeName(video: Video, modifier: Modifier) {
    val name = remember { mutableStateOf("") }
    suspend fun Video.update() { name.value = TmdbEpisodeRepo.getEpisode(EpisodeKey(infoId, series, episode)).name }
    LaunchedEffect(video) { launch(Dispatchers.IO) { video.update() } }
    Text(name.value, modifier, White, 20.sp, textAlign = TextAlign.Center, style = getTextStyle())
}

@Composable private fun getTextStyle(): TextStyle {
    val color = Color.Black
    val offset = Offset(4f, 4f)
    val blurRadius = 4f
    return LocalTextStyle.current.copy(fontWeight = FontWeight.Bold, shadow = Shadow(color, offset, blurRadius))
}
