package com.pajato.argus.android.ui.common

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.navigation.NavController
import java.io.File

typealias ChangeHandler = (String) -> Unit
typealias Handler = () -> Unit
typealias MutableString = MutableState<String>
typealias MutableBoolean = MutableState<Boolean>
typealias Nav = NavController
typealias NavRouteParam = Pair<NavRoute, Handler>
typealias Widget = @Composable () -> Unit

fun File.doesNotExist() = exists().not()
