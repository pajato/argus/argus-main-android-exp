package com.pajato.argus.android.ui.watchlater

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.genre.GenreViewModel.selectedGenreIds
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.network.NetworkViewModel.selectedNetworkIds
import com.pajato.argus.android.ui.profile.ProfileViewModel.selectedProfileIds
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.android.ui.search.I18nStrings
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.cache
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_UNREGISTERED_ERROR
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.modifyVideo
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.i18n.strings.StringsResource.get
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

object WatchLaterViewModel : ShelfViewModel, ViewModel() {
    override val id: String = Shelves.WatchLater.name
    override val coordinator: Coordinator = cache[id] ?: throw CoordinatorError(get(COORDINATOR_UNREGISTERED_ERROR))

    override val videos: List<Video> get() = videosStateList.reversed()
    private var videosStateList = listOf<Video>().toMutableStateList() // aka _videos

    private val repo = coordinator.repo

    init { I18nStrings.registerStrings() }

    override fun loadData() {
        val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds )
        val list = repo.filter(filters, ArgusInfoRepo).toMutableStateList()
        resetStateLists(list)
    }

    private fun resetStateLists(videosToAdd: SnapshotStateList<Video>) {
        videosStateList.clear()
        videosStateList.addAll(videosToAdd)
    }

    override fun runUseCase(handler: suspend () -> Unit) {
        viewModelScope.launch(IO) {
            val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds )
            handler()
            resetStateLists(repo.filter(filters, ArgusInfoRepo).toMutableStateList())
        }
    }

    internal fun moveToWatchNext(params: DetailParams) = runUseCase {
        val video = params.video
        params.coordinator.repo.apply { register(video.copy(episode = (-1 * video.episode) - 1)) }
        reloadShelfDataInOrder()
        goToVideosScreen(params.nav) {}
    }

    internal fun update(video: Video, newVideo: Video) {
        modifyVideo(repo, video, newVideo)
        reloadShelfDataInOrder()
    }
}
