package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.text.style.TextOverflow.Companion.Ellipsis
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

@Composable fun TitleText(video: Video, modifier: Modifier) {
    val name = remember { mutableStateOf("") }
    val width = CardWidth.dp - 4.dp
    LaunchedEffect(video) { withContext(IO) { name.value = getName(ArgusInfoRepo, video) } }
    TitleText(name.value, modifier.width(width))
}

@Composable private fun TitleText(title: String, modifier: Modifier) {
    Text(title, modifier, White, 14.sp, fontWeight = Bold, textAlign = Center, maxLines = 1, overflow = Ellipsis)
}
