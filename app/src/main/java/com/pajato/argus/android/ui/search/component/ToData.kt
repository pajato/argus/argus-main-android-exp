package com.pajato.argus.android.ui.search.component

import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultTv

private val t = true
private val f = false

internal fun SearchResultMovie.toData(): SearchData =
    SearchData(id, backdropPath, posterPath, overview, title, releaseDate, voteAverage, voteCount, popularity, t)
internal fun SearchResultTv.toData(): SearchData =
    SearchData(id, backdropPath, posterPath, overview, name, firstAirDate, voteAverage, voteCount, popularity, f)
