package com.pajato.argus.android.ui.video.handler

import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.network.NetworkViewModel
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.getRepo
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.MovieUseCases
import com.pajato.argus.info.uc.TvUseCases
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultTv

fun addToHistory(item: SearchResultMovie, nav: NavController, dismiss: Handler) = registerMovie(item, nav, dismiss)

fun addToHistory(item: SearchResultTv, nav: NavController, dismiss: Handler) = registerTv(item, nav, dismiss)

private fun registerMovie(item: SearchResultMovie, nav: NavController, dismiss: Handler) {
    val profile = ProfileViewModel.defaultProfile.profile
    val network = NetworkViewModel.selectedNetwork.network
    val video = Video(System.currentTimeMillis(), item.id, 1, -1, network.id, profile.id)
    HistoryViewModel.runUseCase { addMovie(HistoryViewModel.id, video, nav, dismiss) }
}

private fun registerTv(item: SearchResultTv, nav: NavController, dismiss: Handler) {
    val profile = ProfileViewModel.defaultProfile.profile
    val network = NetworkViewModel.selectedNetwork.network
    val video = Video(System.currentTimeMillis(), item.id, 1, 1, network.id, profile.id)
    HistoryViewModel.runUseCase { addTv(HistoryViewModel.id, video, nav, dismiss) }
}

internal suspend fun addMovie(id: String, video: Video, nav: NavController, dismiss: Handler) {
    MovieUseCases.registerMovie(ArgusInfoRepo, InfoKey(Tv.name, video.infoId))
    CoordinatorUseCases.addVideo(getRepo(ArgusCoordinatorRepo, id), video)
    goToVideosScreen(nav, dismiss)
}

internal suspend fun addTv(id: String, video: Video, nav: NavController, dismiss: Handler) {
    TvUseCases.registerTv(ArgusInfoRepo, InfoKey(Tv.name, video.infoId))
    CoordinatorUseCases.addVideo(getRepo(ArgusCoordinatorRepo, id), video)
    goToVideosScreen(nav, dismiss)
}
