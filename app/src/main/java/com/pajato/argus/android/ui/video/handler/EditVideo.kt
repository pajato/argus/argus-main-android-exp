package com.pajato.argus.android.ui.video.handler

import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.video.VideoDetailArgs
import com.pajato.argus.android.ui.video.VideoViewModel
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.android.ui.video.screen.EditVideoScreen

internal fun editVideo(params: DetailParams, dismiss: Handler) {
    VideoViewModel.detailArgs = VideoDetailArgs(params.coordinator, params.video)
    params.nav.navigate(EditVideoScreen.ROUTE)
    dismiss()
}
