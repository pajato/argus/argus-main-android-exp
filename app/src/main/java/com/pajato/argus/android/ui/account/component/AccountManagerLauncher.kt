package com.pajato.argus.android.ui.account.component

import androidx.compose.foundation.layout.Box
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable internal fun AccountManagerLauncher(modifier: Modifier) = Box(modifier, Alignment.Center) {
    Text("Space reserved for account manager")
}
