package com.pajato.argus.android.ui.network.component

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.network.NetworkChooserViewModel
import com.pajato.argus.network.core.SelectableNetwork

@Composable fun NetworkChooser() {
    LaunchedEffect(Unit) { NetworkChooserViewModel.loadChoosers() }
    LazyRow(Modifier.padding(start = 2.dp), rememberLazyListState()) {
        items(NetworkChooserViewModel.choosers.size) { NetworkChooserFilterChip(NetworkChooserViewModel.choosers, it) }
    }
}

@Composable private fun NetworkChooserFilterChip(list: List<SelectableNetwork>, index: Int) {
    val colors = FilterChipDefaults.filterChipColors(selectedContainerColor = Color.White, selectedLabelColor = Color.Black)
    val text = @Composable { Text(list[index].network.shortLabel) }
    fun onClick() { NetworkChooserViewModel.toggleSelected(list[index]) }
    FilterChip(isSelected(list, index), ::onClick, text, Modifier.padding(horizontal = 4.dp), colors = colors)
}

private fun isSelected(list: List<SelectableNetwork>, index: Int): Boolean = list[index].isSelected
