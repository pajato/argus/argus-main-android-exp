package com.pajato.argus.android.ui.profile.component

import androidx.compose.material.icons.Icons.AutoMirrored.Default
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import com.pajato.argus.android.R

@Composable fun BackButton(nav: NavController) {
    val backDesc = LocalContext.current.getString(R.string.cancel_description)
    IconButton({ nav.navigateUp() }) { Icon(Default.ArrowBack, backDesc) }
}
