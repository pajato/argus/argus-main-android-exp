package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Arrangement.SpaceAround
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.uc.CommonUseCases
import com.pajato.argus.info.uc.MovieUseCases.getMovieRuntime
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.tks.common.core.EpisodeKey
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable fun VideoDetailsTmdbInfo(video: Video) {
    val runtime = remember { mutableIntStateOf(0) }
    LaunchedEffect(key1 = video) { getInfo(video, runtime) }
    LabelNetworkRuntime(video, runtime)
}

@Composable fun LabelNetworkRuntime(video: Video, runtime: MutableState<Int>) {
    Row(Modifier.fillMaxWidth(), horizontalArrangement = SpaceAround) {
        if (video.isTv()) Text("S${video.series}:E${video.episode}")
        Text(CommonUseCases.getNetworkName(ArgusNetworkRepo, video.networkId))
        Text("${runtime.value}m")
    }
}

private fun CoroutineScope.getInfo(video: Video, runtime: MutableState<Int>) {
    launch(Dispatchers.IO) { if (video.isMovie()) setMovieData(video, runtime) else setEpisodeData(video, runtime) }
}

private suspend fun setEpisodeData(video: Video, runtime: MutableState<Int>) {
    val episode = TmdbEpisodeRepo.getEpisode(EpisodeKey(video.infoId, video.series, video.episode))
    runtime.value = episode.runtime
}

private fun setMovieData(video: Video, runtime: MutableState<Int>) {
    runtime.value = getMovieRuntime(ArgusInfoRepo, InfoKey(InfoType.Movie.name, video.infoId))
}
