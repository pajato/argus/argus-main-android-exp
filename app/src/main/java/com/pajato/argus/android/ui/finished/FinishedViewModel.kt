package com.pajato.argus.android.ui.finished

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import com.pajato.argus.android.ui.genre.GenreViewModel.selectedGenreIds
import com.pajato.argus.android.ui.history.HistoryViewModel.lastWatchedSeries
import com.pajato.argus.android.ui.network.NetworkViewModel.selectedNetworkIds
import com.pajato.argus.android.ui.profile.ProfileViewModel.selectedProfileIds
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.cache
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_UNREGISTERED_ERROR
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.uc.TvUseCases.isLastEpisode
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get

object FinishedViewModel : ShelfViewModel, ViewModel() {
    override val id: String = Shelves.Finished.name

    override val coordinator: Coordinator = cache[id] ?: throw CoordinatorError(getUnregisteredErrorMessage(id))

    private fun getUnregisteredErrorMessage(id: String): String = get(COORDINATOR_UNREGISTERED_ERROR, Arg("id", id))

    override val videos: List<Video> get() = videosStateList.reversed()
    private var videosStateList = listOf<Video>().toMutableStateList() // aka _videos

    override fun loadData() {
        val filters = Filter(selectedGenreIds, selectedNetworkIds, selectedProfileIds )
        val list = coordinator.repo.filter(filters, ArgusInfoRepo, getFinishedList()).reversed().toMutableStateList()
        resetStateLists(list)
    }

    override fun runUseCase(handler: suspend () -> Unit) {}

    private fun resetStateLists(videosToAdd: SnapshotStateList<Video>) {
        videosStateList.clear()
        videosStateList.addAll(videosToAdd)
    }

    private fun getFinishedList(): List<Video> = lastWatchedSeries.filter {
        isLastEpisode(ArgusInfoRepo, InfoKey(InfoType.Tv.name, it.infoId), it.series, it.episode)
    }
}
