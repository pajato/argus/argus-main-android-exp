package com.pajato.argus.android.ui.profile

import com.pajato.i18n.strings.StringsResource.put

object I18nStrings {
    const val PROFILE_ADD_FAB_DESCRIPTION = "ProfileAddFabDescription"
    const val PROFILE_BACK_BUTTON_DESCRIPTION = "ProfileBackButtonDescription"
    const val PROFILE_MODIFY_BUTTON_DESCRIPTION = "ProfileEditButtonDescription"
    const val MANAGE_PROFILES_SCREEN_TITLE = "ManageProfilesScreenTitle"

    fun registerStrings() {
        put(PROFILE_ADD_FAB_DESCRIPTION, "Add profile floating action")
        put(PROFILE_BACK_BUTTON_DESCRIPTION, "Navigate back action icon")
        put(PROFILE_MODIFY_BUTTON_DESCRIPTION, "Edit action icon")
        put(MANAGE_PROFILES_SCREEN_TITLE, "Manage Profiles")
    }
}
