package com.pajato.argus.android.ui.video.component

import com.pajato.argus.android.ui.common.MutableBoolean
import com.pajato.argus.android.ui.common.MutableString
import com.pajato.argus.android.ui.video.EditVideoViewModel

internal fun getList(count: Int): List<String> = (1..count).map { it.toString() }

internal fun selectItem(item: String, selected: MutableString, expanded: MutableBoolean, key: String) {
    selected.value = item
    expanded.value = false
    EditVideoViewModel.updateEditVideoValues(false, key, item)
}
