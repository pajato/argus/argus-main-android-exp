package com.pajato.argus.android.ui.search.component

import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.search.VideoSearchError
import com.pajato.argus.android.ui.search.screen.getShelfId
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel

internal fun getSearchDetailsScreenTitle(): String = when (getShelfId()) {
    WatchLaterViewModel.id -> "Add to Watch Later"
    HistoryViewModel.id -> "Add to History"
    else -> "MultiSearch Details"
}
