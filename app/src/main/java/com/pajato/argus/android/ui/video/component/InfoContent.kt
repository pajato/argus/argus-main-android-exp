package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.uc.TvUseCases
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.uc.ProfileUseCases

@Composable fun InfoContent(shelf: Shelf, video: Video) {
    TitleText(video, Modifier.padding(horizontal = 2.dp).fillMaxWidth().horizontalScroll(rememberScrollState()))
    // Log.d("InfoContent", "InfoContent: ${video.getEpisodeLabel()}")
    if (showEpisodeLabel(shelf)) Text(video.getEpisodeLabel(), Modifier, White, 12.sp, textAlign = Center)
    OptionalInfoItem(shelf, video)
    if (ProfileViewModel.selectedProfileIds.isEmpty()) ProfileName(video)
}

private fun showEpisodeLabel(shelf: Shelf): Boolean = when (shelf.id) {
    Shelves.WatchNext.name -> true
    Shelves.LastWatched.name -> true
    Shelves.Rejected.name -> true
    else -> false
}

@Composable private fun ProfileName(video: Video) {
    val text = ProfileUseCases.get(ArgusProfileRepo, video.profileId)?.profile?.label ?: "Nobody"
    Text(text, Modifier, White, 12.sp, textAlign = Center)
}

private fun Video.getEpisodeLabel(): String = if (isMovie()) "Movie" else getShowLabel()

private fun Video.getShowLabel(): String {
    val seriesCount = TvUseCases.getSeriesCount(ArgusInfoRepo, infoId)
    val episodeCount = TvUseCases.getEpisodeCount(ArgusInfoRepo, infoId, series)
    return "S$series:$seriesCount E$episode:$episodeCount"
}
