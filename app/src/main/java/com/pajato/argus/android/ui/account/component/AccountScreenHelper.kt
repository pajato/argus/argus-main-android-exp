package com.pajato.argus.android.ui.account.component

import androidx.navigation.NavController

internal const val ABOUT_PAGE = "About Page"
internal const val MANAGE_PROFILES = "Manage Profiles"
internal const val MANAGE_NETWORKS = "Manage Networks"
internal const val MANAGE_GENRES = "Manage Genres"
internal const val HELP_PAGE = "Help"
internal const val FEEDBACK_PAGE = "Feedback"
internal const val SETTINGS_TITLE = "Settings"
internal const val UPLOAD_TITLE = "Upload"

internal fun cancel(nav: NavController) { nav.navigateUp() }

internal enum class AccountItems { ABOUT, PROFILES, NETWORKS, GENRES, SETTINGS, HELP, FEEDBACK, UPLOAD }
