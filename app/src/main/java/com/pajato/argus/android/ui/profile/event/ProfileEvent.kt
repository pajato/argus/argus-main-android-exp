package com.pajato.argus.android.ui.profile.event

import com.pajato.argus.profile.core.SelectableProfile

sealed class ProfileEvent

data class CreateProfile(val profile: SelectableProfile) : ProfileEvent()
data class HideProfile(val profile: SelectableProfile) : ProfileEvent()
data class ModifyProfile(val profile: SelectableProfile) : ProfileEvent()
data class ToggleProfileSelection(val profile: SelectableProfile) : ProfileEvent()
