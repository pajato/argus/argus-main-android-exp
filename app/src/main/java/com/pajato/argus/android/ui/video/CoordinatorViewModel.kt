package com.pajato.argus.android.ui.video

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo

object CoordinatorViewModel : ViewModel() {
    private var orderStateList = listOf<String>().toMutableStateList() // aka _order
    val order: List<String> get() = orderStateList

    init { register(ArgusCoordinatorRepo.orderList.toMutableStateList()) }

    fun register(list: SnapshotStateList<String>) {
        orderStateList.clear()
        if (list.isNotEmpty()) orderStateList.addAll(list)
    }
}
