package com.pajato.argus.android.ui.search

// import androidx.paging.compose.items
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.paging.compose.collectAsLazyPagingItems
import kotlinx.coroutines.flow.Flow

// Data class for your text items
data class TextItem(val id: Int, val text: String)

// Network service for fetching text data
interface TextService {
    suspend fun getText(page: Int, pageSize: Int): List<TextItem>
}

// Paging source for loading text data from the network
class TextPagingSource(private val service: TextService) : PagingSource<Int, TextItem>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TextItem> {
        return try {
            val page = params.key ?: 1
            val pageSize = params.loadSize
            val data = service.getText(page, pageSize)
            LoadResult.Page(
                data = data,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (data.isEmpty()) null else page + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, TextItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}

// Function to create a Flow of PagingData
fun createTextFlow(service: TextService): Flow<PagingData<TextItem>> {
    return Pager(
        config = PagingConfig(pageSize = 20),
        pagingSourceFactory = { TextPagingSource(service) }
    ).flow
}

// Composable function to display the text items in a LazyRow
@Composable fun TextList(textFlow: Flow<PagingData<TextItem>>) {
    val lazyPagingItems = textFlow.collectAsLazyPagingItems()
    LazyRow { items(lazyPagingItems.itemCount) { item -> Text(text = lazyPagingItems[item]?.text ?: "default text") } }
}
