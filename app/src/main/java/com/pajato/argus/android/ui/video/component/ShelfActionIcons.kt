package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.pajato.argus.android.ui.common.Nav
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.orderList
import com.pajato.argus.coordinator.core.Coordinator

@Composable fun ShelfActionIcons(coordinator: Coordinator, nav: Nav, modifier: Modifier) = Row {
    if (coordinator.shelf.order != 0) MoveShelfUpIcon(coordinator, nav, modifier)
    if (coordinator.shelf.order != orderList.size - 1) MoveShelfDownIcon(coordinator, nav, modifier)
    if (isLastWatchedOrWatchLater(coordinator.shelf)) AddToShelfIcon(coordinator, nav, modifier)
}
