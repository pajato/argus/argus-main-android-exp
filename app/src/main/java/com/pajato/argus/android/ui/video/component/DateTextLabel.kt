package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.video.Video
import java.text.SimpleDateFormat
import java.time.Year
import java.util.Calendar
import java.util.Date
import java.util.Locale

@Composable fun DateTextLabel(video: Video) {
    val modifier = Modifier.padding(horizontal = 8.dp)
    Text(getFormattedDate(video), modifier, Color.White, 12.sp, textAlign = TextAlign.Center)
}

private fun getFormattedDate(video: Video): String = when {
    video.timestamp < 0L -> "Pre-historic"
    video.isCurrentYear() -> getFormattedDate(video, "EEE MMM d")
    else -> getFormattedDate(video, "EEE MMM d y")
}

private fun getFormattedDate(video: Video, pattern: String): String =
    SimpleDateFormat(pattern, Locale.getDefault()).format(Date(video.timestamp))

private fun Video.isCurrentYear(): Boolean {
    val cal: Calendar = Calendar.getInstance()
    cal.time = Date(timestamp)
    return Year.now().value == cal.get(Calendar.YEAR)
}
