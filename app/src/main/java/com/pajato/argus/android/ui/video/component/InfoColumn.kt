package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Video

@Composable fun InfoColumn(shelf: Shelf, video: Video) {
    Column(horizontalAlignment = CenterHorizontally) { InfoContent(shelf, video) }
}
