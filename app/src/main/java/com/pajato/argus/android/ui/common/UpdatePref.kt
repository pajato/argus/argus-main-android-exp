package com.pajato.argus.android.ui.common

import android.content.SharedPreferences

fun updateIntPref(prefs: SharedPreferences?, key: String, value: Int?) {
    when {
        prefs == null -> return
        value == null -> prefs.edit().remove(key).apply()
        else -> prefs.edit().putInt(key, value).apply()
    }
}

fun updateBooleanPref(prefs: SharedPreferences?, key: String, value: Boolean?) {
    when {
        prefs == null -> return
        value == null -> prefs.edit().remove(key).apply()
        else -> prefs.edit().putBoolean(key, value).apply()
    }
}
