package com.pajato.argus.android.ui.common.component

import androidx.compose.foundation.layout.BoxScope
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.video.screen.VideosScreen

@Composable fun BoxScope.CancelButton(nav: NavController) {
    val desc = LocalContext.current.getString(R.string.cancel_description)
    IconButton({ nav.navigateUp() }, Modifier.align(CenterStart)) { Icon(Icons.Default.Close, desc) }
}

@Composable fun BoxScope.DoneButton(nav: NavController) {
    val desc = LocalContext.current.getString(R.string.done_description)
    IconButton({ navigateHome(nav) }, Modifier.align(CenterEnd)) { Icon(Icons.Filled.Done, desc) }
}

private fun navigateHome(nav: NavController) = nav.popBackStack(VideosScreen.ROUTE, false)
