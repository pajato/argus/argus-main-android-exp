package com.pajato.argus.android.ui.filter

import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.runtime.Composable

typealias Handler = () -> Unit
typealias QueryCB = (String) -> Unit
typealias Composer = @Composable () -> Unit
typealias Done = KeyboardActionScope.() -> Unit
