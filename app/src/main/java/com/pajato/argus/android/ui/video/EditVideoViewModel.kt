package com.pajato.argus.android.ui.video

import androidx.compose.runtime.MutableState
import androidx.lifecycle.ViewModel
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.toTimestamp
import java.text.ParsePosition

internal typealias MutableInt = MutableState<Int>

object EditVideoViewModel : ViewModel() {
    internal val map: MutableMap<String, String> = mutableMapOf()

    fun updateEditVideoValues(clear: Boolean, key: String, value: String) = when {
        clear -> map.clear()
        key == "series" -> dealWithSeriesAndResetEpisode(value)
        else -> map[key] = value
    }

    private fun dealWithSeriesAndResetEpisode(value: String) {
        map["episode"] = ""
        map["series"] = value
    }

    fun updateVideo(shelf: Shelf, video: Video, errorPosition: MutableInt) {
        val errors = ParsePosition(errorPosition.value)
        val timestamp = map["timestamp"]?.toTimestamp(errors) ?: video.timestamp
        val hasErrors = errors.errorIndex >= 0
        if (hasErrors) dealWithErrors(errors) else update(shelf, video, map, timestamp)
    }
}

private fun dealWithErrors(errors: ParsePosition) { throw IllegalArgumentException(errors.errorIndex.toString()) }
