package com.pajato.argus.android.ui.filter

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.coordinator.core.video.Query
import com.pajato.argus.filter.adapter.ArgusQueryRepo
import com.pajato.argus.info.adapter.ArgusInfoRepo
import java.io.File
import java.net.URI
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

object FilterViewModel : ViewModel() {
    private var uri: URI? = null

    private var _showFilterBar = mutableStateOf(false)
    val showFilterBar: Boolean get() = _showFilterBar.value

    private var queryStateList = listOf<String>().toMutableStateList() // aka _queries
    val savedQueries: List<String> get() = queryStateList
    val selectedQuery: MutableState<Query> = mutableStateOf(Query("", ArgusInfoRepo))

    fun setSearchQuery(query: String) {
        selectedQuery.value = selectedQuery.value.copy(query)
        if (query.isNotEmpty()) ArgusQueryRepo.register(query) // savedQueries.add(selectedQuery.value.text)
        viewModelScope.launch(IO) { reloadShelfDataInOrder() }
    }

    fun injectDependency(uri: URI) {
        this.uri = uri
        viewModelScope.launch(IO) { ArgusQueryRepo.injectDependency(URI("${uri}queries.txt")) }
    }

    fun loadQueryData(uri: URI) {
        viewModelScope.launch(IO) {
            ArgusQueryRepo.injectDependency(uri)
            queryStateList.clear()
            queryStateList.addAll(ArgusQueryRepo.get().toMutableStateList())
        }
    }

    fun loadData() {
        if (uri == null) return
        Log.d("FilterViewModel", "loading saved queries...")
        queryStateList.clear()
        queryStateList.addAll(ArgusQueryRepo.get().toMutableStateList())
        _showFilterBar.value = getPersistedValue()
    }

    fun removeQuery(query: String) {
        ArgusQueryRepo.remove(query)
        queryStateList.remove(query)
    }

    fun setShowFilterBar(value: Boolean) {
        val uri = this.uri
        _showFilterBar.value = value
        if (uri != null) persistValue(File(getShowFilterBarUri(uri)), value)
    }

    private fun persistValue(file: File, value: Boolean) { file.writeText("$value".lowercase()) }

    private fun getPersistedValue(): Boolean {
        val uri = this.uri
        return if (uri == null) false else getValueFromFile(File(getShowFilterBarUri(uri)))
    }

    private fun getValueFromFile(file: File): Boolean = if (!file.exists()) false else file.readText().toBoolean()

    private fun getShowFilterBarUri(uri: URI): URI = URI("${uri}showFilterBar.txt")
}
