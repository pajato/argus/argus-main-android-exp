package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.video.Video

@Composable fun InfoImageWithOverlay(video: Video, path: String, name: String) {
    TitleImage(path, name)
    EpisodeName(video, Modifier.fillMaxWidth().padding(vertical = 4.dp))
}
