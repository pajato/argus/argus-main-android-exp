package com.pajato.argus.android.ui.video.component

import android.content.Context
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.android.R
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.Shelves

private typealias ComposeUnit = @Composable () -> Unit
internal typealias MutableBoolean = MutableState<Boolean>

@Composable fun ShelfHeader(wrapper: Coordinator, actions: ComposeUnit, showHelp: MutableBoolean) {
    val context = LocalContext.current
    val size = getViewModel(wrapper).videos.size
    val text = "${getShelfLabel(Shelves.valueOf(wrapper.shelf.id), context)} ($size)"
    ClickableHeader(text, actions, showHelp)
}

@Composable internal fun ClickableHeader(text: String, actions: ComposeUnit, showHelp: MutableBoolean) {
    Row(Modifier.fillMaxWidth().height(56.dp), SpaceBetween, CenterVertically) {
        TextButton({ showHelp.value = true }) { Text(text, fontSize = 16.sp) }
        actions()
    }
}

private fun getShelfLabel(shelf: Shelves, context: Context) = when (shelf) {
    Shelves.WatchNext -> context.getString(R.string.watch_next)
    Shelves.LastWatched -> context.getString(R.string.last_watched)
    Shelves.WatchLater -> context.getString(R.string.watch_later)
    Shelves.Paused -> context.getString(R.string.paused)
    Shelves.Finished -> context.getString(R.string.finished)
    Shelves.Rejected -> context.getString(R.string.rejected)
    Shelves.Waiting -> context.getString(R.string.waiting)
}
