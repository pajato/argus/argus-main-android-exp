package com.pajato.argus.android.ui.network.screen

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.component.ManageParams
import com.pajato.argus.android.ui.common.component.ManageScreen
import com.pajato.argus.android.ui.network.NetworkViewModel
import com.pajato.argus.network.adapter.ArgusNetworkRepo.cache

object ManageNetworksScreen { const val ROUTE = "ManageNetworksScreen" }

private const val TYPE = "Networks"

@Composable fun ManageNetworksScreen(nav: NavController) =
    ManageScreen(ManageParams(nav, NetworkViewModel, TYPE, R.drawable.networks, cache.values.map { it.network.label }))
