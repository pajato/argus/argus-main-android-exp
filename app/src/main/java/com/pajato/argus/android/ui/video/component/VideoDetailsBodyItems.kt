package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable fun VideoDetailsBodyItems(params: DetailParams, height: Dp) {
    Box(Modifier.fillMaxWidth().heightIn(height, height)) { ImageOrText(Modifier.align(Center), params.video) }
    Column(Modifier.padding(top = 8.dp)) { VideoDetailsTmdbInfo(params.video) }
    VideoDetailsExtra(params)
}
