package com.pajato.argus.android.ui.common

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.ui.graphics.vector.ImageVector
import com.pajato.argus.android.ui.video.screen.VideosScreen

enum class NavRoute(val icon: ImageVector, val route: String, val desc: String) {
    BackToVideos(Icons.AutoMirrored.Filled.ArrowBack, VideosScreen.ROUTE, "Back Icon"),
    UpToSeries(Icons.Default.KeyboardArrowUp, VideosScreen.ROUTE, "Up Icon"),
    Done(Icons.Default.Done, VideosScreen.ROUTE, "Done Icon"),
    Cancel(Icons.Default.Close, "", "Navigate up"),
    Save(Icons.Default.Done, "", "Save action icon")
}
