package com.pajato.argus.android.ui.profile.screen

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.NavRoute.Cancel
import com.pajato.argus.android.ui.common.NavRoute.Save
import com.pajato.argus.android.ui.common.component.NavHeader
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.android.ui.profile.component.EditProfileForm
import com.pajato.argus.android.ui.profile.event.CreateProfile

object EditProfileScreen { const val ROUTE = "EditProfileScreen" }

@Composable fun EditProfileScreen(nav: NavController) {
    val (start, end) = Pair(Pair(Cancel) { cancel(nav) }, Pair(Save) { saveProfile(nav) })
    LazyColumn(Modifier.fillMaxSize()) {
        item { NavHeader("Add Profile", start, end) }
        item { EditProfileForm() }
    }
}

private fun cancel(nav: NavController) { nav.navigateUp() }

private fun saveProfile(nav: NavController) {
    val item = ProfileViewModel.itemUnderEdit
    ProfileViewModel.onEvent(CreateProfile(item))
    nav.navigateUp()
}
