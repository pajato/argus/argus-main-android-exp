package com.pajato.argus.android.ui.common.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Visibility
import androidx.compose.material.icons.outlined.VisibilityOff
import androidx.compose.material3.FilterChip
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Alignment.Companion.Start
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.Widget

@Composable fun ManageList(params: ManageParams) {
    val state = rememberLazyListState()
    val list = params.list
    val modifier = Modifier.fillMaxWidth().padding(end = 16.dp)
    LazyColumn(modifier, state, horizontalAlignment = Start) { items(list.size) { ManageItems(params, it) } }
}

@Composable private fun ManageItems(params: ManageParams, index: Int) {
    val text = @Composable { Text(params.list[index]) }
    Row(Modifier.fillMaxWidth().padding(start = 16.dp), SpaceBetween) { ManageRowItems(params, index, text) }
}

@Composable private fun RowScope.ManageRowItems(params: ManageParams, index: Int, text: Widget) {
    val modifier = Modifier.padding(horizontal = 4.dp)
    val enabled = getHidden(params, index).not()
    val colors = getFilterChipColors()
    FilterChip(getSelected(params, index), { toggleSelected(params, index) }, text, modifier, enabled, colors = colors)
    HideRestoreActionButton(params, index)
}

@Composable private fun RowScope.HideRestoreActionButton(params: ManageParams, index: Int) {
    val hidden = params.viewModel.getHidden(params.list[index])
    val icon = if (hidden) Icons.Outlined.Visibility else Icons.Outlined.VisibilityOff
    val text = if (hidden) "Restore" else "Hide"
    Row(Modifier.align(CenterVertically).clickable { toggleHidden(params, index) }) { RowItems(icon, text) }
}

@Composable private fun RowItems(icon: ImageVector, text: String) {
    Icon(icon, stringResource(R.string.hide_restore_icon_desc), Modifier.padding(end = 8.dp))
    Text(text)
}
