package com.pajato.argus.android.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.pajato.argus.android.ui.theme.ArgusTheme

@Composable fun ArgusUI() = ArgusTheme(dark = true) {
    val nav = rememberNavController()
    Surface(Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) { ArgusNavigation(nav) }
}
