package com.pajato.argus.android.ui.network.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.ChangeHandler
import com.pajato.argus.android.ui.common.MutableBoolean
import com.pajato.argus.android.ui.common.MutableString
import com.pajato.argus.android.ui.network.NetworkViewModel.hasError
import com.pajato.argus.network.core.Network
import com.pajato.argus.network.core.SelectableNetwork

private typealias Callback = () -> SelectableNetwork

@Composable fun NetworkForm(nav: NavController, state: MutableString, error: MutableBoolean, networkId: Int) {
    val network = Network(networkId, "", state.value, "")
    val (isSelected, isHidden) = Pair(false, false)
    val callback: Callback = { SelectableNetwork(isSelected, isHidden, network) }
    val changeHandler: ChangeHandler = { state.value = it; error.value = hasError(it) }
    NetworkForm(nav, state.value, error.value, callback, changeHandler)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NetworkForm(nav: NavController, label: String, error: Boolean, callback: Callback, onValueChanged: ChangeHandler) =
    Column {
        if (error) Text("This label is already used", Modifier.padding(8.dp), Color.Red)
        LabelTextField(label, onValueChanged)
        LabelActions(Modifier.padding(end = 8.dp), nav, network = callback(), error)
    }
