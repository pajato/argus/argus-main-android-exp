package com.pajato.argus.android.ui.video.screen

import android.annotation.SuppressLint
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavController
import com.pajato.argus.android.ui.video.VideoViewModel
import com.pajato.argus.android.ui.video.component.VideoRatingsHeader
import com.pajato.argus.android.ui.video.component.VideoRatingsShelf
import com.pajato.argus.android.ui.video.component.getName
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import java.net.URLEncoder.encode

object VideoRatingsScreen { const val ROUTE = "VideoRatingsScreen" }

@Composable fun VideoRatingsScreen(nav: NavController) {
    val showName = remember { mutableStateOf("") }
    val args = VideoViewModel.detailArgs
    LaunchedEffect(args.video) { showName.value = getName(ArgusInfoRepo, args.video) }
    Column(Modifier.fillMaxSize()) { RatingItems(nav, args.video, showName, Modifier.padding(top = 24.dp)) }
}

@Composable fun RatingItems(nav: NavController, video: Video, state: MutableState<String>, modifier: Modifier) {
    val context = LocalContext.current
    VideoRatingsHeader(nav)
    VideoRatingsShelf(video)
    AndroidView(factory = { WebView(context).apply { run(WebViewClient(), state) } }, modifier)
}

@SuppressLint("SetJavaScriptEnabled")
private fun WebView.run(client: WebViewClient, state: MutableState<String>) {
    val url = "https://google.com/search?q=${encode(state.value, "utf-8")}"
    webViewClient = client
    settings.javaScriptEnabled = true
    loadUrl(url)
}
