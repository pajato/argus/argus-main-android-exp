package com.pajato.argus.android.ui.search

import com.pajato.tks.search.movie.adapter.TmdbSearchMovieRepo
import com.pajato.tks.search.multi.adapter.TmdbSearchMultiRepo
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultMulti

object MovieSearchViewModel : AbstractSearchViewModel<SearchResultMovie>(TmdbSearchMovieRepo::getSearchPageMovie)
