package com.pajato.argus.android.ui.account.component

import android.content.Context
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.Help
import androidx.compose.material.icons.outlined.Feedback
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.Person
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material.icons.outlined.Stream
import androidx.compose.material.icons.outlined.Upload
import androidx.compose.material.icons.outlined.VideoLabel
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.account.component.AccountItems.ABOUT
import com.pajato.argus.android.ui.account.component.AccountItems.FEEDBACK
import com.pajato.argus.android.ui.account.component.AccountItems.GENRES
import com.pajato.argus.android.ui.account.component.AccountItems.HELP
import com.pajato.argus.android.ui.account.component.AccountItems.NETWORKS
import com.pajato.argus.android.ui.account.component.AccountItems.PROFILES
import com.pajato.argus.android.ui.account.component.AccountItems.SETTINGS
import com.pajato.argus.android.ui.account.component.AccountItems.UPLOAD
import com.pajato.argus.android.ui.account.screen.AboutScreen
import com.pajato.argus.android.ui.feedback.screen.FeedbackScreen
import com.pajato.argus.android.ui.genre.screen.ManageGenresScreen
import com.pajato.argus.android.ui.network.screen.ManageNetworksScreen
import com.pajato.argus.android.ui.profile.screen.ManageProfilesScreen
import com.pajato.argus.android.ui.transfer.upload.upload

@Composable internal fun AccountItem(imageVector: ImageVector, label: String, onClick: () -> Unit = {}) {
    val rowModifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp, vertical = 12.dp)
    val textModifier = Modifier.padding(start = 16.dp)
    Row(rowModifier) { AccountRowItems(imageVector, label, textModifier, onClick) }
}

@Composable internal fun AccountItem(nav: NavController, context: Context, item: AccountItems) = when (item) {
    ABOUT -> AccountItem(Icons.Outlined.Info, ABOUT_PAGE) { nav.navigate(AboutScreen.ROUTE) }
    PROFILES -> AccountItem(Icons.Outlined.Person, MANAGE_PROFILES) { nav.navigate(ManageProfilesScreen.ROUTE) }
    NETWORKS -> AccountItem(Icons.Outlined.Stream, MANAGE_NETWORKS) { nav.navigate(ManageNetworksScreen.ROUTE) }
    GENRES -> AccountItem(Icons.Outlined.VideoLabel, MANAGE_GENRES) { nav.navigate(ManageGenresScreen.ROUTE) }
    SETTINGS -> AccountItem(Icons.Outlined.Settings, SETTINGS_TITLE) {}
    HELP -> AccountItem(Icons.AutoMirrored.Outlined.Help, HELP_PAGE) {}
    FEEDBACK -> AccountItem(Icons.Outlined.Feedback, FEEDBACK_PAGE) { nav.navigate(FeedbackScreen.ROUTE) }
    UPLOAD -> AccountItem(Icons.Outlined.Upload, UPLOAD_TITLE) { upload(context) }
}
