package com.pajato.argus.android.ui.search

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.navigation.NavController
import com.pajato.argus.android.ui.search.component.CloseHandler

private const val DEFAULT_QUERY_HINT = "Search movies, tv shows or people"
private const val LEADING_ICON_DESC = "Search icon"

data class SearchBarParams(
    val nav: NavController,
    val text: MutableState<String>,
    val active: MutableState<Boolean>,
    val onQueryChange: (String) -> Unit = { text.value = it },
    val onSearch: (String) -> Unit = { active.value = true },
    val onActiveChange: (Boolean) -> Unit = { },
    val placeholder: @Composable () -> Unit = { Text(DEFAULT_QUERY_HINT) },
    val leadingIcon: @Composable () -> Unit = { Icon(Icons.Default.Search, LEADING_ICON_DESC) },
    val trailingIcon: @Composable () -> Unit = { CloseHandler(nav, text, active) }
)
