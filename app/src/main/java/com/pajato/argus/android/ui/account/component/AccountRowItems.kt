package com.pajato.argus.android.ui.account.component

import androidx.compose.foundation.clickable
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

@Composable internal fun AccountRowItems(image: ImageVector, label: String, modifier: Modifier, onClick: () -> Unit) {
    Icon(image, label)
    Text(label, modifier.clickable { onClick.invoke() }, Color.White, 14.sp, FontStyle.Normal, FontWeight.W400)
}
