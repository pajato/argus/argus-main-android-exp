package com.pajato.argus.android.ui.video.component

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.component.notSelectedPair
import com.pajato.argus.android.ui.video.CoordinatorViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.cache
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_UNREGISTERED_ERROR
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get

@Composable fun VideoShelvesScroller(nav: NavController, state: LazyListState) {
    val orderList: List<String> = remember { CoordinatorViewModel.order }
    val modifier = Modifier
        .padding(horizontal = 8.dp)
        .fillMaxSize()
    LazyColumn(modifier, state) { items(orderList.size) { shelfIndex -> VideoShelf(shelfIndex, orderList, nav)} }
}

@Composable private fun VideoShelf(index: Int, orderList: List<String>, nav: NavController) {
    val coordinator = getCoordinator(orderList[index])
    Column { VideoShelfItems(coordinator, nav) }
}

private fun getCoordinator(id: String): Coordinator = cache[id]
    ?: throw CoordinatorError(get(COORDINATOR_UNREGISTERED_ERROR, Arg("id", id)))

@Composable private fun VideoShelfItems(coordinator: Coordinator, nav: NavController) {
    val showHelpDialog = remember { mutableStateOf(false) }
    Header(coordinator, nav, showHelpDialog)
    ScrollableVideoItems(coordinator, getViewModel(coordinator).videos, nav)
    ShelfHelpDialog(coordinator, showHelpDialog.value, { showHelpDialog.value = false })
}

@Composable private fun Header(coordinator: Coordinator, nav: NavController, showHelpDialog: MutableState<Boolean>) {
    val modifier = Modifier.padding(horizontal = 16.dp).height(20.dp).width(20.dp).getActionModifier()
    ShelfHeader(coordinator, { ShelfActionIcons(coordinator, nav, modifier) }, showHelpDialog)
}

private fun Modifier.getActionModifier(): Modifier = composed {
    val color: Color by animateColorAsState(notSelectedPair.first, notSelectedPair.second, label = "Action")
    this.background(color).clip(CircleShape)
}
