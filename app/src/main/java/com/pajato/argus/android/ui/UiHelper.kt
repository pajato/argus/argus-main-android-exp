package com.pajato.argus.android.ui

import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.filter.FilterViewModel
import com.pajato.argus.android.ui.finished.FinishedViewModel
import com.pajato.argus.android.ui.rejected.RejectedViewModel
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.paused.PausedViewModel
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.android.ui.video.screen.VideosScreen
import com.pajato.argus.android.ui.waiting.WaitingViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.android.ui.watchnext.WatchNextViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

fun reloadShelfDataInOrder() {
    val shelfViewModels: List<ShelfViewModel> = listOf(RejectedViewModel, WaitingViewModel, WatchLaterViewModel,
        HistoryViewModel, PausedViewModel, FinishedViewModel, WatchNextViewModel)
    shelfViewModels.forEach { it.loadData() }
    FilterViewModel.loadData()
}

internal fun goToVideosScreen(nav: NavController, dismiss: Handler = {}) {
    val context: CoroutineContext = Dispatchers.IO + SupervisorJob()
    val uiScope = CoroutineScope(context)
    uiScope.launch { showReloadedVideos(nav) }
    dismiss()
}

private suspend fun showReloadedVideos(nav: NavController) {
    reloadShelfDataInOrder()
    withContext(Dispatchers.Main) {
        nav.navigate(VideosScreen.ROUTE) { popUpTo(VideosScreen.ROUTE) { inclusive = true } }
    }
}
