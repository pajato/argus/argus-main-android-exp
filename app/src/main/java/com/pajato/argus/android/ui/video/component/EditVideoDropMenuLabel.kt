package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBoxScope
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class) internal typealias Scope = ExposedDropdownMenuBoxScope
internal typealias StringHandler = (String) -> Unit

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Scope.EditVideoDropMenuLabel(value: String, label: String, expanded: Boolean) {
    val text = @Composable { Text(label) }
    val icon = @Composable { ExposedDropdownMenuDefaults.TrailingIcon(expanded) }
    val colors = ExposedDropdownMenuDefaults.textFieldColors()
    val modifier = Modifier.menuAnchor(MenuAnchorType.PrimaryNotEditable)
    TextField(value, {}, modifier, readOnly = true, label = text, trailingIcon = icon, colors = colors)
}
