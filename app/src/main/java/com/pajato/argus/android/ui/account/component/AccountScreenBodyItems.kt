package com.pajato.argus.android.ui.account.component

import android.content.Context
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.BottomCenter
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController

@Composable internal fun AccountScreenBodyItems(modifier: Modifier, nav: NavController, context: Context) {
    AccountManagerLauncher(Modifier.fillMaxWidth().height(150.dp).padding(16.dp).border(1.dp, Color.White))
    Text("ARGUS BASIC", modifier, Color.White, 14.sp, FontStyle.Normal)
    AccountItems.entries.forEach { item -> AccountItem(nav, context, item) }
    Box(Modifier.fillMaxSize().padding(16.dp), BottomCenter) { Text("Privacy Policy . Terms of Service") }
}
