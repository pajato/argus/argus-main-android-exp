package com.pajato.argus.android.ui.video.component

import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.shelf.Shelf

internal fun isLastWatchedOrWatchLater(shelf: Shelf): Boolean =
    shelf.id == Shelves.LastWatched.name || shelf.id == Shelves.WatchLater.name
