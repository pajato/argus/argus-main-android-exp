package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.video.screen.VideoRatingsScreen.ROUTE

@Composable fun VideoDetailsExtra(params: DetailParams) = Column(Modifier.fillMaxSize(), SpaceBetween) {
    VideoOverview(params.video, Modifier.padding(all = 8.dp))
    Text("Ratings", Modifier.fillMaxWidth().clickable { params.nav.navigate(ROUTE) }, textAlign = Center)
    EpisodeDetailsActions(params)
}
