package com.pajato.argus.android.ui.search.component

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.navigation.NavController

@Composable fun VideoSearchScreen(nav: NavController) {
    var name by remember { mutableStateOf("") }
    val onValueChanged: (String) -> Unit = { name = it }
    VideoSearchBody(nav, name, onValueChanged)
}
