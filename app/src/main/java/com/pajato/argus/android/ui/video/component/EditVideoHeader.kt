package com.pajato.argus.android.ui.video.component

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableIntStateOf
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.component.NavHeader
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.video.EditVideoViewModel

@Composable fun EditVideoHeader(params: DetailParams) {
    val startDest = Pair(NavRoute.BackToVideos) { params.nav.navigate(NavRoute.BackToVideos.route) }
    val endDest = Pair(NavRoute.Done) { updateVideo(params) }
    NavHeader("Edit Video", startDest, endDest)
}

private fun updateVideo(params: DetailParams) {
    if (hasTimestampErrors) return
    EditVideoViewModel.updateVideo(params.coordinator.shelf, params.video, mutableIntStateOf(0))
    goToVideosScreen(params.nav)
}
