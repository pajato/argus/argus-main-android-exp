package com.pajato.argus.android.ui.network

import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import com.pajato.argus.network.core.Network
import com.pajato.argus.network.core.SelectableNetwork

object NetworkChooserViewModel : ViewModel() {
    private const val UNKNOWN = "Unknown"

    private var chooserStateList = listOf<SelectableNetwork>().toMutableStateList() // aka _choosers

    val choosers: List<SelectableNetwork> get() = chooserStateList

    val selectedChooser: SelectableNetwork get() = choosers.firstOrNull { it.isSelected } ?: getDefaultChooser()

    private fun getDefaultChooser(): SelectableNetwork {
        val (isSelected, isHidden) = Pair(false, false)
        val network = Network(id = 0, packageId = "", label = UNKNOWN, shortLabel = UNKNOWN)
        return SelectableNetwork(isSelected, isHidden, network)
    }

    fun loadChoosers() {
        val (isSelected, isHidden) = Pair(false, false)
        val networks = NetworkViewModel.networks.filter { !it.isHidden }
        chooserStateList.clear()
        chooserStateList.addAll(networks.map { SelectableNetwork(isSelected, isHidden, it.network) })
    }

    fun toggleSelected(item: SelectableNetwork) {
        loadChoosers()
        replaceItem(item)
    }

    private fun replaceItem(item: SelectableNetwork) {
        val index = chooserStateList.indexOf(item)
        val newItem = item.copy(isSelected = item.isSelected.not())
        chooserStateList.removeAt(index)
        chooserStateList.add(index, newItem)
    }
}
