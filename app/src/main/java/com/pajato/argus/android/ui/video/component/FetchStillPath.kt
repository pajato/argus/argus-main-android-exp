package com.pajato.argus.android.ui.video.component

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.tks.common.core.EpisodeKey
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

@Composable fun FetchStillPath(video: Video, path: MutableState<String>) {
    LaunchedEffect(video) {
        val key = EpisodeKey(video.infoId, video.series, video.episode)
        launch(IO) { path.value = TmdbEpisodeRepo.getEpisode(key).stillPath }
    }
}
