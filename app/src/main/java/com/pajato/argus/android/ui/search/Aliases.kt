package com.pajato.argus.android.ui.search

import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.runtime.Composable
import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.search.pager.core.SearchPage

typealias QueryCB = (String) -> Unit
typealias Composer = @Composable () -> Unit
typealias KeyboardDone = KeyboardActionScope.() -> Unit
typealias Searcher<T> = suspend (SearchKey) -> SearchPage<T>
