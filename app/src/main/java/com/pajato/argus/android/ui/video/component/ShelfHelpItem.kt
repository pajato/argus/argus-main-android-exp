package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.Shelves

@Composable internal fun ShelfHelpItem(shelf: Shelves, type: TextType) {
    val text = getShelfHelpText(shelf, type, context = LocalContext.current)
    StyledText(type, text)
    Spacer(modifier = Modifier.height(8.dp))
}

@Composable private fun StyledText(type: TextType, text: String) {
    when (type) {
        TextType.Title -> Text(text, style = MaterialTheme.typography.titleMedium)
        else -> Text(text, fontSize = 14.sp)
    }
}
