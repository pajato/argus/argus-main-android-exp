package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.video.EditVideoViewModel
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.formatTimestamp

internal var hasTimestampErrors: Boolean = false

@Composable fun TimestampRow(video: Video) = Row(Modifier.fillMaxWidth().padding(vertical = 4.dp), Center) {
    val text = remember { mutableStateOf(formatTimestamp(video)) }
    hasTimestampErrors = false
    TextField(text.value, { updateTimestamp(it, text) }, Modifier.width(248.dp), label = { Text("Timestamp") })
}

private fun updateTimestamp(newText: String, text: MutableState<String>) {
    EditVideoViewModel.updateEditVideoValues(false, "timestamp", newText)
    text.value = newText
}
