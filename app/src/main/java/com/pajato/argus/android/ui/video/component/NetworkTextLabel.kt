package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.argus.network.uc.NetworkUseCases

@Composable fun NetworkTextLabel(video: Video) {
    val modifier = Modifier.padding(horizontal = 8.dp)
    val label = NetworkUseCases.get(ArgusNetworkRepo, video.networkId)?.network?.shortLabel ?: "No Network"
    Text(label, modifier, Color.White, 12.sp, textAlign = Center)
}
