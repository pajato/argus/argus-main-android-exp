package com.pajato.argus.android.ui.profile.screen

import androidx.compose.foundation.layout.Arrangement.End
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.component.ManageParams
import com.pajato.argus.android.ui.common.component.ManageScreen
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.android.ui.profile.component.NewProfileFAB
import com.pajato.argus.profile.adapter.ArgusProfileRepo.cache

object ManageProfilesScreen { const val ROUTE = "ManageProfilesScreen" }

private const val TYPE = "Profiles"

@Composable fun ManageProfilesScreen(nav: NavController) {
    val list = cache.values.map { it.profile.label }
    val modifier = Modifier.fillMaxWidth()
    val params = ManageParams(nav, ProfileViewModel, TYPE, R.drawable.profile, list) { Row(modifier, End) { NewProfileFAB(nav) } }
    ManageScreen(params)
}
