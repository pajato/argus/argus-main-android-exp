package com.pajato.argus.android.main

import android.app.Application
import com.pajato.argus.android.ui.account.AccountViewModel
import com.pajato.argus.android.ui.common.doesNotExist
import com.pajato.argus.android.ui.filter.FilterViewModel
import com.pajato.argus.android.ui.filter.FilterViewModel.loadQueryData
import com.pajato.argus.android.ui.genre.GenreViewModel.reloadData
import com.pajato.argus.android.ui.info.InfoViewModel
import com.pajato.argus.android.ui.network.NetworkViewModel.loadNetworkData
import com.pajato.argus.android.ui.profile.ProfileViewModel.loadProfileData
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.uc.CommonUseCases.loadInfoData
import java.io.File
import java.net.URI
import kotlin.io.path.toPath
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

private const val TWENTY_THREE_HOURS = 1000L * 60 * 60 * 23
private const val ONE_SECOND = 1000L

class AndroidMain : Application() {

    override fun onCreate() {
        super.onCreate()
        initArgus(applicationContext.filesDir.toURI())
    }

    private fun initArgus(uri: URI) {
        initInfoService(URI("${uri}settings.txt"))
        initArgusData(uri)
        initViewModels(uri)
        InfoViewModel.performPeriodicRefresh(ArgusInfoRepo, Int.MAX_VALUE, TWENTY_THREE_HOURS, ONE_SECOND)
    }

    private fun initArgusData(uri: URI) {
        loadInfoData(ArgusInfoRepo, URI("${uri}info.txt"))
        loadProfileData(URI("${uri}profiles.txt"))
        loadNetworkData(URI("${uri}networks.txt"))
        reloadData(URI("${uri}genres.txt"))
        loadQueryData(URI("${uri}queries.txt"))
        loadCoordinatorData(uri)
    }

    private fun initViewModels(uri: URI) {
        FilterViewModel.injectDependency(uri)
        AccountViewModel.initAccountViewModel(applicationContext)
    }

    private fun loadCoordinatorData(uri: URI) {
        val file = URI("$uri/shelf.txt").toPath().toFile()
        if (file.doesNotExist()) createMasterShelf(file)
        runBlocking { launch(IO) { loadDataFromInjectedUri(uri) } }
    }

    private suspend fun loadDataFromInjectedUri(uri: URI) {
        ArgusCoordinatorRepo.injectDependencies(uri, ArgusInfoRepo)
        reloadShelfDataInOrder()
    }

    private fun createMasterShelf(file: File) {
        val name = "master/shelf.txt"
        val url = this::class.java.classLoader?.getResource(name)
        checkNotNull(url) { "Fatal: master shelf resource not found, no shelf data is available!" }
        file.writeText(url.readText())
    }
}
