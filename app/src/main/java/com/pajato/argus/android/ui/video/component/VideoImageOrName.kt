package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.video.Video

@Composable fun VideoImageOrName(video: Video, modifier: Modifier) {
    Box(modifier) { ImageOrText(Modifier.align(Alignment.Center), video) }
    ShowName(video, Modifier.fillMaxWidth().padding(vertical = 4.dp))
}
