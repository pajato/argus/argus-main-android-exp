package com.pajato.argus.android.ui.network.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@Composable fun NetworkList(nav: NavController) =
    Row(Modifier.padding(8.dp, 12.dp, 0.dp, 4.dp)) { NetworkFilters() }
