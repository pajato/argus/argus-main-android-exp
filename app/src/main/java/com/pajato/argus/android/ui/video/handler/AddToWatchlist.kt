package com.pajato.argus.android.ui.video.handler

import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.common.Nav
import com.pajato.argus.android.ui.network.NetworkChooserViewModel
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel.runUseCase
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.tks.common.core.TmdbId
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultMulti
import com.pajato.tks.search.pager.core.SearchResultTv

fun addToWatchLater(item: SearchResultMovie, nav: Nav, dismiss: Handler) = registerMovie(item.id, nav, dismiss)

fun addToWatchLater(item: SearchResultMulti, nav: Nav, dismiss: Handler) = registerMovie(item.id, nav, dismiss)

fun addToWatchLater(item: SearchResultTv, nav: Nav, dismiss: Handler) = registerTv(item.id, nav, dismiss)

private fun registerMovie(id: TmdbId, nav: Nav, dismiss: Handler) =
    runUseCase { addMovie(WatchLaterViewModel.id, getVideo(id, -1), nav, dismiss) }

private fun registerTv(id: TmdbId, nav: Nav, dismiss: Handler) =
    runUseCase { addTv(WatchLaterViewModel.id, getVideo(id, 1), nav, dismiss) }

private fun getVideo(id: TmdbId, series: Int): Video {
    val profile = ProfileViewModel.defaultProfile.profile
    val network = NetworkChooserViewModel.selectedChooser.network
    return Video(System.currentTimeMillis(), id, 1, series, network.id, profile.id)
}
