package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.Icons.AutoMirrored.Filled
import androidx.compose.material.icons.Icons.Outlined
import androidx.compose.material.icons.automirrored.filled.StarHalf
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Gray
import androidx.compose.ui.unit.dp

private const val FILLED_STAR = "Filled Star"
private const val HALF_STAR = "Half Star"
private const val EMPTY_STAR = "Empty Star"
private const val STARS = 5

@Composable fun RatingStars(percentage: Int) {
    val filledStars = (percentage / 20).coerceAtMost(5)
    val hasHalfStar = percentage % 20 >= 10
    Row(horizontalArrangement = Arrangement.spacedBy(4.dp)) { FiveStars(filledStars, hasHalfStar) }
}

@Composable private fun FiveStars(filledStars: Int, hasHalfStar: Boolean) {
    val modifier = Modifier.padding(start = 2.dp).size(18.dp)
    repeat(filledStars) { Icon(Icons.Filled.Star, FILLED_STAR, modifier, Color.Yellow) }
    if (hasHalfStar) Icon(Filled.StarHalf, HALF_STAR, modifier, Color.Yellow)
    repeat(getHalfStarCount(filledStars, hasHalfStar)) { Icon(Outlined.Star, EMPTY_STAR, modifier, Gray) }
}

private fun getHalfStarCount(filledStars: Int, hasHalfStar: Boolean) =
    STARS - filledStars - (if (hasHalfStar) 1 else 0)
