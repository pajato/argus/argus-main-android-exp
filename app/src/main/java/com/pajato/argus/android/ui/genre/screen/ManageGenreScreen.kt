package com.pajato.argus.android.ui.genre.screen

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.pajato.argus.android.R
import com.pajato.argus.android.ui.common.component.ManageParams
import com.pajato.argus.android.ui.common.component.ManageScreen
import com.pajato.argus.android.ui.genre.GenreViewModel
import com.pajato.argus.genre.adapter.ArgusGenreRepo.cache

object ManageGenresScreen { const val ROUTE = "ManageGenresScreen" }

private const val TYPE = "Genres"

@Composable fun ManageGenresScreen(nav: NavController) =
    ManageScreen(ManageParams(nav, GenreViewModel, TYPE, R.drawable.genres, cache.values.map { it.genre.name }))
