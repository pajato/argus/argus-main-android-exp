package com.pajato.argus.android.ui.video.component

import android.content.Context
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Output
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.R.string.edit
import com.pajato.argus.android.R.string.edit_desc
import com.pajato.argus.android.R.string.mark_watched
import com.pajato.argus.android.R.string.mark_watched_desc
import com.pajato.argus.android.R.string.move_to_watch_next
import com.pajato.argus.android.R.string.move_to_watch_next_desc
import com.pajato.argus.android.R.string.watch_now
import com.pajato.argus.android.R.string.watch_now_desc
import com.pajato.argus.android.ui.video.handler.editVideo
import com.pajato.argus.android.ui.video.handler.markWatched
import com.pajato.argus.android.ui.video.handler.watchNow
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel.moveToWatchNext
import com.pajato.argus.coordinator.core.Coordinator

@Composable fun EpisodeDetailsActions(params: DetailParams) {
    val context = LocalContext.current
    val modifier = Modifier.fillMaxWidth().padding(8.dp)
    val weight = if (params.coordinator.shelf.id == WatchLaterViewModel.id) 0.25f else 0.33f
    Row(modifier, Arrangement.SpaceEvenly, CenterVertically) { Items(params, Modifier.weight(weight), context) }
}

@Composable private fun Items(params: DetailParams, modifier: Modifier, context: Context) {
    EpisodeDetailsAction(getWatchNowParams(params, context), modifier)
    EpisodeDetailsAction(getMarkWatchedParams(params), modifier)
    if (isWatchLater(params.coordinator)) EpisodeDetailsAction(getMoveToWatchNextParams(params), modifier)
    EpisodeDetailsAction(getEditParams(params), modifier)
}

private fun getWatchNowParams(params: DetailParams, context: Context): ActionParams =
    ActionParams(params, Default.PlayArrow, watch_now_desc, watch_now) { watchNow(context, params) {} }

private fun getMarkWatchedParams(params: DetailParams): ActionParams =
    ActionParams(params, Default.CheckCircle, mark_watched_desc, mark_watched) { markWatched(params) {} }

private fun isWatchLater(coordinator: Coordinator): Boolean = coordinator.shelf.id == WatchLaterViewModel.id

private fun getMoveToWatchNextParams(params: DetailParams): ActionParams =
    ActionParams(params, Default.Output, move_to_watch_next_desc, move_to_watch_next) { moveToWatchNext(params) }

private fun getEditParams(params: DetailParams): ActionParams =
    ActionParams(params, Default.Edit, edit_desc, edit) { editVideo(params) {} }
