package com.pajato.argus.android.ui.genre.event

import com.pajato.argus.genre.core.SelectableGenre

sealed class GenreEvent

data class ToggleSelectedGenre(val item: SelectableGenre) : GenreEvent()
data class ToggleHiddenGenre(val item: SelectableGenre) : GenreEvent()
