package com.pajato.argus.android.ui.video.handler

import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.rejected.RejectedViewModel
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.paused.PausedViewModel
import com.pajato.argus.android.ui.video.ShelfViewModel
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.android.ui.waiting.WaitingViewModel
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.android.ui.watchnext.WatchNextViewModel.getPreviousVideo
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.addVideo

internal fun reject(params: DetailParams, dismiss: Handler) {
    performAction(params, RejectedViewModel)
    dismiss()
}

internal fun waitlist(params: DetailParams, dismiss: Handler) {
    performAction(params, WaitingViewModel)
    dismiss()
}

private fun performAction(params: DetailParams, viewModel: ShelfViewModel) = when (params.coordinator.shelf.id) {
    Shelves.WatchNext.name -> viewModel.runUseCase { addPreviousVideo(viewModel, params) }
    Shelves.Paused.name -> viewModel.runUseCase { addVideo(PausedViewModel.coordinator.repo, params.video) }
    Shelves.LastWatched.name -> viewModel.runUseCase { addVideo(HistoryViewModel.coordinator.repo, params.video) }
    Shelves.WatchLater.name -> viewModel.runUseCase { addVideo(WatchLaterViewModel.coordinator.repo, params.video) }
    else -> {}
}

fun addPreviousVideo(viewModel: ShelfViewModel, params: DetailParams) {
    addVideo(viewModel.coordinator.repo, getPreviousVideo(params.video))
}
