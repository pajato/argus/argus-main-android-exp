package com.pajato.argus.android.ui.info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.uc.CommonUseCases.performRefreshUpdates
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object InfoViewModel : ViewModel() {
    fun performPeriodicRefresh(repo: InfoRepo, refreshCount: Int, refreshPeriod: Long, sendPeriod: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            performRefreshUpdates(refreshCount, repo, refreshPeriod, sendPeriod)
            reloadShelfDataInOrder()
        }
    }
}
