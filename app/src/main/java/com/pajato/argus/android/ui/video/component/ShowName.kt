package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable fun ShowName(video: Video, modifier: Modifier) {
    val showName = remember { mutableStateOf("") }
    LaunchedEffect(video) { launch(Dispatchers.IO) { showName.value = getName(ArgusInfoRepo, video) } }
    Text(showName.value, modifier, Color.White, 22.sp, textAlign = TextAlign.Center, style = getTextStyle())
}

@Composable private fun getTextStyle(): TextStyle {
    val shadow = Shadow(Color.Black, Offset(4f, 4f), 4f)
    return LocalTextStyle.current.copy(fontWeight = FontWeight.Bold, shadow = shadow)
}
