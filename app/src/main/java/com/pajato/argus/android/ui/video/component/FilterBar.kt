package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.genre.component.GenreFilters
import com.pajato.argus.android.ui.network.component.NetworkFilters
import com.pajato.argus.android.ui.profile.component.ProfileFilters

@Composable fun FilterBar(nav: NavController) = Column(Modifier.padding(top = 16.dp, bottom = 4.dp).fillMaxWidth()) {
    Row(Modifier.padding(8.dp, 0.dp, 0.dp, 4.dp)) { ProfileFilters() }
    Row(Modifier.padding(8.dp, 0.dp, 0.dp, 4.dp)) { NetworkFilters() }
    Row(Modifier.padding(8.dp, 0.dp, 0.dp, 4.dp)) { GenreFilters() }
}
