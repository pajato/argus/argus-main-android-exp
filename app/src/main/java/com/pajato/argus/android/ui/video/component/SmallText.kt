package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable fun SmallText(label: String) {
    Text(label, Modifier.padding(horizontal = 12.dp).padding(bottom = 4.dp), fontSize = 10.sp)
}
