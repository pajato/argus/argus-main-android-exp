package com.pajato.argus.android.ui.search.component

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.pajato.argus.android.ui.video.component.TitleImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable fun SearchImageOrText(item: SearchData, modifier: Modifier) {
    val path = remember { mutableStateOf("") }
    val id = "${item.id}"
    LaunchedEffect(item) { launch(Dispatchers.IO) { path.value = item.backdropPath } }
    if (path.value.isEmpty()) Text(id, modifier) else TitleImage(path.value, id)
}
