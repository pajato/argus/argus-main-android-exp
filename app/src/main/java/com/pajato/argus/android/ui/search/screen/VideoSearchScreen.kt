package com.pajato.argus.android.ui.search.screen

import com.pajato.argus.coordinator.core.shelf.Shelf

const val CardHeight = 200
const val CardWidth = 130

object VideoSearchScreen {
    const val ROUTE = "VideoSearchScreen"
    var originShelf: Shelf? = null
}
