package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable internal fun ShelfTextLabel(label: String) {
    Text(label, Modifier.padding(start = 8.dp, top = 8.dp), Color.White.copy(alpha = 0.78F))
}