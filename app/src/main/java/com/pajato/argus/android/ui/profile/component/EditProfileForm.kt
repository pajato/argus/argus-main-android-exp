package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.common.ChangeHandler
import com.pajato.argus.android.ui.common.MutableString
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.android.ui.profile.ProfileViewModel.itemUnderEdit

@Composable fun EditProfileForm() {
    val error = remember { mutableStateOf(ProfileViewModel.hasError(itemUnderEdit.profile.label)) }
    Column { ProfileTextField(error) }
}

@Composable private fun ProfileTextField(error: MutableState<Boolean>) {
    if (error.value) Text("This label is already used", Modifier.padding(8.dp), Color.Red)
    ProfileTextField { Text("Enter label") }
}


@Composable fun ProfileTextField(label: @Composable () -> Unit) {
    val state = remember { mutableStateOf(itemUnderEdit.profile.label) }
    val modifier = Modifier.padding(horizontal = 8.dp).fillMaxWidth()
    val changeHandler: ChangeHandler = { newLabel -> onValueChanged(state, newLabel) }
    OutlinedTextField(state.value, changeHandler, modifier, label = label, trailingIcon = { ClearIcon(state) })
}

@Composable fun ClearIcon(state: MutableString) {
    val contentDesc = "Profile TextField"
    val modifier =  Modifier.clickable { onValueChanged(state, "") }
    Icon(Icons.Default.Clear, contentDesc,modifier)
}

private fun onValueChanged(state: MutableString, text: String) {
    val profile = itemUnderEdit.profile.copy(label = text)
    itemUnderEdit = itemUnderEdit.copy(profile = profile)
    state.value = text
}
