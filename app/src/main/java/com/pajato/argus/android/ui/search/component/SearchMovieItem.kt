package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.search.MovieSearchViewModel
import com.pajato.argus.android.ui.search.SearchState
import com.pajato.argus.android.ui.search.screen.MovieSearchDetailsScreen
import com.pajato.tks.search.pager.core.SearchResultMovie

@Composable fun SearchMovieItem(i: Int, nav: NavController, name: String, state: SearchState<SearchResultMovie>) {
    val needsFetch = i >= state.items.size && !state.endReached && !state.isLoading
    LaunchedEffect(key1 = state) { if (needsFetch) MovieSearchViewModel.getNextPage(name, state) }
    SearchMovieItem(state.items[i], nav)
}

@Composable fun SearchMovieItem(item: SearchResultMovie, nav: NavController) = Row {
    var expanded by remember { mutableStateOf(false) }
    val dismiss: Handler = { expanded = false }
    val onClick: Handler = { navigateToMovieDetailsScreen(nav, item) }
    val onLongClick: Handler = { expanded = true }
    Modifier.fillMaxSize().extend(onClick, onLongClick).SearchMovieItemHolder(item, nav, expanded, dismiss)
}

private fun navigateToMovieDetailsScreen(nav: NavController, item: SearchResultMovie) {
    MovieSearchViewModel.selectedItem = item
    nav.navigate(MovieSearchDetailsScreen.ROUTE)
}
