package com.pajato.argus.android.ui.search.screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.common.component.NavHeader
import com.pajato.argus.android.ui.search.component.SearchDetailsBody
import com.pajato.argus.android.ui.search.component.getSearchDetailsScreenTitle
import com.pajato.argus.android.ui.search.component.toData
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.component.CancelDoneHeader
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.search.VideoSearchError
import com.pajato.argus.android.ui.video.handler.addToHistory
import com.pajato.argus.android.ui.video.handler.addToWatchLater
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultTv

@Composable fun TvSearchDetailsScreen(item: SearchResultTv, nav: NavController) =
    Column(Modifier.fillMaxSize()) {
        val startDest = Pair(NavRoute.BackToVideos) { nav.navigate(NavRoute.BackToVideos.route) }
        NavHeader(getSearchDetailsScreenTitle(), startDest, Pair(NavRoute.Done) { addToShelf(item, nav) {} })
        SearchDetailsBody(item.toData())
    }

@Composable fun MovieSearchDetailsScreen(item: SearchResultMovie, nav: NavController) =
    Column(Modifier.fillMaxSize()) {
        val startDest = Pair(NavRoute.BackToVideos) { nav.navigate(NavRoute.BackToVideos.route) }
        NavHeader(getSearchDetailsScreenTitle(), startDest, Pair(NavRoute.Done) { addToShelf(item, nav) {} })
        SearchDetailsBody(item.toData())
    }

@Composable fun MultiSearchDetailsScreen(nav: NavController) = Column(Modifier.fillMaxSize()) {
    CancelDoneHeader(nav, getSearchDetailsScreenTitle())
    Box(Modifier.fillMaxSize(), Alignment.Center) { Text("Details Coming soon") }
}

internal fun addToShelf(item: SearchResultMovie, nav: NavController, dismiss: Handler) = when (getShelfId()) {
    WatchLaterViewModel.id -> addToWatchLater(item, nav, dismiss)
    HistoryViewModel.id -> addToHistory(item, nav, dismiss)
    else -> throw VideoSearchError("Origin shelf is not declared!")
}

internal fun addToShelf(item: SearchResultTv, nav: NavController, dismiss: Handler) = when (getShelfId()) {
    WatchLaterViewModel.id -> addToWatchLater(item, nav, dismiss)
    HistoryViewModel.id -> addToHistory(item, nav, dismiss)
    else -> throw VideoSearchError("Origin shelf is not declared!")
}

internal fun getShelfId(): String = VideoSearchScreen.originShelf?.id ?: ""
