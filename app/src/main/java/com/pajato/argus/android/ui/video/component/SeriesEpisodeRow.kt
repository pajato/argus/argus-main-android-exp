package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Arrangement.SpaceAround
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.uc.TvUseCases.getEpisodeCount
import com.pajato.argus.info.uc.TvUseCases.getSeriesCount
@Composable fun SeriesEpisodeRow(video: Video) {
    val seriesState = getSeriesMenuState(video)
    val series = remember { mutableIntStateOf(video.series) }
    val episodeState = getEpisodeMenuState(video, series.intValue)
    Row(Modifier.fillMaxWidth(), SpaceAround) { RowContent(video, seriesState, episodeState) }
}

@Composable private fun getSeriesMenuState(video: Video): MenuState {
    val list = getList(getSeriesCount(ArgusInfoRepo, InfoKey(InfoType.Tv.name, video.infoId))).toMutableStateList()
    val selected = video.series.toString()
    return MenuState(list, remember { mutableStateOf(false) }, remember { mutableStateOf(selected) })
}

@Composable private fun getEpisodeMenuState(video: Video, series: Int): MenuState {
    val list = getList(getEpisodeCount(ArgusInfoRepo, video.infoId, series)).toMutableStateList()
    val selected = video.episode.toString()
    return MenuState(list, remember { mutableStateOf(false) }, remember { mutableStateOf(selected) })
}

@Composable private fun RowContent(video: Video, seriesState: MenuState, episodeState: MenuState) {
    val modifier = Modifier.padding(all = 4.dp).width(128.dp)
    SeriesDropMenu(video, seriesState, episodeState, modifier)
    EpisodeDropMenu(episodeState, modifier)
}
