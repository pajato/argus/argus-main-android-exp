package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults.filterChipColors
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.android.ui.profile.event.ToggleProfileSelection
import com.pajato.argus.profile.core.SelectableProfile

@Composable fun ProfileFilters() {
    val list: List<SelectableProfile> = ProfileViewModel.profiles.filter { !it.isHidden }
    val listState = rememberLazyListState()
    LazyRow(Modifier.padding(start = 2.dp), listState) { items(list.size) { ProfileFilterChip(list, it) } }
}

@Composable private fun ProfileFilterChip(list: List<SelectableProfile>, it: Int) {
    val colors = filterChipColors(selectedContainerColor = White, selectedLabelColor = Black)
    val text = @Composable { Text(list[it].profile.label) }
    fun onClick() { ProfileViewModel.onEvent(ToggleProfileSelection(list[it])) }
    FilterChip(list[it].isSelected, ::onClick, text, Modifier.padding(horizontal = 4.dp), colors = colors)
}
