package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.History
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.filter.FilterViewModel
import com.pajato.argus.android.ui.search.SearchBarParams

typealias MutableString = MutableState<String>
typealias MutableBoolean = MutableState<Boolean>

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun SearchBarParams.ArgusSearchBar(focusRequester: FocusRequester) {
    val params = this
    val modifier = Modifier.fillMaxWidth().focusRequester(focusRequester)
    SearchBar(text.value, onQueryChange, onSearch, true, onActiveChange, modifier, placeholder = placeholder,
        leadingIcon = leadingIcon, trailingIcon = trailingIcon) { SearchViewContent(params, focusRequester) }
}

@Composable private fun SearchViewContent(params: SearchBarParams, focusRequester: FocusRequester) {
    SearchViewContent(params)
    LaunchedEffect(Unit) { focusRequester.requestFocus() }
}

@Composable private fun SearchViewContent(params: SearchBarParams) {
    val list: List<String> = FilterViewModel.savedQueries
    val listState = rememberLazyListState()
    val active = params.active.value
    if (active) QueryResults(params.nav, params.text.value) else QueryHistory(params, listState, list)
}

@Composable private fun QueryHistory(params: SearchBarParams, listState: LazyListState, list: List<String>) {
    val modifier = Modifier.padding(start = 2.dp)
    LazyColumn(modifier, listState) { items(list.size) { HistoryItem(list[it], params) } }
}

@Composable private fun HistoryItem(item: String, params: SearchBarParams) {
    fun search() { params.onQueryChange(item); params.onSearch(item) }
    val keyboard = LocalSoftwareKeyboardController.current
    val modifier = Modifier.padding(14.dp).fillMaxWidth().clickable { keyboard?.hide(); search() }
    Row(modifier, Arrangement.SpaceBetween) { HistoryItem(item) }
}

@Composable private fun HistoryItem(item: String) {
    Row { Icon(Icons.Default.History, "History icon", Modifier.padding(end = 8.dp)); Text(item) }
    Icon(Icons.Default.Close, "Close icon", Modifier.clickable { FilterViewModel.removeQuery(item) })
}

@Composable internal fun CloseHandler(nav: NavController, text: MutableString, active: MutableBoolean) {
    val modifier = Modifier.clickable { if (text.value.isNotEmpty()) { text.value = ""; active.value = false } else nav.navigateUp() }
    Icon(Icons.Default.Close, "Close icon", modifier)
}
