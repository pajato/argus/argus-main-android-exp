package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Video

@Composable fun InfoBox(video: Video, shelf: Shelf) {
    val modifier = Modifier.fillMaxWidth()
    Column(modifier, horizontalAlignment = CenterHorizontally) { InfoColumn(shelf, video) }
}
