package com.pajato.argus.android.ui.video.component

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.common.component.notSelectedPair

fun Modifier.getNavModifier(handler: Handler): Modifier = composed {
    val color: Color by animateColorAsState(notSelectedPair.first, notSelectedPair.second, label = "")
    val source = remember { MutableInteractionSource() }
    val indication = null
    fun Modifier.getClickable(): Modifier = composed { clickable(remember { source }, indication) { handler() } }
    this.background(color).height(32.dp).widthIn(32.dp).clip(CircleShape).getClickable()
}
