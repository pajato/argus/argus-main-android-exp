package com.pajato.argus.android.ui.video

enum class EditVideoParam {
    Profile, Network, Series, Episode, Timestamp;
    fun toKey() = name.lowercase()
    fun toLabel() = name
}
