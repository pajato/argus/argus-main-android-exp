package com.pajato.argus.android.ui.profile.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.ElevatedFilterChip
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.profile.ProfileViewModel
import com.pajato.argus.profile.adapter.ArgusProfileRepo
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.argus.profile.uc.ProfileUseCases

@Composable fun ProfileList(nav: NavController, list: List<SelectableProfile>) {
    val state = rememberLazyListState()
    val modifier = Modifier.fillMaxWidth().padding(top = 48.dp)
    LazyColumn(modifier, state, horizontalAlignment = Alignment.Start) { showProfiles(nav, list) }
}

private fun LazyListScope.showProfiles(nav: NavController, list: List<SelectableProfile>) {
    item { Text("PROFILES", Modifier.padding(top = 32.dp, start = 16.dp, bottom = 8.dp)) }
    items(list.size) { ProfileRow(nav, list[it]) }
}

@Composable private fun ProfileRow(nav: NavController, item: SelectableProfile) {
    Row(Modifier.padding(horizontal = 8.dp), Arrangement.Start, CenterVertically) {
        Box(Modifier.fillMaxWidth(0.15f).padding(horizontal = 4.dp)) { ProfileIcon(item.profile.label) }
        Row(Modifier.fillMaxWidth(), SpaceBetween) { ChipAndActions(item, nav) }
    }
}

@Composable private fun ProfileIcon(label: String) = ProfileIcon(label, Modifier, 100)

@Composable private fun ChipAndActions(item: SelectableProfile, nav: NavController) {
    FilterChip(item, nav)
    ActionsRow(item, nav)
}

@Composable private fun FilterChip(item: SelectableProfile, nav: NavController) {
    val isSelected = item.isSelected
    fun select() = selectItem(item, nav)
    ElevatedFilterChip(isSelected, onClick = ::select, label = { Text(item.profile.label) })
}

private fun selectItem(item: SelectableProfile, nav: NavController) {
    ProfileUseCases.getAll(ArgusProfileRepo).forEach { if (it.isSelected) ProfileViewModel.deselect(it) }
    ProfileViewModel.select(item)
    goToVideosScreen(nav) {}
}
