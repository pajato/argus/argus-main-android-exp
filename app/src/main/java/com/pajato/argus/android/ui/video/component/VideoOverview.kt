package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Cyan
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.uc.MovieUseCases
import com.pajato.tks.common.core.EpisodeKey
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable fun VideoOverview(video: Video, modifier: Modifier) {
    val overview = remember { mutableStateOf("") }
    LaunchedEffect(key1 = true) { getOverview(video, overview) }
    Row(Modifier.fillMaxWidth().height(216.dp)) { OverviewWithScrollBarMaybe(modifier, overview) }
}

private fun CoroutineScope.getOverview(video: Video, overview: MutableState<String>) {
    launch(Dispatchers.IO) {
        fun getMovieOverview() = MovieUseCases.getMovieOverview(ArgusInfoRepo, InfoKey(Movie.name, video.infoId))
        if (video.isMovie()) overview.value = getMovieOverview() else getEpisodeOverview(video, overview)
    }
}

private suspend fun getEpisodeOverview(video: Video, overview: MutableState<String>) {
    fun Video.toEpisodeKey() = EpisodeKey(infoId, series, episode)
    overview.value = TmdbEpisodeRepo.getEpisode(video.toEpisodeKey()).overview
}

@Composable fun OverviewWithScrollBarMaybe(modifier: Modifier, overview: MutableState<String>) {
    val lineCount = remember { mutableIntStateOf(1) }
    fun onTextLayout(result: TextLayoutResult) { lineCount.intValue = result.lineCount }
    OverviewText(modifier, overview, ::onTextLayout)
    if (lineCount.intValue > 8) Box(Modifier.padding(end = 4.dp).fillMaxHeight().width(4.dp).background(Cyan))
}
