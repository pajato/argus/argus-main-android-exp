package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.component.NavHeader
import com.pajato.argus.android.ui.common.NavRoute

typealias Nav = NavController

@Composable fun VideoSearchBody(nav: Nav, name: String, onValueChanged: (String) -> Unit) {
    val modifier: Modifier = Modifier.fillMaxSize()
    Column(modifier.background(Color(0xFF180E36))) { VideoSearchBody(nav, name, onValueChanged, modifier) }
}

@Composable fun VideoSearchBody(nav: Nav, name: String, onValueChanged: (String) -> Unit, modifier: Modifier) {
    val start = Pair(NavRoute.BackToVideos) { nav.navigate(NavRoute.BackToVideos.route) }
    val label = @Composable { Text("Enter video name") }
    NavHeader("Add New Video", start, null)
    SearchTextField(name, onValueChanged, label)
    SearchScroller(nav, rememberLazyListState(), modifier, name)
}
