package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProfileDropMenu(profileState: MenuState, modifier: Modifier) {
    val (options, expanded, selected) = profileState
    fun selectItem(item: String) { profileSelectItem(item, profileState) }
    ExposedDropdownMenuBox(expanded.value, { expanded.value = !expanded.value }, modifier) {
        EditVideoDropMenuLabel(selected.value, "Profile", expanded.value)
        EditVideoDropMenuItems(options, expanded.value, { expanded.value = !expanded.value }, ::selectItem)
    }
}

private fun profileSelectItem(item: String, profileState: MenuState) {
    val (_, expanded, selected) = profileState
    selectItem(item, selected, expanded, "profile")
}
