package com.pajato.argus.android.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.search.pager.core.SearchPage
import com.pajato.tks.search.pager.core.SearchResult
import java.net.URLEncoder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class AbstractSearchViewModel<T : SearchResult>(val searchPage: Searcher<T>) : ViewModel() {
    val screenState get() = _state
    lateinit var selectedItem: T
    private var activeJob: Job? = null
    private var _state: SearchState<T> = SearchState()

   suspend fun getFirstPage(name: String): SearchState<T> {
       if (activeJob != null) cancelActiveJob()
       return getNextPage(name, SearchState())
   }

   private fun cancelActiveJob() {
       activeJob?.cancel() ?: return
       activeJob?.cancelChildren() ?: return
   }

   suspend fun getNextPage(name: String, state: SearchState<T>): SearchState<T> {
       activeJob = viewModelScope.launch(Dispatchers.IO) { fetchResponse(name, state.page + 1, state) }
       activeJob!!.join()
       return _state
   }

   private suspend fun fetchResponse(name: String, nextPage: Int, state: SearchState<T>) {
       val key = SearchKey("", withContext(Dispatchers.IO) { URLEncoder.encode(name, "utf-8") }, nextPage)
       val movieSearchResponse: SearchPage<T> = searchPage(key) // TmdbSearchMovieRepo.getSearchPageMovie(key)
       _state = movieSearchResponse.toMovieScreenState(state)
       activeJob = null
   }

   private fun SearchPage<T>.toMovieScreenState(state: SearchState<T>): SearchState<T> {
       val items = state.items + results
       val error = ""
       val endReached = page == totalPages
       return SearchState(false, items, error, endReached, page, totalResults)
   }
}
