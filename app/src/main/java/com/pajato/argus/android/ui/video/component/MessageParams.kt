package com.pajato.argus.android.ui.video.component

import android.content.Context
import android.content.Intent

data class MessageParams(
    val context: Context,
    val label: String,
    val intent: Intent?,
    val invalidMessage: String,
    val notInstalledMessage: String
)
