package com.pajato.argus.android.ui.video.handler

import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.coordinator.uc.CoordinatorUseCases
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.addAllVideos
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.moveShelfDown
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.moveShelfUp

internal fun transfer(list: List<Video>, dismiss: Handler, mainDismiss: Handler) {
    val repo: VideoRepo = ArgusCoordinatorRepo.getVideoRepo(HistoryViewModel.id)
    HistoryViewModel.runUseCase { addAllVideos(repo, list) }
    dismiss()
    mainDismiss()
}

internal fun moveShelfUpHandler(id: String) { moveShelfUp(ArgusCoordinatorRepo, id) }

internal fun moveShelfDownHandler(id: String) { moveShelfDown(ArgusCoordinatorRepo, id) }
