package com.pajato.argus.android.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.pajato.argus.android.ui.account.screen.AboutScreen
import com.pajato.argus.android.ui.account.screen.AccountScreen
import com.pajato.argus.android.ui.feedback.screen.FeedbackScreen
import com.pajato.argus.android.ui.filter.component.SelectFiltersScreen
import com.pajato.argus.android.ui.filter.screen.SelectFiltersScreen
import com.pajato.argus.android.ui.genre.screen.ManageGenresScreen
import com.pajato.argus.android.ui.network.screen.ManageNetworksScreen
import com.pajato.argus.android.ui.profile.screen.EditProfileScreen
import com.pajato.argus.android.ui.profile.screen.ManageProfilesScreen
import com.pajato.argus.android.ui.search.MovieSearchViewModel
import com.pajato.argus.android.ui.search.TvSearchViewModel
import com.pajato.argus.android.ui.search.component.QuerySearchScreen
import com.pajato.argus.android.ui.search.component.VideoSearchScreen
import com.pajato.argus.android.ui.search.screen.MovieSearchDetailsScreen
import com.pajato.argus.android.ui.search.screen.MultiSearchDetailsScreen
import com.pajato.argus.android.ui.search.screen.QuerySearchScreen
import com.pajato.argus.android.ui.search.screen.TvSearchDetailsScreen
import com.pajato.argus.android.ui.search.screen.VideoSearchScreen
import com.pajato.argus.android.ui.video.component.ArgusMainScreen
import com.pajato.argus.android.ui.video.screen.EditVideoScreen
import com.pajato.argus.android.ui.video.screen.VideoDetailsScreen
import com.pajato.argus.android.ui.video.screen.VideoRatingsScreen
import com.pajato.argus.android.ui.video.screen.VideosScreen

@Composable fun ArgusNavigation(nav: NavHostController) {
    NavHost(nav, startDestination = VideosScreen.ROUTE) {
        composable(AboutScreen.ROUTE) { AboutScreen(nav) }
        composable(AccountScreen.ROUTE) { AccountScreen(nav) }
        composable(EditProfileScreen.ROUTE) { EditProfileScreen(nav) }
        composable(EditVideoScreen.ROUTE) { EditVideoScreen(nav) }
        composable(FeedbackScreen.ROUTE) { FeedbackScreen(nav) }
        composable(ManageGenresScreen.ROUTE) { ManageGenresScreen(nav) }
        composable(ManageNetworksScreen.ROUTE) { ManageNetworksScreen(nav) }
        composable(ManageProfilesScreen.ROUTE) { ManageProfilesScreen(nav) }
        composable(MovieSearchDetailsScreen.ROUTE) { MovieSearchDetailsScreen(MovieSearchViewModel.selectedItem, nav)}
        composable(MultiSearchDetailsScreen.ROUTE) { MultiSearchDetailsScreen(nav)}
        composable(QuerySearchScreen.ROUTE) { QuerySearchScreen(nav) }
        composable(SelectFiltersScreen.ROUTE) { SelectFiltersScreen(nav) }
        composable(TvSearchDetailsScreen.ROUTE) { TvSearchDetailsScreen(TvSearchViewModel.selectedItem, nav)}
        composable(VideoDetailsScreen.ROUTE) { VideoDetailsScreen(nav) }
        composable(VideoRatingsScreen.ROUTE) { VideoRatingsScreen(nav) }
        composable(VideoSearchScreen.ROUTE) { VideoSearchScreen(nav) }
        composable(VideosScreen.ROUTE) { ArgusMainScreen(nav) }
    }
}
