package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.NavRouteParam
import com.pajato.argus.android.ui.search.Composer
import com.pajato.argus.android.ui.search.KeyboardDone
import com.pajato.argus.android.ui.search.QueryCB
import com.pajato.argus.android.ui.video.component.getNavModifier

@Composable fun QuerySearchBody(nav: NavController, query: String, onValueChanged: QueryCB, onDone: KeyboardDone) {
    val start = Pair(NavRoute.BackToVideos) { nav.navigate(NavRoute.BackToVideos.route) }
    Column(Modifier.fillMaxSize()) {
        Row {
            val label = @Composable { Text("Enter query text") }
            NavIcon(Modifier.padding(start = 8.dp).align(Alignment.CenterVertically), start)
            QuerySearchTextField(query, onValueChanged, onDone, label)
        }
    }
}

@Composable private fun NavIcon(modifier: Modifier, param: NavRouteParam) {
    val handler: Handler = param.second
    val iconModifier = modifier.getNavModifier(handler)
    Icon(param.first.icon, param.first.desc, iconModifier)
}

@Composable
private fun QuerySearchTextField(name: String, onValueChanged: QueryCB, onDone: KeyboardDone, label: Composer) {
    val contentDesc = ""
    val icon = @Composable { Icon(Icons.Filled.Clear, contentDesc, Modifier.clickable { onValueChanged("") }) }
    QueryTextField(name, onValueChanged, label, icon, onDone)
}

@Composable
private fun QueryTextField(name: String, changed: QueryCB, label: Composer, icon: Composer, onDone: KeyboardDone) {
    val modifier = Modifier.padding(horizontal = 8.dp).fillMaxWidth()
    val actions = KeyboardActions(onDone)
    OutlinedTextField(
        name, changed, modifier, label = label, trailingIcon = icon, singleLine = true,
        keyboardActions = actions
    )
}
