package com.pajato.argus.android.ui.genre

import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pajato.argus.android.ui.common.ManagedViewModel
import com.pajato.argus.android.ui.genre.event.GenreEvent
import com.pajato.argus.android.ui.genre.event.ToggleHiddenGenre
import com.pajato.argus.android.ui.genre.event.ToggleSelectedGenre
import com.pajato.argus.android.ui.reloadShelfDataInOrder
import com.pajato.argus.genre.adapter.ArgusGenreRepo
import com.pajato.argus.genre.core.SelectableGenre
import java.net.URI
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

object GenreViewModel : ManagedViewModel, ViewModel() {
    private var genreStateList = listOf<SelectableGenre>().toMutableStateList() // aka _genres

    val genres: List<SelectableGenre> get() = genreStateList
    val selectedGenreIds: List<Int> get() = genres.filter { it.isSelected }.map { it.genre.id }

    // private var errorState: MutableState<Boolean> = mutableStateOf(false)

    override fun getSelected(label: String): Boolean =
        genreStateList.find { it.genre.name == label }?.isSelected ?: false

    override fun setSelected(label: String, selected: Boolean) {
        val item = genreStateList.find { it.genre.name == label } ?: return
        reloadData(item.copy(isSelected = selected))
    }

    override fun getHidden(label: String): Boolean =
        genreStateList.find { it.genre.name == label }?.isHidden ?: false

    override fun setHidden(label: String, hidden: Boolean) {
        val item = genreStateList.find { it.genre.name == label } ?: return
        reloadData(item.copy(isHidden = hidden))
    }

    fun reloadData(uri: URI) {
        viewModelScope.launch(IO) {
            ArgusGenreRepo.injectDependency(uri)
            reloadData()
        }
    }

    private fun reloadData() {
        genreStateList.clear()
        genreStateList.addAll(ArgusGenreRepo.cache.values.toMutableStateList())
    }

    fun onEvent(event: GenreEvent) = when (event) {
        is ToggleSelectedGenre -> toggleSelected(event.item)
        is ToggleHiddenGenre -> toggleHidden(event.item)
    }

    private fun toggleHidden(item: SelectableGenre) = reloadData(item.copy(isHidden = !item.isHidden))

    private fun toggleSelected(item: SelectableGenre) = reloadData(item.copy(isSelected = !item.isSelected))

    private fun reloadData(item: SelectableGenre) {
        ArgusGenreRepo.register(item)
        reloadData()
        viewModelScope.launch { reloadShelfDataInOrder() }
    }
}
