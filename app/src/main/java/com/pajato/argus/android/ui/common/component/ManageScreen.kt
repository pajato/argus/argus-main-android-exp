package com.pajato.argus.android.ui.common.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign.Companion.Center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.NavRoute
import com.pajato.argus.android.ui.common.NavRouteParam
import com.pajato.argus.android.ui.profile.I18nStrings
import com.pajato.argus.android.ui.video.screen.VideosScreen.ROUTE

@Composable fun ManageScreen(params: ManageParams) {
    I18nStrings.registerStrings()
    Column(Modifier.fillMaxWidth()) { ManagedScreenSections(params) }
}

@Composable private fun ManagedScreenSections(params: ManageParams) {
    ManageHeader(params.nav, "Manage ${params.title}")
    TopImageBox(params.drawable, "Manage ${params.title} Image")
    Text(params.title.uppercase(), Modifier.padding(16.dp))
    ManageList(params)
    params.extra()
}
@Composable private fun ManageHeader(nav: NavController, title: String) {
    val modifier = Modifier.fillMaxWidth().height(60.dp).padding(vertical = 8.dp)
    Box(modifier) { ManageHeaderItems(nav, title) }
}

@Composable private fun BoxScope.ManageHeaderItems(nav: NavController, title: String) {
    NavIcon(Modifier.padding(start = 8.dp).align(CenterStart), NavRouteParam(NavRoute.Cancel) { nav.navigate(ROUTE) })
    Text(title, Modifier.align(Alignment.Center).padding(8.dp), textAlign = Center, fontSize = 20.sp)
}
