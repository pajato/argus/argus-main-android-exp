package com.pajato.argus.android.ui.search.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.LightGray
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.tks.search.pager.core.SearchResultMulti

@Composable
fun Modifier.SearchMultiItemHolder(item: SearchResultMulti, nav: NavController, expanded: Boolean, dismiss: Handler) =
    Column(this.background(Color.Black)) {
        MultiSearchResultRow(item, Modifier.fillMaxWidth().padding(horizontal = 8.dp))
        SearchItemMenu(item, nav, expanded, dismiss)
        OverviewText(item)
        Spacer(modifier = Modifier.fillMaxWidth().height(2.dp).background(color = Color.DarkGray))
    }

@Composable private fun MultiSearchResultRow(item: SearchResultMulti, modifier: Modifier) =
    Row(Modifier.fillMaxWidth()) {
        SearchItemImage(item)
        SearchItemInfo(item, modifier)
    }

@Composable fun SearchItemImage(item: SearchResultMulti) = SearchImage(item.name, item.posterPath, 72)

@Composable private fun SearchItemInfo(item: SearchResultMulti, modifier: Modifier) =
    Column(Modifier.fillMaxWidth().height(100.dp), Arrangement.SpaceAround) {
        Row(modifier, SpaceBetween, CenterVertically) { TypeDateRatings(item) }
        TitleText(item, modifier)
        Genres(item)
    }

@Composable private fun TypeDateRatings(item: SearchResultMulti) {
    MediaTypeText(item, Modifier)
    DateText(item, Modifier, LightGray, 12.sp)
    RatingStars((item.voteAverage * 10).toInt())
}

@Composable fun MediaTypeText(item: SearchResultMulti, modifier: Modifier) {
    val text = item.mediaType.uppercase()
    Text(text, modifier, LightGray, 12.sp)
}

@Composable fun DateText(item: SearchResultMulti, modifier: Modifier, color: Color = White, fontSize: TextUnit) {
    val text = if (item.mediaType == "movie") item.releaseDate else item.firstAirDate
    Text(text, modifier, color, fontSize)
}

@Composable fun TitleText(item: SearchResultMulti, modifier: Modifier) {
    val text = if (item.mediaType == "movie") item.title else item.name
    Text(text, modifier, White, 16.sp, FontStyle.Normal, FontWeight.Bold)
}

@Composable fun OverviewText(item: SearchResultMulti) {
    val text = item.overview
    val modifier = Modifier.padding(8.dp)
    val params = TextParams(text, modifier, LocalTextStyle.current, 14.sp)
    ExpandableText(modifier, params)
}

@Composable fun Genres(item: SearchResultMulti) {
    LazyRow { items(item.genreIds.size) { index -> GenreFilterChip(item.genreIds[index]) } }
}
