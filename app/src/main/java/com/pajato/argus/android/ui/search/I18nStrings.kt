package com.pajato.argus.android.ui.search

import com.pajato.i18n.strings.StringsResource.put

object I18nStrings {
    const val SEARCH_SEASON_SHELF = "SearchSeasonShelf"
    const val SEARCH_CAST_CREW = "SearchCastCrewShelf"
    const val SEARCH_REVIEWS = "SearchReviews"
    const val SEARCH_WATCH_NOW = "SearchWatchNow"
    const val SEARCH_WATCH_LATER_ADD = "SearchWatchLaterAdd"

    fun registerStrings() {
        put(SEARCH_SEASON_SHELF, "Season shelf, coming soon")
        put(SEARCH_CAST_CREW, "Cast and Crew shelves, coming soon")
        put(SEARCH_REVIEWS, "Reviews, etc, coming soon")
        put(SEARCH_WATCH_NOW, "Watch Now")
        put(SEARCH_WATCH_LATER_ADD, "Add to Watch Later list")
    }
}
