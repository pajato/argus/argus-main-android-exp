package com.pajato.argus.android.ui.network.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@Composable fun NetworkFilterShelf(nav: NavController) {
    Column(Modifier.padding(top = 16.dp, bottom = 4.dp).fillMaxWidth()) { NetworkList(nav) }
}
