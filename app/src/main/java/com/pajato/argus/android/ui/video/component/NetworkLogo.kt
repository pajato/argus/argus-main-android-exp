package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.unit.dp
import com.pajato.argus.coordinator.core.video.Video

@Composable fun NetworkLogo(video: Video) {
    Box(Modifier.height(56.dp).width(56.dp).background(White), Center) { LogoImage(video) }
}
