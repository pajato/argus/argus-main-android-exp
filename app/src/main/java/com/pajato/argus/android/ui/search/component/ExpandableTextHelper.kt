package com.pajato.argus.android.ui.search.component

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableIntState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString.Builder
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.TextUnit

internal data class TextParams(
    val text: String,
    val modifier: Modifier,
    val style: TextStyle,
    val size: TextUnit,
    val align: TextAlign? = null,
    val fontStyle: FontStyle? = null,
    val maxLines: Int = 2
)

internal data class ShowControls(
    val moreText: String,
    val moreStyle: SpanStyle,
    val lessText: String,
    val lessStyle: SpanStyle
)

internal data class ShowState(
    val clickable: MutableBoolean,
    val isExpanded: MutableBoolean,
    val lastCharIndex: MutableIntState
)

@Composable internal fun DecoratedText(state: ShowState, params: TextParams, controls: ShowControls, maxLines: Int ) {
    Text(getAnnotatedString(params.text, state, controls), params.modifier.fillMaxWidth().animateContentSize(),
        maxLines = if (state.isExpanded.value) Int.MAX_VALUE else maxLines, fontStyle = params.fontStyle,
        onTextLayout = { textLayoutResult -> performTextLayout(textLayoutResult, state, maxLines) },
        style = params.style, textAlign = params.align, fontSize = params.size)
}

private fun performTextLayout(result: TextLayoutResult, state: ShowState, collapsedMaxLine: Int) {
    if (!state.isExpanded.value && result.hasVisualOverflow) {
        state.clickable.value = true
        state.lastCharIndex.intValue = result.getLineEnd(collapsedMaxLine - 1)
    }
}

@Composable private fun getAnnotatedString(text: String, state: ShowState, controls: ShowControls) =
    buildAnnotatedString {
        when {
            state.clickable.value && state.isExpanded.value -> { FullText(text, controls) }
            state.clickable.value && !state.isExpanded.value -> { TruncatedText(text, state, controls) }
            else -> append(text)
        }
    }

@Composable private fun Builder.TruncatedText(fullText: String, state: ShowState, controls: ShowControls) {
    val text = fullText.substring(startIndex = 0, endIndex = state.lastCharIndex.intValue)
    append(text.dropLast(controls.moreText.length).dropLastWhile { Character.isWhitespace(it) || it == '.' })
    withStyle(style = controls.moreStyle) { append(controls.moreText) }
}

@Composable private fun Builder.FullText(text: String, controls: ShowControls) {
    append(text)
    withStyle(style = controls.lessStyle) { append(controls.lessText) }
}
