package com.pajato.argus.android.ui.video.component

import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.video.handler.transferNetwork
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.network.adapter.ArgusNetworkRepo
import com.pajato.argus.network.uc.NetworkUseCases

@Composable fun NetworkMenuItems(video: Video, dismiss: Handler, mainDismiss: Handler) =
    NetworkUseCases.getAll(ArgusNetworkRepo).filter { it.network.id != video.networkId }.forEach {
        DropdownMenuItem({ Text(it.network.label) }, { transferNetwork(it.network, video, dismiss, mainDismiss) })
    }
