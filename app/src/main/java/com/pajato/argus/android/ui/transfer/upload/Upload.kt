package com.pajato.argus.android.ui.transfer.upload

import android.content.Context
import com.pajato.argus.android.R
import java.io.File
import java.io.IOException
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

private const val EXTENSION = ".txt"

fun upload(context: Context) {
    val filesDir = context.filesDir
    val zipFile = getUploadZipFile(context, filesDir) ?: return
    val stream = ZipOutputStream(zipFile.outputStream())
    upload(context, stream, filesDir)
}

private fun upload(context: Context, stream: ZipOutputStream, dir: File) =
    try { stream.use { uploadAll(context, dir, it) } } catch (exc: IOException) { exc.printStackTrace() }

private fun uploadAll(context: Context, filesDir: File, stream: ZipOutputStream) {
    filesDir.walkTopDown().forEach { file -> if (captureFile(file)) uploadFile(file, filesDir, stream) }
    showToast(context, R.string.upload_complete)
}

private fun uploadFile(file: File, filesDir: File, outputStream: ZipOutputStream) {
    val relativePath = file.relativeTo(filesDir).path
    val zipEntry = ZipEntry(relativePath)
    outputStream.putNextEntry(zipEntry)
    file.inputStream().use { inputStream -> inputStream.copyTo(outputStream) }
    outputStream.closeEntry()
}

private fun captureFile(file: File): Boolean = file.isFile && file.name.endsWith(EXTENSION)
