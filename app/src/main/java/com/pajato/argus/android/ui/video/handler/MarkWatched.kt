package com.pajato.argus.android.ui.video.handler

import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.Handler
import com.pajato.argus.android.ui.goToVideosScreen
import com.pajato.argus.android.ui.history.HistoryViewModel
import com.pajato.argus.android.ui.search.component.getMovieVideo
import com.pajato.argus.android.ui.search.component.getTvVideo
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.android.ui.watchlater.WatchLaterViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.cache
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.addVideo
import com.pajato.argus.info.adapter.ArgusInfoRepo
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultMulti
import com.pajato.tks.search.pager.core.SearchResultTv

fun markWatched(item: SearchResultMovie, nav: NavController, dismiss: Handler) {
    addNew(getMovieVideo(item.id).copyAsNow(), nav, dismiss)
}

fun markWatched(item: SearchResultMulti, nav: NavController, dismiss: Handler) {
    addNew(getMovieVideo(item.id).copyAsNow(), nav, dismiss)
}

fun markWatched(item: SearchResultTv, nav: NavController, dismiss: Handler) {
    addNew(getTvVideo(item.id).copyAsNow(), nav, dismiss)
}

private fun addNew(newVideo: Video, nav: NavController, dismiss: Handler) {
    val historyCoordinator = cache[HistoryViewModel.id] ?: error("History coordinator not found!")
    HistoryViewModel.runUseCase { addVideo(historyCoordinator.repo, newVideo.copyAsNow()) }
    goToVideosScreen(nav, dismiss)
}

fun markWatched(params: DetailParams, dismiss: Handler) {
    val id = params.coordinator.shelf.id
    CoordinatorUseCases.markWatched(ArgusInfoRepo, ArgusCoordinatorRepo, params.coordinator, params.video)
    if (id == WatchLaterViewModel.id) removeFromWatchLater(params, dismiss)
    goToVideosScreen(params.nav, dismiss)
}

private fun removeFromWatchLater(params: DetailParams, dismiss: Handler) { removeVideo(params, dismiss) }
