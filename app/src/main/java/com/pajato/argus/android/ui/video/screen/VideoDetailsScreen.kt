package com.pajato.argus.android.ui.video.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.pajato.argus.android.ui.video.VideoViewModel
import com.pajato.argus.android.ui.video.component.DetailParams
import com.pajato.argus.android.ui.video.component.VideoDetailsBody
import com.pajato.argus.android.ui.video.component.VideoDetailsHeader

object VideoDetailsScreen { const val ROUTE = "VideoDetailsScreen" }

@Composable fun VideoDetailsScreen(nav: NavController) = Column(Modifier.fillMaxSize()) {
    val args = VideoViewModel.detailArgs
    val params = DetailParams(args.coordinator, args.video, nav)
    VideoDetailsHeader(args.video, nav)
    VideoDetailsBody(params)
}
