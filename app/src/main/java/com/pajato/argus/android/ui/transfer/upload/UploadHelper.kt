package com.pajato.argus.android.ui.transfer.upload

import android.content.Context
import android.widget.Toast
import com.pajato.argus.android.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.zip.ZipOutputStream

private const val ZIP_FILE_NAME = "argus_exp_files.zip"
private const val UPLOAD_DIR = "Upload"

internal fun getUploadZipFile(context: Context, filesDir: File): File? = when {
    filesDir.exists().not() -> showToastReturnNull(context, R.string.files_dir_does_not_exist)
    File(filesDir, UPLOAD_DIR).isDirectory -> createWithFile(filesDir)
    File(filesDir, UPLOAD_DIR).exists().not() -> createWithDir(context, filesDir)
    else -> showToastReturnNull(context, R.string.upload_dir_is_a_file)
}

fun createWithDir(context: Context, filesDir: File): File {
    val resId = R.string.failed_to_create_upload_dir
    var dir: File? = null
    try { dir = File(filesDir, UPLOAD_DIR).apply { mkdir() } } catch (exc: IOException) {
        showToast(context, resId)
    }
    return File(dir, ZIP_FILE_NAME).apply { createZipFile(this) }
}

private fun createWithFile(dir: File): File {
    val zipDir = File(dir, UPLOAD_DIR)
    val zipFile = File(zipDir, ZIP_FILE_NAME)
    createZipFile(zipFile)
    return zipFile
}

private fun createZipFile(zipFile: File) {
    val tempFile = File.createTempFile("temp", ".zip", zipFile.parentFile)
    ZipOutputStream(FileOutputStream(tempFile)).use { }
    if (zipFile.exists()) zipFile.delete()
    tempFile.renameTo(zipFile)
}

internal fun showToast(context: Context, resId: Int) {
    Toast.makeText(context, context.getString(resId), Toast.LENGTH_LONG).show()
}

internal fun showToastReturnNull(context: Context, resId: Int): File? {
    Toast.makeText(context, context.getString(resId), Toast.LENGTH_LONG).show()
    return null
}
