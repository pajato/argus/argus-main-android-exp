package com.pajato.argus.android.ui.video.component

import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.pajato.argus.android.ui.video.EditVideoViewModel
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.toTimestamp
import java.text.ParsePosition

@Composable fun TimestampWithErrorMaybe(video: Video) = Column(Modifier.fillMaxWidth(), Center) {
    TimestampRow(video)
    if (isInvalidTimestamp(video)) {
        val message = "Invalid timestamp: conflicts with another video"
        Text(message, Modifier.fillMaxWidth().padding(vertical = 4.dp), Color.Red, textAlign = TextAlign.Center)
        hasTimestampErrors = true
    }
}

private fun isInvalidTimestamp(video: Video): Boolean {
    val savedTimestamp = EditVideoViewModel.map["timestamp"] ?: return false
    val newTimestamp = savedTimestamp.toTimestamp(ParsePosition(0)) ?: return true
    val repo = ArgusCoordinatorRepo.getVideoRepo(Shelves.LastWatched.name)
    return newTimestamp != video.timestamp && repo.cache.containsKey(newTimestamp)
}
