package com.pajato.argus.android.ui.video.component

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.pajato.argus.android.ui.common.component.NavHeader
import com.pajato.argus.android.ui.common.NavRoute

@Composable fun VideoRatingsHeader(nav: NavController) {
    val entry = NavRoute.BackToVideos
    val startDest = Pair(entry) { if (!nav.navigateUp()) nav.navigate(entry.route)  }
    NavHeader("Ratings", startDest, null)
}
