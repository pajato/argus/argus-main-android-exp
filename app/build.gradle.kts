import java.io.FileInputStream
import java.util.Properties

plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.kotlinAndroid)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.androidJUnit5)
    alias(libs.plugins.compose.compiler)
}

val appVersion = "0.9.68"
val credentials = rootProject.file("local.properties")
val credentialProperty = Properties()
credentialProperty.load(FileInputStream(credentials))

kotlin {
    sourceSets {
        debug { kotlin.srcDir("build/generated/ksp/debug/kotlin") }
        release { kotlin.srcDir("build/generated/ksp/release/kotlin") }
    }
}

android {
    namespace = "com.pajato.argus.android"
    compileSdk = 35

    defaultConfig {
        applicationId = "com.pajato.argus.android"
        minSdk = 30
        targetSdk = 34
        versionCode = 11
        versionName = "v$appVersion-$versionCode"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables { useSupportLibrary = true }

        buildConfigField("String", "BASE_URL", "\"https://api.themoviedb.org/\"")
        buildConfigField("String", "SMALL_IMAGE_URL", "\"https://image.tmdb.org/t/p/w200\"")
        buildConfigField("String", "LARGE_IMAGE_URL", "\"https://image.tmdb.org/t/p/w500\"")
        buildConfigField("String", "ORIGINAL_IMAGE_URL", "\"https://image.tmdb.org/t/p/original\"")
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            signingConfig = signingConfigs.getByName("debug")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions { jvmTarget = "17" }

    buildFeatures {
        compose = true
        buildConfig = true
    }

    composeOptions { kotlinCompilerExtensionVersion = "1.5.11" }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
            excludes += "/META-INF/gradle/incremental.annotation.processors"
        }
    }
}

dependencies {
    implementation(libs.activity.compose)

    implementation(libs.androidx.paging.runtime)
    implementation(libs.androidx.paging.compose)
    implementation(libs.androidx.navigation.compose)

    implementation(libs.coil.compose)

    implementation(libs.core)
    implementation(libs.core.splashscreen)
    implementation(libs.core.ktx)

    implementation(platform(libs.compose.bom))

    implementation(libs.ktor.client.core)
    implementation(libs.ktor.client.cio)

    implementation(libs.lifecycle.runtime.ktx)
    implementation(libs.material3)
    implementation(libs.material.icons.extended)

    implementation(libs.ui)
    implementation(libs.ui.graphics)
    implementation(libs.ui.tooling.preview)

    implementation(libs.kotlinx.datetime)

    implementation(libs.pajato.i18n.strings)
    implementation(libs.pajato.uri.validator)

    implementation(libs.argus.filter.adapter)
    implementation(libs.argus.filter.core)
    implementation(libs.argus.filter.uc)
    implementation(libs.argus.genre.adapter)
    implementation(libs.argus.genre.core)
    implementation(libs.argus.genre.uc)
    implementation(libs.argus.info.adapter)
    implementation(libs.argus.info.core)
    implementation(libs.argus.info.uc)
    implementation(libs.argus.network.adapter)
    implementation(libs.argus.network.core)
    implementation(libs.argus.network.uc)
    implementation(libs.argus.profile.adapter)
    implementation(libs.argus.profile.core)
    implementation(libs.argus.profile.uc)
    implementation(libs.argus.shared.core)
    implementation(libs.argus.videoShelf.adapter)
    implementation(libs.argus.videoShelf.core)
    implementation(libs.argus.videoShelf.uc)

    implementation(libs.tks.common.adapter)
    implementation(libs.tks.common.core)
    implementation(libs.tks.episode.adapter)
    implementation(libs.tks.episode.core)
    implementation(libs.tks.movie.adapter)
    implementation(libs.tks.movie.core)
    implementation(libs.tks.network.core)
    implementation(libs.tks.person.adapter)
    implementation(libs.tks.person.core)
    implementation(libs.tks.searchMovie.adapter)
    implementation(libs.tks.searchMovie.core)
    implementation(libs.tks.searchMulti.adapter)
    implementation(libs.tks.searchMulti.core)
    implementation(libs.tks.searchPager.core)
    implementation(libs.tks.searchTv.adapter)
    implementation(libs.tks.searchTv.core)
    implementation(libs.tks.season.adapter)
    implementation(libs.tks.season.core)
    implementation(libs.tks.tv.adapter)
    implementation(libs.tks.tv.core)

    testImplementation(libs.junit.jupiter)
    testImplementation(libs.pajato.test)

    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
    androidTestImplementation(platform(libs.compose.bom))

    debugImplementation(libs.ui.tooling)
    debugImplementation(libs.ui.test.manifest)
}
