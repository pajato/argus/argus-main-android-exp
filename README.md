# Argus

Argus (the all seeing one) is an Android app that tracks and manages streaming video watching activities.

The vision for Argus is to provide an app that allows a User to:

1) add and persist video titles associated with a network name, which identifies the organization providing the video: Hulu, Netflix, HBO, etc.
2) identify the type of video: movie, tv show, something else
3) start, stop, pause or resume watching a selected video
4) seamlessly obtain information from a video database (IMDB) for the selected video
...

In providing these features, Argus normalizes the myriad different experiences one might encounter in watching streamed videos.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Run the tests using the Gradle connectedCheck task: ```./gradlew connectedCheck```

After a successful test run, examine the code coverage data for that test run by viewing ...app/build/reports/coverage/debug/com.pajato.argus/index.html

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

This version of Argus borrows extensively from sample apps, tutorials and code-labs including:

* Jetsnack from the AOSP team for navigation and bottom app bar
* Infinite list scrolling article and code from Vincent Singh?
* Basic Compose code-labs from Google/Android
* Neatflix by Eric...
* This page for [configuration change layout design](https://johannblake.medium.com/creating-responsive-layouts-using-jetpack-compose-7746ba42666c)

## Todo

As of June 2022, some items that should be taken care of include:

- Apply useful tips from John Siracusa's blog post
- Add a real video id (int/long)
- Put into Play Store
- Favor Kotlin serialization over Gson
- Refactor ArgusBottomNavLayout
- Use video detail screen over launch click handler (launch from detail screen)
- Develop a "periodic episode release" user story
- Develop an "information provider" user story to support IMDB, TMDB, FMDB, etc
- Develop a "persistence" user story to support Firebase and possibly other backends
- Develop an "authentication" user story using Firebase auth
- Add Kover/Edgar to supply code coverage for Android-compose
